<?define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");?>
<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>

<?
CModule::IncludeModule("iblock");

$arData = json_decode($_REQUEST['data-trigger']);

//получаем адреса всех меток на Яндекс карту
$arResult["POINTS"] = Array();
$res = CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>40, "ACTIVE"=>"Y", "DEPTH_LEVEL"=>1), false, Array("UF_MAP", "UF_ADDRESS", "UF_RECEPTION", "UF_DOSTUP_TIME", "UF_PHONE"));
while($arSect = $res->GetNext()) {
	if(isset($arData->{'data-element-id'})) {
		if($arData->{'data-element-id'} == $arSect["ID"]) {
			$arSect["CHECKED_ON_MAP"] = "Y";
		}	
	}
	
	$arResult["POINTS"][] = $arSect;
}
?>

<span class="jqmClose top-close fa fa-close"><?=CPriority::showIconSvg(SITE_TEMPLATE_PATH.'/images/include_svg/close.svg');?></span>
<script type="text/javascript">
    ymaps.ready(init);    
    function init(){ 
        var myMap = new ymaps.Map("bigmap", {
            center: [55.76, 37.64],
            zoom: 9,
            controls: []
        });

        <?foreach($arResult["POINTS"] as $key=>$arPoint) {?>
            myPlacemark_<?=$key?> = new ymaps.Placemark([<?=$arPoint["UF_MAP"]?>], {
                hintContent: '<?=$arPoint["NAME"]?>',
                balloonContent: '<div><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/map-logo-w.png" height="16px" style="margin-top: -5px;" /> <span style="font-size: 16px; font-weight: bold;"><?=$arPoint["NAME"]?></span></div>'+
                                '<div><?=$arPoint["UF_ADDRESS"]?></div>'+
                                '<div style="margin-top: 10px; font-weight: bold;">Режим работы:</div>'+
                                '<?if(strlen($arPoint["UF_RECEPTION"])>0) {?><div>Ресепшн: <?=$arPoint["UF_RECEPTION"]?></div><?}?>'+
                                '<?if(strlen($arPoint["UF_DOSTUP_TIME"])>0) {?><div>Доступ на склад: <?=$arPoint["UF_DOSTUP_TIME"]?></div><?}?>'+
                                '<?if(strlen($arPoint["UF_PHONE"])>0) { $phone = preg_replace('/[^\d+]/', '', $arPoint["UF_PHONE"]);?><div>Телефон: <a href="tel:<?=$phone?>" class="dark-color"><?=$arPoint["UF_PHONE"]?></a></div><?}?>',
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#imageWithContent',
                // Своё изображение иконки метки.
                iconImageHref: '<?=SITE_TEMPLATE_PATH?>/images/custom/map-logo-<?=($arPoint["CHECKED_ON_MAP"]=="Y")?"r":"w"?>.png',
                // Размеры метки.
                iconImageSize: [50, 50],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-25, -25],
                // Смещение слоя с содержимым относительно слоя с картинкой.
                iconContentOffset: [0, 0],
            });

            myMap.geoObjects.add(myPlacemark_<?=$key?>);    
        <?}?>
        
        myMap.setBounds(myMap.geoObjects.getBounds(), {useMapMargin: true, zoomMargin: 15});
    }
</script>


<div id="bigmap" class="bx-yandex-view-layout" style="width:100%; height:100%;"></div>





<script>
$(document).ready(function(){
    $('.map_frame').prepend('<div class="overlay_form"><div class="loader"><div class="duo duo1"><div class="dot dot-a"></div><div class="dot dot-b"></div></div><div class="duo duo2"><div class="dot dot-a"></div><div class="dot dot-b"></div></div></div></div>')
    $('.map_frame #bigmap').css('opacity', 0);
    setTimeout(function(){              
        $('.map_frame .overlay_form').remove();
        $('.map_frame #bigmap').css('opacity', 1);
    }, 1800);
});
</script> 