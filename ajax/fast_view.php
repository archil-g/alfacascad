<? define("STATISTIC_SKIP_ACTIVITY_CHECK", "true"); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<? \Bitrix\Main\Loader::includeModule("aspro.next"); ?>
<div class="popup form">
    <div class="wrap">
        <div class="right_slide__content">
            <span class="jqmClose top-close fa fa-close"><?= CPriority::showIconSvg(SITE_TEMPLATE_PATH . '/images/include_svg/close.svg'); ?></span>
            <div class="form-header">
                <div class="text">
                    <div class="title">
                        Быстрый просмотр
                    </div>
                </div>
            </div>
            <?
            if (isset($_GET['iblock_id']) && $_GET['iblock_id']) {
                global $APPLICATION, $arRegion, $arTheme;
                $arRegion = CNextRegionality::getCurrentRegion();
                $arTheme = CNext::GetFrontParametrsValues(SITE_ID);
                $url = htmlspecialcharsbx(urldecode($_GET['item_href']));


                \Bitrix\Main\Loader::includeModule('sale');
                \Bitrix\Main\Loader::includeModule('currency');
                \Bitrix\Main\Loader::includeModule('catalog'); ?>

                <script>
                    var objUrl = parseUrlQuery(),
                        add_url = '?FAST_VIEW=Y';
                    if ('clear_cache' in objUrl) {
                        if (objUrl.clear_cache == 'Y')
                            add_url += '&clear_cache=Y';
                    }
                    BX.ajax({
                        url: '<?=$url;?>' + add_url,
                        method: 'POST',
                        data: BX.ajax.prepareData({'FAST_VIEW': 'Y'}),
                        dataType: 'html',
                        processData: false,
                        start: true,
                        headers: [{'name': 'X-Requested-With', 'value': 'XMLHttpRequest'}],
                        onfailure: function (data) {
                            alert('Error connecting server');
                        },
                        onsuccess: function (html) {
                            var ob = BX.processHTML(html);
                            // inject
                            BX('fast_view_item').innerHTML = ob.HTML;
                            BX.ajax.processScripts(ob.SCRIPT);
                            $('#fast_view_item').closest('.form').addClass('init');
                            $('.fast_view_frame .form-header .title').html($('#fast_view_item .title.hidden').html());

                            InitFlexSlider();
                            /*initCountdown();
                            setBasketStatusBtn();

                            InitZoomPict($('#fast_view_item .zoom_picture'));

                            setTimeout(function(){
                                showTotalSummItem('Y');
                            }, 100);*/
                          /*  $('.slides_block li').on('click', function (e) {
                                e.preventDefault();
                                $('.slides_block li').removeClass('current');
                                $(this).addClass('current');
                                $('.slides li' + $(this).data('href')).addClass('current').siblings().removeClass('current');
                            });
*/
                            $(window).scroll();
                        }
                    })
                    $('.jqmClose').on('click', function (e) {
                        e.preventDefault();
                        $(this).closest('.jqmWindow').jqmHide();
                    })
                </script>
                <div id="fast_view_item" class="scrollbar">
                    <div class="loading_block"></div>
                </div>
            <? } ?>
        </div>
    </div>
</div>