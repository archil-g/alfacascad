<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
global $USER;

if($_REQUEST["ACTION"] == "EMAIL_NOTIFY") {
	if($USER->IsAuthorized()) {
		$oUser = new CUser;
		$oUser->Update($USER->GetID(), Array("UF_EMAIL_NOTIFY" => $_REQUEST["VALUE"]));
	}
}

if($_REQUEST["ACTION"] == "SMS_NOTIFY") {
	if($USER->IsAuthorized()) {	
		$oUser = new CUser;
		$oUser->Update($USER->GetID(), Array("UF_SMS_NOTIFY" => $_REQUEST["VALUE"]));
	}
}
?>