<?define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?if((int)$_REQUEST['ELEMENT_ID'] && (int)$_REQUEST['IBLOCK_ID'] && \Bitrix\Main\Loader::includeModule('aspro.next')):?>
	<?$APPLICATION->IncludeComponent("aspro:oneclickbuy.next", "shop", array(
		"BUY_ALL_BASKET" => "N",
		"IBLOCK_ID" => (int)$_REQUEST["IBLOCK_ID"],
		"ELEMENT_ID" => (int)$_REQUEST["ELEMENT_ID"],
		"ELEMENT_QUANTITY" => (float)$_REQUEST["ELEMENT_QUANTITY"],
		"OFFER_PROPERTIES" => $_REQUEST["OFFER_PROPS"],
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"CACHE_GROUPS" => "N",
		"SHOW_LICENCE" => 'Y',
		"SHOW_DELIVERY_NOTE" => 'N',
		//"PROPERTIES" => (strlen($tmp = COption::GetOptionString('aspro.next', 'ONECLICKBUY_PROPERTIES', 'FIO,PHONE,EMAIL,COMMENT', SITE_ID)) ? explode(',', $tmp) : array()),
		"PROPERTIES" => ['FIO','PHONE','EMAIL','COMMENT'],
		//"REQUIRED" => (strlen($tmp = COption::GetOptionString('aspro.next', 'ONECLICKBUY_REQUIRED_PROPERTIES', 'FIO,PHONE', SITE_ID)) ? explode(',', $tmp) : array()),
		"REQUIRED" => ['FIO','PHONE'],
		"DEFAULT_PERSON_TYPE" => 1,
		"DEFAULT_DELIVERY" => 2,
		"DEFAULT_PAYMENT" => 1,
		"DEFAULT_CURRENCY" => 'RUB',
		"USE_CAPTCHA"=>'N'
		),
		false
	);?>
<?endif;?>