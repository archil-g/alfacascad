<?
global $USER;
?>

<?if(strlen($_REQUEST["webform-id"]) > 0) {?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:form",
		"popup",
		Array(
			"SKLAD_ID" => $_REQUEST["sklad-id"],
			"SQUARE_FROM" => $_REQUEST["square-from"],
			"SQUARE_TO" => $_REQUEST["square-to"],
			"PROP_SIZE" => $_REQUEST["prop-size"],
			"INVOICE_GUID" => $_REQUEST["invoice-guid"],
			"CONTRACT_GUID" => $_REQUEST["contract-guid"],
			"USER_ID" => $USER->GetId(),
			"USER_EMAIL" => $USER->GetEmail(),
			"AJAX_MODE" => "Y",
			"SEF_MODE" => "N",
			"WEB_FORM_ID" => $_REQUEST["webform-id"],
			"START_PAGE" => "new",
			"SHOW_LIST_PAGE" => "N",
			"SHOW_EDIT_PAGE" => "N",
			"SHOW_VIEW_PAGE" => "N",
			"SUCCESS_URL" => "",
			"SHOW_ANSWER_VALUE" => "N",
			"SHOW_ADDITIONAL" => "N",
			"SHOW_STATUS" => "N",
			"EDIT_ADDITIONAL" => "N",
			"EDIT_STATUS" => "Y",
			"NOT_SHOW_FILTER" => "",
			"NOT_SHOW_TABLE" => "",
			"CHAIN_ITEM_TEXT" => "",
			"CHAIN_ITEM_LINK" => "",
			"IGNORE_CUSTOM_TEMPLATE" => "N",
			"USE_EXTENDED_ERRORS" => "Y",
			"CACHE_GROUPS" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3675",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"SHOW_LICENCE" => "Y",
			"LICENCE_TEXT" => "",
			"DISPLAY_CLOSE_BUTTON" => "Y",
			"SUCCESS_MESSAGE" => "",
			"CLOSE_BUTTON_NAME" => "Закрыть",
			"CLOSE_BUTTON_CLASS" => "",
			"VARIABLE_ALIASES" => Array(
				"action" => "action"
			)
		)
	);?>
<?}?> 