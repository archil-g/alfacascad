<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");
Bitrix\Main\Loader::includeModule("iblock");
$IBLOCK_CATALOG_BOX = 40;

if($_REQUEST["ACTION"] == "ADD_BOX") {
	$result = Array();
	if(strlen($_REQUEST["PRODUCT_ID"]) > 0) {
		$basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
		$basketItems = $basket->getBasketItems();
		foreach ($basketItems as $basketItem) {
			$basketItemId = $basketItem->getId();
			$basketItemProductId = $basketItem->getProductId();
			$basketItemIblock = CIBlockElement::GetIBlockByID($basketItemProductId);
			//удаляем все боксы в корзине, т.к. добавляется новый бокс
			if($basketItemIblock == $IBLOCK_CATALOG_BOX) {
				$basket->getItemById($basketItemId)->delete();
				$basket->save();
			}
			//удаляем все счета в корзине
			if($basketItemIblock == INVOICES_IBLOCK) {
				$basket->getItemById($basketItemId)->delete();
				$basket->save();
			}
			//удаляем все пополнения баланса в корзине
			if($basketItemIblock == BALANCE_ITEMS_IBLOCK) {
				$basket->getItemById($basketItemId)->delete();
				$basket->save();
			}			
		}

		$productId = intval($_REQUEST["PRODUCT_ID"]);
		$quantity = intval($_REQUEST["COUNT"]);
		if($quantity <= 0) {
			$quantity = 1;
		}		
		
		//добавляем в корзину новый бокс
		$item = $basket->createItem("catalog", $productId);
		$item->setFields(array(
			'QUANTITY' => $quantity,
			'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
			'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
			'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
		));
		$basket->save();
		
		
		//Рассчитываем правильную цену бокса и записываем доп. свойства для товара в корзине
		calculateBoxPriceInBasket($basket, $item->getId());
		
		
		$result["STATUS"] = "OK";
	} else {
		$result["STATUS"] = "ERROR";
	}
	
	echo json_encode($result);
}


if($_REQUEST["ACTION"] == "UPDATE_SMALL_CART") {
	?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:sale.basket.basket.line", 
		"fly_basket", 
		array(
			"COMPONENT_TEMPLATE" => "fly_basket",
			"PATH_TO_BASKET" => SITE_DIR."cart/",
			"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
			"SHOW_NUM_PRODUCTS" => "Y",
			"SHOW_TOTAL_PRICE" => "Y",
			"SHOW_EMPTY_VALUES" => "Y",
			"SHOW_PERSONAL_LINK" => "N",
			"PATH_TO_PERSONAL" => SITE_DIR."personal/",
			"SHOW_AUTHOR" => "N",
			"PATH_TO_AUTHORIZE" => "",
			"SHOW_REGISTRATION" => "N",
			"PATH_TO_REGISTER" => SITE_DIR."login/",
			"PATH_TO_PROFILE" => SITE_DIR."personal/",
			"SHOW_PRODUCTS" => "N",
			"POSITION_FIXED" => "N",
			"HIDE_ON_BASKET_PAGES" => "Y"
		),
		false,
		array("HIDE_ICONS" => "Y")
	);?>	
	<?
}
?>