<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if($_REQUEST["ACTION"] == "USE_TYPE_FILTER") {

	$section_code = "";
	if(strlen($_REQUEST["UF_USE_TYPE_B"]) > 0) {
		$section_code = "for_business";
	}
	if(strlen($_REQUEST["UF_USE_TYPE_P"]) > 0) {
		$section_code = "storage";
	}

	global $arrUseTypeFilter;
	$arrUseTypeFilter[$_REQUEST["USETYPE_CODE"]] = $_REQUEST["USETYPE_ID"];

	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list", 
		"useblock_for_business", 
		array(
			"ADD_SECTIONS_CHAIN" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COUNT_ELEMENTS" => "N",
			"FILTER_NAME" => "arrUseTypeFilter",
			"IBLOCK_ID" => "41",
			"IBLOCK_TYPE" => "aspro_priority_content",
			"SECTION_CODE" => $section_code,
			"SECTION_FIELDS" => array(
				0 => "NAME",
				1 => "DESCRIPTION",
				2 => "PICTURE",
				3 => "",
			),
			"SECTION_ID" => "",
			"SECTION_URL" => "#CODE#/",
			"SECTION_USER_FIELDS" => array(
				0 => "UF_USE_TEXT",
				1 => "",
				2 => "",
			),
			"SHOW_PARENT_NAME" => "Y",
			"TOP_DEPTH" => "2",
			"VIEW_MODE" => "LINE",
			"COMPONENT_TEMPLATE" => "useblock_for_business",
			"AJAX_LOAD" => "Y"
		),
		false
	);
}
?>