<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Ищите склад в аренду? Или задумались, куда сдать вещи в Москве? АльфаСклад предлагает аренду складов хранения вещей и складские боксы от 1 м3 до 50 м2, по низким ценам от 760 руб. в месяц. 5 пунктов хранения расположены по всей Москве, звоните и заказывайте.");
$APPLICATION->SetPageProperty("keywords", "самосклад, склад хранения вещей москва, аренда склада,  аренда бокса для хранения вещей, складовка, снять складской бокс");
$APPLICATION->SetPageProperty("title", "АльфаСклад - аренда склада для хранения вещей");
$APPLICATION->SetTitle("Главная");
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>