<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<div class="row margin0">
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"front-banners-big",
		array(
			"IBLOCK_TYPE" => "aspro_priority_advt",
			"IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_priority_advt"]["aspro_priority_advtbig"][0],
			"NEWS_COUNT" => "30",
			"SORT_BY1" => "SORT",
			"SORT_ORDER1" => "ASC",
			"SORT_BY2" => "ID",
			"SORT_ORDER2" => "ASC",
			"FILTER_NAME" => "arRegionLink",
			"FIELD_CODE" => array(
				0 => "NAME",
				1 => "PREVIEW_TEXT",
				2 => "PREVIEW_PICTURE",
				3 => "DETAIL_PICTURE",
				4 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "",
				1 => "BANNERTYPE",
				2 => "TEXTCOLOR",
				3 => "LINKIMG",
				4 => "BUTTON1TEXT",
				5 => "BUTTON1CLASS",
				6 => "BUTTON2TEXT",
				7 => "BUTTON2LINK",
				8 => "BUTTON2CLASS",
				9 => "HEADER_COLOR",
				10 => "SECTION",
				11 => "TITLE_H1",
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600000",
			"CACHE_FILTER" => "Y",
			"CACHE_GROUPS" => "N",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"SET_TITLE" => "N",
			"SET_STATUS_404" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "Новости",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "3600000",
			"PAGER_SHOW_ALL" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"COMPONENT_TEMPLATE" => "front-banners-big",
			"SET_BROWSER_TITLE" => "Y",
			"SET_META_KEYWORDS" => "Y",
			"SET_META_DESCRIPTION" => "Y",
			"SET_LAST_MODIFIED" => "N",
			"STRICT_SECTION_CHECK" => "N",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"SHOW_404" => "N",
			"MESSAGE_404" => ""
		),
		false
	);?>
</div>

<?CPriority::ShowPageType('index_component', 'float_banners', 'TOP_FLOAT_BANNERS_INDEX', true);?>

<div class="bg-gray">

	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "inc",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/include/featuresblock_mainpage.php",
			"SECTION_CODE" => "storage"
		),
		false,
		Array("HIDE_ICONS" => "Y")
	);?>


	<div class="hidden-xs">
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.section.list",
			"useblock_mainpage",
			array(
				"ADD_SECTIONS_CHAIN" => "N",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COUNT_ELEMENTS" => "N",
				"FILTER_NAME" => "",
				"IBLOCK_ID" => "41",
				"IBLOCK_TYPE" => "aspro_priority_content",
				"SECTION_CODE" => "storage",
				"SECTION_FIELDS" => array(
					0 => "NAME",
					1 => "",
				),
				"SECTION_ID" => "",
				"SECTION_URL" => "/#SECTION_CODE_PATH#/",
				"SECTION_USER_FIELDS" => array(
					0 => "UF_TEXT_MAINPAGE",
					1 => "UF_PICTURE_MAINPAGE",
					2 => "UF_ICON_MAINPAGE",
					3 => "",
				),
				"SHOW_PARENT_NAME" => "Y",
				"TOP_DEPTH" => "2",
				"VIEW_MODE" => "LINE",
				"COMPONENT_TEMPLATE" => "useblock_mainpage"
			),
			false
		);?>
	</div>

	
	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"mainpage_price_storages",
		array(
			"SQUARE_FROM" => "1",
			"SQUARE_TO" => "3",
			"ADD_SECTIONS_CHAIN" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COUNT_ELEMENTS" => "N",
			"FILTER_NAME" => "",
			"IBLOCK_ID" => "40",
			"IBLOCK_TYPE" => "aspro_priority_catalog",
			"SECTION_CODE" => "",
			"SECTION_FIELDS" => array(
				0 => "NAME",
				1 => "DESCRIPTION",
				2 => "",
			),
			"SECTION_ID" => "",
			"SECTION_URL" => "#CODE#/",
			"SECTION_USER_FIELDS" => array(
				0 => "UF_PHOTOGALLERY",
				1 => "UF_ADDRESS",
				2 => "UF_RECEPTION",
				3 => "UF_DOSTUP_TIME",
				4 => "UF_PHONE",
				5 => "UF_MAP",
				6 => "UF_FLOORS",
				7 => "UF_PRICE_ON_MAP",
				8 => "",
			),
			"SHOW_PARENT_NAME" => "Y",
			"TOP_DEPTH" => "1",
			"VIEW_MODE" => "LINE",
			"COMPONENT_TEMPLATE" => "mainpage_price_storages"
		),
		false
	);?>
	

	
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "inc",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/include/oprosnik.php"
		),
		false,
		Array(
			'HIDE_ICONS' => 'Y'
		)
	);?>
	

	<div class="maxwidth-theme hidden-xs">
		<div class="shadow-box">
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.detail",
				"company_front_1",
				array(
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "N",
					"DISPLAY_PICTURE" => "N",
					"DISPLAY_PREVIEW_TEXT" => "N",
					"IBLOCK_TYPE" => "aspro_priority_content",
					"IBLOCK_ID" => CCache::$arIBlocks[SITE_ID]["aspro_priority_content"]["aspro_priority_static"][0],
					"FIELD_CODE" => array(
						0 => "NAME",
						1 => "PREVIEW_TEXT",
						2 => "PREVIEW_PICTURE",
						3 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "URL",
						2 => "PROPS",
						3 => "SHOW_BUTTON",
						4 => "VIDEO",
						5 => "VIDEO_SRC",
						6 => "VIDEO_SOURCE",
						7 => "COMPANY_NAME",
						8 => "COMPANY_TEXT",
						9 => "",
					),
					"DETAIL_URL" => "",
					"SECTION_URL" => "",
					"META_KEYWORDS" => "-",
					"META_DESCRIPTION" => "-",
					"BROWSER_TITLE" => "-",
					"DISPLAY_PANEL" => "N",
					"SET_CANONICAL_URL" => "N",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"ADD_ELEMENT_CHAIN" => "N",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "3600000",
					"CACHE_FILTER" => "Y",
					"CACHE_GROUPS" => "N",
					"USE_PERMISSIONS" => "N",
					"GROUP_PERMISSIONS" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "",
					"PAGER_SHOW_ALL" => "Y",
					"CHECK_DATES" => "N",
					"ELEMENT_ID" => "112",
					"ELEMENT_CODE" => "",
					"IBLOCK_URL" => "",
					"COUNT_BENEFITS" => "3",
					"COMPONENT_TEMPLATE" => "company_front_1",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"SET_BROWSER_TITLE" => "Y",
					"SET_META_KEYWORDS" => "Y",
					"SET_META_DESCRIPTION" => "Y",
					"SET_LAST_MODIFIED" => "N",
					"STRICT_SECTION_CHECK" => "N",
					"SHOW_ALL_TITLE" => "",
					"MORE_BUTTON_TITLE" => "",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"SHOW_404" => "N",
					"MESSAGE_404" => "",
					"FILTER_NAME" => "arRegionLink",
					"REGION" => ""
				),
				false
			);?>
		</div>
	</div>


	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"storage_list_withmap",
		array(
			"ADD_SECTIONS_CHAIN" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COUNT_ELEMENTS" => "N",
			"FILTER_NAME" => "",
			"IBLOCK_ID" => "40",
			"IBLOCK_TYPE" => "aspro_priority_catalog",
			"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
			"SECTION_FIELDS" => array(
				0 => "NAME",
				1 => "DESCRIPTION",
				2 => "",
			),
			"SECTION_ID" => "",
			"SECTION_URL" => "#CODE#/",
			"SECTION_USER_FIELDS" => array(
				0 => "UF_PHOTOGALLERY",
				1 => "UF_ADDRESS",
				2 => "UF_RECEPTION",
				3 => "UF_DOSTUP_TIME",
				4 => "UF_PHONE",
				5 => "UF_MAP",
				6 => "UF_PRICE_ON_MAP",
				7 => "",
			),
			"SHOW_PARENT_NAME" => "Y",
			"TOP_DEPTH" => "1",
			"VIEW_MODE" => "LINE",
			"COMPONENT_TEMPLATE" => "storage_list_withmap"
		),
		false
	);?>

	<div class="hidden-xs">
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list",
			"partners_front_1_v2",
			array(
				"COMPONENT_TEMPLATE" => "partners_front_1_v2",
				"IBLOCK_TYPE" => "aspro_priority_content",
				"IBLOCK_ID" => "14",
				"NEWS_COUNT" => "20",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "ASC",
				"SORT_BY2" => "ID",
				"SORT_ORDER2" => "ASC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array(
					0 => "NAME",
					1 => "PREVIEW_PICTURE",
					2 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_LAST_MODIFIED" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "N",
				"STRICT_SECTION_CHECK" => "N",
				"SHOW_DETAIL_LINK" => "N",
				"TITLE" => "Нам доверяют",
				"SHOW_ALL_TITLE" => "",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"SET_STATUS_404" => "N",
				"SHOW_404" => "N",
				"MESSAGE_404" => ""
			),
			false
		);?>
	</div>

	<?
	global $arrFilterGalleryMainpage;
	$arrFilterGalleryMainpage["!PROPERTY_MAINPAGE"] = false;
	?>
	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"gallery_mainpage",
		array(
			"COMPONENT_TEMPLATE" => "gallery_mainpage",
			"IBLOCK_TYPE" => "aspro_priority_content",
			"IBLOCK_ID" => "43",
			"NEWS_COUNT" => "20",
			"SORT_BY1" => "SORT",
			"SORT_ORDER1" => "ASC",
			"SORT_BY2" => "ID",
			"SORT_ORDER2" => "DESC",
			"FILTER_NAME" => "arrFilterGalleryMainpage",
			"FIELD_CODE" => array(
				0 => "NAME",
				1 => "DETAIL_PICTURE",
				2 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "",
				1 => "",
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"SET_TITLE" => "N",
			"SET_BROWSER_TITLE" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_LAST_MODIFIED" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "N",
			"STRICT_SECTION_CHECK" => "N",
			"DISPLAY_DATE" => "N",
			"DISPLAY_NAME" => "N",
			"DISPLAY_PICTURE" => "N",
			"DISPLAY_PREVIEW_TEXT" => "N",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "Новости",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"SET_STATUS_404" => "N",
			"SHOW_404" => "N",
			"MESSAGE_404" => ""
		),
		false
	);?>

	<div style="margin-top:40px;"></div>
	<?CPriority::ShowPageType('index_component', 'info', 'CONSULT_INDEX', true);?>
</div>

