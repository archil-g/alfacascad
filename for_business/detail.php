<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Складские помещения Альфасклад предназначены и для хранения имущества. Вы можете арендовать складское помещение под склад для бизнеса, просто позвонив нам по телефону.");
$APPLICATION->SetPageProperty("keywords", "складские помещения, складское помещение, хранение имущества, складские помещения в Москве, снять складское помещение, аренда складских помещений, аренда складских помещений в Москве, складские помещения в аренду");
$APPLICATION->SetPageProperty("title", "Складские помещения для хранения имущества");
$APPLICATION->SetTitle("Складские помещения для хранения имущества");
?>
<section class="page-top maxwidth-theme">
	<div class="row">
		<div class="col-md-12">
			<?$APPLICATION->IncludeComponent(
				"bitrix:breadcrumb", 
				"corp", 
				array(
					"COMPONENT_TEMPLATE" => "corp",
					"START_FROM" => "0",
					"PATH" => "",
					"SITE_ID" => "s1"
				),
				false
			);?>
			<br>
			<div class="page-top-main">
				<h1 id="pagetitle"><?$APPLICATION->ShowTitle(false)?></h1>
			</div>
		</div>
	</div>
</section>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"useblock_detail", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "41",
		"IBLOCK_TYPE" => "aspro_priority_content",
		"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
		"SECTION_FIELDS" => array(
			0 => "NAME",
			1 => "",
			2 => "",
		),
		"SECTION_ID" => "",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_TISERS_ITEMS",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "useblock_detail"
	),
	false
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>