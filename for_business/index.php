<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Нужна аренда склада для бизнеса в Москве? АльфаСклад предлагает снять боксы бизнесменам для хранения личных вещей на выгодных условиях ✔ Без риэлторов и переплат ✔ Онлайн подбор боксов по районам Москвы ✔ Онлайн консультации и заказ склада 8 (495) 580-20-47.");
$APPLICATION->SetPageProperty("keywords", "аренда склада для бизнеса, склад для бизнеса");
$APPLICATION->SetPageProperty("title", "Аренда склада для бизнеса. Склады для малого и среднего бизнеса в Москве | АльфаСклад");
$APPLICATION->SetTitle("Аренда склада для бизнеса");
?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"topblock_for_business",
	array(
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COMPONENT_TEMPLATE" => "topblock_for_business",
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"FILTER_NAME" => "",	// Имя массива со значениями фильтра разделов
		"IBLOCK_ID" => "41",	// Инфоблок
		"IBLOCK_TYPE" => "aspro_priority_content",	// Тип инфоблока
		"SECTION_CODE" => "for_business",	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "NAME",
			1 => "DESCRIPTION",
			2 => "PICTURE",
			3 => "",
		),
		"SECTION_ID" => "",	// ID раздела
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "UF_USE_TYPE_B",
			1 => "UF_USE_TYPE_P",
			2 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LINE"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"fixed_custom_menu",
	array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "contentMenu",	// Тип меню для остальных уровней
		"COMPONENT_TEMPLATE" => "fixed_custom_menu",
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "contentMenu",	// Тип меню для первого уровня
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	)
);?>
<div class="bg-gray">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"tisersblock_for_business",
	array(
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COMPONENT_TEMPLATE" => "tisersblock_for_business",
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"FILTER_NAME" => "",	// Имя массива со значениями фильтра разделов
		"IBLOCK_ID" => "41",	// Инфоблок
		"IBLOCK_TYPE" => "aspro_priority_content",	// Тип инфоблока
		"SECTION_CODE" => "for_business",	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "NAME",
			1 => "DESCRIPTION",
			2 => "PICTURE",
			3 => "",
		),
		"SECTION_ID" => "",	// ID раздела
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "UF_TISERS_TITLE",
			1 => "UF_TISERS_TEXT",
			2 => "UF_TISERS_GALLERY",
			3 => "UF_TISERS_ITEMS",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LINE"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",	// Суффикс имени файла включаемой области
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "/include/tabs_formManagerOrder.php",
		"SHADOW_BOX" => "Y"
	),
	false,
	array("HIDE_ICONS" => "Y")
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"useblock_for_business",
	array(
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COMPONENT_TEMPLATE" => "useblock_for_business",
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"FILTER_NAME" => "",	// Имя массива со значениями фильтра разделов
		"IBLOCK_ID" => "41",	// Инфоблок
		"IBLOCK_TYPE" => "aspro_priority_content",	// Тип инфоблока
		"SECTION_CODE" => "for_business",	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "NAME",
			1 => "DESCRIPTION",
			2 => "PICTURE",
			3 => "",
		),
		"SECTION_ID" => "",	// ID раздела
		"SECTION_URL" => "#CODE#/",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "UF_USE_TEXT",
			1 => "",
			2 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LINE"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",	// Суффикс имени файла включаемой области
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "/include/featuresblock_for_business.php",
		"SECTION_CODE" => "for_business"
	),
	false,
	array("HIDE_ICONS" => "N")
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"storage_list_withmap",
	array(
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COMPONENT_TEMPLATE" => "storage_list_withmap",
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"FILTER_NAME" => "",	// Имя массива со значениями фильтра разделов
		"IBLOCK_ID" => "40",	// Инфоблок
		"IBLOCK_TYPE" => "aspro_priority_catalog",	// Тип инфоблока
		"SECTION_CODE" => "",	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "NAME",
			1 => "DESCRIPTION",
			2 => "",
		),
		"SECTION_ID" => "",	// ID раздела
		"SECTION_URL" => "#CODE#/",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "UF_PHOTOGALLERY",
			1 => "UF_ADDRESS",
			2 => "UF_RECEPTION",
			3 => "UF_DOSTUP_TIME",
			4 => "UF_PHONE",
			5 => "UF_MAP",
			6 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LINE"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"partners_front_1_v2",
	array(
	"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"COMPONENT_TEMPLATE" => "partners_front_1_v2",
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "14",	// Код информационного блока
		"IBLOCK_TYPE" => "aspro_priority_content",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "20",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SHOW_ALL_TITLE" => "",
		"SHOW_DETAIL_LINK" => "N",
		"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
		"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"TITLE" => "Нам доверяют"
	)
);?>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>