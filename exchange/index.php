<?php

use Enum\Helper;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


class Exchange
{
    public static function GetKey()
    {
        return md5(date('d.m.Y') . 'alfasklad');
    }

    public $file_recive_deal = '/logs/resiveDealFromB24.txt';
    public $file_recive_contact = '/logs/resiveContactFromB24.txt';
    public $file_recive_contract = '/logs/resiveContractFromB24.txt';
    public $file_recive_invoice = '/logs/resiveInvoiceFromB24.txt';

    public $key;

    public function start()
    {
        $this->key = $this->GetKey();
        $keyRequest = $_REQUEST['key'];

        if ($keyRequest == $this->key) {

            $action = htmlspecialchars($_REQUEST['action']);

            switch ($action) {
                case 'addOrder' :
                    $data = $_REQUEST;
                    $this->AddOrder($data);
                    break;
                case 'addcontact' :
                    $arrCompany = $_REQUEST['dataCompany'];
                    $this->AddContact($arrCompany);
                    break;
                case 'addContract' :
                    $data = $_REQUEST;
                    $this->AddContract($data);
                    break;
                case 'addInvoice' :
                    $data = $_REQUEST;
                    $this->AddInvoice($data);
                    break;					
            }
        }
    }

    //создание контакта который прилетает из Б24 из сделки
    function AddContact($arrCompany)
    {
        Helper::log($this->file_recive_contact, $arrCompany);

        \Bitrix\Main\Loader::includeModule("sale");

        $email = $arrCompany['EMAIL'];
        $phone = $arrCompany['PHONE'];

        if ($arrCompany['PROFILE_TYPE'] == 1) {
			if((strlen($arrCompany['PASSPORT_SERIES']) > 0) and (strlen($arrCompany['PASSPORT_NUMBER']) > 0)) {
				$PROFILE = $this->getUserByPassport($arrCompany['PASSPORT_SERIES'], $arrCompany['PASSPORT_NUMBER']);
				$PROFILE_ID = $PROFILE['PROFILE_ID'];
				$USER_ID = $PROFILE['USER_ID'];
			}
        }
        if ($arrCompany['PROFILE_TYPE'] == 2) {
			if((strlen($arrCompany['INN']) > 0) and (strlen($arrCompany['KPP']) > 0)) {
				$PROFILE = $this->getUserByInnAndKpp($arrCompany['INN'], $arrCompany['KPP']);
				$PROFILE_ID = $PROFILE['PROFILE_ID'];
				$USER_ID = $PROFILE['USER_ID'];
			}
        }
		if(!$USER_ID) {
			//пробуем найти пользователя по телефону в профиле покупателя
			$PROFILE = $this->getUserByPhone($phone);
			$PROFILE_ID = $PROFILE['PROFILE_ID'];
			$USER_ID = $PROFILE['USER_ID'];			
		}

        //если не нашли пользователя то создаем нового + профиль
        if (!$USER_ID) {

            if ($USER_ID = $this->getUserByEmail($email)) {
				Helper::log($this->file_recive_contact, 'Пользователь с email ' . $email . ' уже существует, берем его ID = ' . $USER_ID);
				
				//обновляем ФИО пользователя
				$user = new \CUser;
				$arFields = Array(
					"NAME" => $arrCompany['NAME'],
					"LAST_NAME" => $arrCompany['LAST_NAME'],
					"SECOND_NAME" => $arrCompany['SECOND_NAME']
				);
				$user->Update($USER_ID, $arFields);
				Helper::log($this->file_recive_contact, 'Обновили пользователя ' . $USER_ID);
				
            } else {
				$user = new \CUser;
				$arFields = Array(
					"NAME" => $arrCompany['NAME'],
					"LAST_NAME" => $arrCompany['LAST_NAME'],
					"SECOND_NAME" => $arrCompany['SECOND_NAME'],
					"EMAIL" => $email,
					"LOGIN" => $email,
					"LID" => "ru",
					"ACTIVE" => "Y",
					"GROUP_ID" => array(3, 4),
					"PASSWORD" => "123456",
					"CONFIRM_PASSWORD" => "123456",
				);

				$USER_ID = $user->Add($arFields);

				if ($USER_ID > 0) {
					Helper::log($this->file_recive_contact, 'Создали пользователя ' . $USER_ID);
				} else {
					Helper::log($this->file_recive_contact, 'Контакт не создан!!! ' . $user->LAST_ERROR);
				}
            }

            //создаём профиль
            //PERSON_TYPE_ID - идентификатор типа плательщика, для которого создаётся профиль
            $arProfileFields = array(
                "NAME" => "Профиль покупателя.",
                "USER_ID" => $USER_ID,
                "PERSON_TYPE_ID" => $arrCompany['PROFILE_TYPE']
            );
            $PROFILE_ID = \CSaleOrderUserProps::Add($arProfileFields);

            if ($PROFILE_ID > 0) {
                Helper::log($this->file_recive_contact, 'Создали профиль ' . $PROFILE_ID);
            } else {
                Helper::log($this->file_recive_contact, 'Профиль не создан!!!');
            }
            //если профиль создан
            if ($PROFILE_ID) {
                //физ лицо
                if ($arrCompany['PROFILE_TYPE'] == 1) {
                    //формируем массив свойств
                    $PROPS = Array(
                        array(
                            //LAST_NAME
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 1,
                            "NAME" => "Фамилия по паспорту (для заключения договора)",
                            "VALUE" => $arrCompany['LAST_NAME']
                        ),
                        array(
                            //NAME
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 2,
                            "NAME" => "Имя по паспорту (для заключения договора)",
                            "VALUE" => $arrCompany['NAME']
                        ),
                        array(
                            //SECOND_NAME
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 3,
                            "NAME" => "Отчество по паспорту (для заключения договора)",
                            "VALUE" => $arrCompany['SECOND_NAME']
                        ),
                        array(
                            //SECOND_NAME
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 5,
                            "NAME" => "Адрес регистрации",
                            "VALUE" => $arrCompany['REQUSITES']['ADDR'][4]['ADDRESS_1']
                        ),
                        array(
                            //SECOND_NAME
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 6,
                            "NAME" => "Фактический адрес",
                            "VALUE" => $arrCompany['REQUSITES']['ADDR'][1]['ADDRESS_1']
                        ),
                        array(
                            //PHONE
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 7,
                            "NAME" => "Телефон",
                            "VALUE" => $phone
                        ),
                        array(
                            //EMAIL
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 8,
                            "NAME" => "E-mail",
                            "VALUE" => $email
                        ),
                        array(
                            //PASSPORT_SERIES
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 9,
                            "NAME" => "Серия паспорта (для заключения договора)",
                            "VALUE" => $arrCompany['PASSPORT_SERIES']
                        ),
                        array(
                            //PASSPORT_NUMBER
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 10,
                            "NAME" => "Номер паспорта (для заключения договора)",
                            "VALUE" => $arrCompany['PASSPORT_NUMBER']
                        )
                    );
                }
                //юр лицо
                if ($arrCompany['PROFILE_TYPE'] == 2) {
                    //формируем массив свойств
                    $PROPS = Array(
                        array(
                            //	INN
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 11,
                            "NAME" => "ИНН",
                            "VALUE" => $arrCompany['INN']
                        ),
                        array(
                            //	KPP
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 12,
                            "NAME" => "КПП",
                            "VALUE" => $arrCompany['KPP']
                        ),
                        array(
                            //	NAME
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 13,
                            "NAME" => "Наименование компании",
                            "VALUE" => $arrCompany['TITLE']
                        ),
                        array(
                            //	EMAIL
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 14,
                            "NAME" => "E-mail",
                            "VALUE" => $arrCompany['EMAIL']
                        ),
                        array(
                            //	PHONE
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 15,
                            "NAME" => "Телефон",
                            "VALUE" => $arrCompany['PHONE']
                        )
                        /*array(
                            //	FIO_IN_COMPANY
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 15,
                            "NAME" => "ФИО контактного лица",
                            "VALUE" => $arrCompany['TITLE']
                        ),*/
                    );
                }
                //добавляем значения свойств к созданному ранее профилю
                foreach ($PROPS as $prop)
                    \CSaleOrderUserPropsValue::Add($prop);
            }
        } else {
            if ($arrCompany['PROFILE_TYPE'] == 1) {
                //формируем массив свойств
                $PROPS = Array(
                    array(
                        //LAST_NAME
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 1,
                        "NAME" => "Фамилия по паспорту (для заключения договора)",
                        "VALUE" => $arrCompany['LAST_NAME']
                    ),
                    array(
                        //NAME
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 2,
                        "NAME" => "Имя по паспорту (для заключения договора)",
                        "VALUE" => $arrCompany['NAME']
                    ),
                    array(
                        //SECOND_NAME
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 3,
                        "NAME" => "Отчество по паспорту (для заключения договора)",
                        "VALUE" => $arrCompany['SECOND_NAME']
                    ),
                    array(
                        //SECOND_NAME
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 5,
                        "NAME" => "Адрес регистрации",
                        "VALUE" => $arrCompany['REQUSITES']['ADDR'][4]['ADDRESS_1']
                    ),
                    array(
                        //SECOND_NAME
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 6,
                        "NAME" => "Фактический адрес",
                        "VALUE" => $arrCompany['REQUSITES']['ADDR'][1]['ADDRESS_1']
                    ),
                    array(
                        //PHONE
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 7,
                        "NAME" => "Телефон",
                        "VALUE" => $phone
                    ),
                    array(
                        //EMAIL
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 8,
                        "NAME" => "E-mail",
                        "VALUE" => $email
                    ),
                    array(
                        //PASSPORT_SERIES
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 9,
                        "NAME" => "Серия паспорта (для заключения договора)",
                        "VALUE" => $arrCompany['PASSPORT_SERIES']
                    ),
                    array(
                        //PASSPORT_NUMBER
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 10,
                        "NAME" => "Номер паспорта (для заключения договора)",
                        "VALUE" => $arrCompany['PASSPORT_NUMBER']
                    )
                );
            }
            //юр лицо
            if ($arrCompany['PROFILE_TYPE'] == 2) {
                //формируем массив свойств
                $PROPS = Array(
                    array(
                        //	INN
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 11,
                        "NAME" => "ИНН",
                        "VALUE" => $arrCompany['INN']
                    ),
                    array(
                        //	KPP
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 12,
                        "NAME" => "КПП",
                        "VALUE" => $arrCompany['KPP']
                    ),
                    array(
                        //	NAME
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 13,
                        "NAME" => "Наименование компании",
                        "VALUE" => $arrCompany['TITLE']
                    ),
                    array(
                        //	EMAIL
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 14,
                        "NAME" => "E-mail",
                        "VALUE" => $arrCompany['EMAIL']
                    ),
                    array(
                        //	PHONE
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 15,
                        "NAME" => "Телефон",
                        "VALUE" => $arrCompany['PHONE']
                    )
                    /*array(
                        //	FIO_IN_COMPANY
                        "USER_PROPS_ID" => $PROFILE_ID,
                        "ORDER_PROPS_ID" => 15,
                        "NAME" => "ФИО контактного лица",
                        "VALUE" => $arrCompany['TITLE']
                    ),*/
                );
            }

            foreach ($PROPS as $prop) {
                $errors = [];
                $properties[$prop['ORDER_PROPS_ID']] = $prop['VALUE'];
            }

            \CSaleOrderUserProps::DoSaveUserProfile(
                $USER_ID,
                $PROFILE_ID,
                'Профиль',
                $arrCompany['PROFILE_TYPE'],
                $properties,
                $errors
            );

            Helper::log($this->file_recive_contact, 'Обновление профиля покупателя с ID='.$PROFILE_ID);			
			//обновляем ФИО пользователя
			$user = new \CUser;
			$arFields = Array(
				"NAME" => $arrCompany['NAME'],
				"LAST_NAME" => $arrCompany['LAST_NAME'],
				"SECOND_NAME" => $arrCompany['SECOND_NAME']
			);
			$user->Update($USER_ID, $arFields);						
            Helper::log($this->file_recive_contact, 'Обновление профиля пользователя с ID='.$USER_ID);
            Helper::log($this->file_recive_contact, $properties);

        }

        Helper::log($this->file_recive_contact, '###############################################');

        return $USER_ID;
    }

    //создание заказа который прилетает из Б24
    function AddOrder($data)
    {
        Helper::log($this->file_recive_deal, $data);
        $id_contact = $this->AddContact($data['dataCompany']);

        if (!$id_contact) {
            Helper::log($this->file_recive_deal, 'Не найден пользователь! Заказ не создан!');
            Helper::log($this->file_recive_deal, $data);
            die();
        }

        $order = $data['order'];
        $person_type_id = $order['dataCompany']['PROFILE_TYPE'];
		
		$isNewOrder = true;
		$orderID = intval($order["ID Заказа"]);
		if($orderID > 0) {
			$myOrder = \Bitrix\Sale\Order::load($orderID);
			if(!empty($myOrder)) {
				$isNewOrder = false;
			}			
		}

		$products = Array();
        foreach ($order['PRODUCTS'] as $product) {
            $product_id = $this->getProductByID($product['XML_ID']);
			if(strlen($product['PRICE_MAIN']) > 0) {
				$price = $product['PRICE_MAIN'];
			} else {
				$price = $product['PRICE'] * $product['QUANTITY'];
			}
            $products[] = [
                "PRODUCT_ID" => $product_id,
                "NAME" => $product['PRODUCT_NAME'],
                "PRICE" => $price,
                "CURRENCY" => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                "LID" => Bitrix\Main\Context::getCurrent()->getSite(),
                "PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider",
                "QUANTITY" => "1",
                "CUSTOM_PRICE" => "Y",
            ];
        }
		
		if(count($products) > 0) {
			if($isNewOrder) {
				//добавляем новый заказ
				Helper::log($this->file_recive_deal, 'Создание нового заказа');
				$basket = \Bitrix\Sale\Basket::create(SITE_ID);

				foreach ($products as $product) {
					$item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
					unset($product["PRODUCT_ID"]);
					$item->setFields($product);
				}

				$order = \Bitrix\Sale\Order::create(SITE_ID, $id_contact);
				$order->setPersonTypeId($person_type_id);
				$order->setBasket($basket);
				$propertyCollection = $order->getPropertyCollection();
				foreach ($propertyCollection as $propertyItem) {
					if($propertyItem->getField("CODE") == "IS_ORDER_FROM_CRM") {
						$propertyItem->setValue("Y");
					}
				}

				$result = $order->save();
				$orderId = $order->getId();
				$orderUserId = $order->getUserId();
				if (!$result->isSuccess()) {
					$errors = $result->getErrors();
					Helper::log($this->file_recive_deal, 'Заказ не создан!');
					Helper::log($this->file_recive_deal, $errors);
					die();
				}
			} else {
				//обновляем имеющийся заказ
				Helper::log($this->file_recive_deal, 'Обновление заказа с ID = '.$orderId);
				
				$order = Sale\Order::load($orderId);
				$basket = $order->getBasket();
				$basketItems = $basket->getBasketItems();
				//очищаем корзину заказа
				foreach ($basketItems as $basketItem) {
					$basket->getItemById($basketItem->getId())->delete();
				}
				//добавляем товары из сделки
				foreach ($products as $product) {
					$item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
					unset($product["PRODUCT_ID"]);
					$item->setFields($product);
				}
				$order->setBasket($basket);	
				$result = $order->save();
				$orderUserId = $order->getUserId();
				if (!$result->isSuccess()) {
					$errors = $result->getErrors();
					Helper::log($this->file_recive_deal, 'Заказ не создан!');
					Helper::log($this->file_recive_deal, $errors);
					die();
				}				
			}
		}
		
		$returnData = Array("ORDER_ID"=>$orderId, "USER_ID"=>$orderUserId);
		Helper::log($this->file_recive_deal, 'Возвращаем данные в Б24');
		Helper::log($this->file_recive_deal, $returnData);		
		
		echo json_encode($returnData);

    }

    //получить id товара по внешнему коду
    public function getProductByID($external_id)
    {
        $return = false;
        if (!$external_id)
            return false;

        $arFilter = [
            'IBLOCK_ID' => Array(40, 48),
            'XML_ID' => $external_id,
        ];

        $arSelect = [
            'ID',
            'IBLOCK_ID',
            'XML_ID',
        ];

        $res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        if ($item = $res->Fetch()) {
            $return = $item['ID'];
        }

        return $return;
    }

    //получить пользователя по почте
    public function getUserByEmail($email)
    {
        $ID = false;
		if(strlen($email) > 0) {	
			$b = "";
			$o = "";
			$res = \CUser::GetList($b, $o, array(
				"=EMAIL" => $email,
				"EXTERNAL_AUTH_ID" => ''
			), array(
				"FIELDS" => array("ID")
			));
			while ($ar = $res->Fetch()) {
				if (intval($ar["ID"]) !== $ID)
					$ID = $ar["ID"];
			}
		}

        return $ID;
    }

    //получить пользователя по серии и номеру паспорта
    public function getUserByPassport($ser_passport, $num_passport)
    {
        global $DB;

        $strSql =
            "SELECT * " .
            "FROM b_sale_user_props_value UP JOIN b_sale_user_props U ON UP.USER_PROPS_ID=U.ID " .
            "WHERE UP.ORDER_PROPS_ID = 9 AND VALUE = '" . $ser_passport . "' AND UP.USER_PROPS_ID=(SELECT UP2.USER_PROPS_ID FROM b_sale_user_props_value UP2 WHERE UP2.ORDER_PROPS_ID = 10 AND VALUE = '" . $num_passport . "' AND UP.USER_PROPS_ID=UP2.USER_PROPS_ID)";


        $dbRes = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        if ($arRes = $dbRes->Fetch()) {
            return [
                'PROFILE_ID' => $arRes['USER_PROPS_ID'],
                'USER_ID' => $arRes['USER_ID']
            ];
        }

        return false;
    }

    //получить пользователя по ИНН и КПП
    public function getUserByInnAndKpp($inn, $kpp)
    {
        global $DB;

        $strSql =
            "SELECT * " .
            "FROM b_sale_user_props_value UP JOIN b_sale_user_props U ON UP.USER_PROPS_ID=U.ID " .
            "WHERE UP.ORDER_PROPS_ID = 11 AND VALUE = '" . $inn . "' AND UP.USER_PROPS_ID=(SELECT UP2.USER_PROPS_ID FROM b_sale_user_props_value UP2 WHERE UP2.ORDER_PROPS_ID = 12 AND VALUE = '" . $kpp . "' AND UP.USER_PROPS_ID=UP2.USER_PROPS_ID)";

        $dbRes = $DB->Query($strSql);
        if ($arRes = $dbRes->Fetch()) {
            return [
                'PROFILE_ID' => $arRes['USER_PROPS_ID'],
                'USER_ID' => $arRes['USER_ID']
            ];
        }
        return false;

    }
	
	//получить пользователя по номеру телефона
	public function getUserByPhone($phone)
	{
		global $DB;
		$result = false;

		if(strlen($phone) > 0) {
			//физ. л.
			$strSql =
				"SELECT * " .
				"FROM b_sale_user_props_value UP JOIN b_sale_user_props U ON UP.USER_PROPS_ID=U.ID " .
				"WHERE UP.ORDER_PROPS_ID = 7 AND VALUE = '" . $phone . "'";

			$dbRes = $DB->Query($strSql);
			if ($arRes = $dbRes->Fetch()) {
				$result = Array(
					'PROFILE_ID' => $arRes['USER_PROPS_ID'],
					'USER_ID' => $arRes['USER_ID']				
				);
			}
			
			//юр. л.
			if(!$result) {
				$strSql =
					"SELECT * " .
					"FROM b_sale_user_props_value UP JOIN b_sale_user_props U ON UP.USER_PROPS_ID=U.ID " .
					"WHERE UP.ORDER_PROPS_ID = 15 AND VALUE = '" . $phone . "'";

				$dbRes = $DB->Query($strSql);
				if ($arRes = $dbRes->Fetch()) {
					$result = Array(
						'PROFILE_ID' => $arRes['USER_PROPS_ID'],
						'USER_ID' => $arRes['USER_ID']				
					);
				}			
			}
		}
		
		return $result;
	}	
	
	
	//создание/обновление договора, прилетевшего из Б24
    function AddContract($data)
    {
		Helper::log($this->file_recive_contract, $data);
		
		\Bitrix\Main\Loader::includeModule("iblock");
		
		$dataDoc = $data["dataDoc"];
		
		$error = false;
		if(strlen($dataDoc["ID_SITE_COMPANY"]) == 0) {
			$error = true;
			Helper::log($this->file_recive_contract, "Пользователь неизвестен, договор не добавлен!");
		}
		if(strlen($dataDoc["VNESHNIY_KOD_BOKSA"]) == 0) {
			$error = true;
			Helper::log($this->file_recive_contract, "Внешний код бокса неизвестен, договор не добавлен!");
		}
		if(strlen($dataDoc["GUID_CONTRACT"]) == 0) {
			$error = true;
			Helper::log($this->file_recive_contract, "GUID договора неизвестен, договор не добавлен!");
		}
		
		if(!$error) {
			$box_id = $this->getProductByID($dataDoc["VNESHNIY_KOD_BOKSA"]);
			if($box_id) {
				//формируем массив с данными для записи
				$contractStatusId = "";
				if($dataDoc["STATUS_ID"] == "112") {
					$contractStatusId = "352";
				} elseif($dataDoc["STATUS_ID"] == "113") {
					$contractStatusId = "353";
				}
				$arSaveData = Array(
					"IBLOCK_ID" => 52,
					"IBLOCK_SECTION_ID" => false,
					"PROPERTY_VALUES"=> Array(
						"BOX" => $box_id,
						"USER" => $dataDoc["ID_SITE_COMPANY"],
						"STATUS" => $contractStatusId,
						"NUMBER" => $dataDoc["NAME"],
						"DATE_CREATE" => $dataDoc["DATA_DOGOVORA"],
						"PAID_DATE_TO" => $dataDoc["DATA_OKONCHANIYA_OPLATY_DOGOVORA"],
						"BALANCE" => explode("|", $dataDoc["BALANS_PO_DOGOVORU"])[0],
						"COUNTERAGENT_GUID" => $dataDoc["GUID_COMPANY"],
						"CONTRACT_GUID" => $dataDoc["GUID_CONTRACT"],
					),
					"NAME" => $dataDoc["NAME"],
					"ACTIVE" => "Y"
				);
			
				//ищем, есть ли в БД договор с таким GUID_CONTRACT
				$contract_id = $this->getContractByGuid($dataDoc["GUID_CONTRACT"]);	
				if($contract_id) {
					//обновляем договор
					$el = new CIBlockElement;
					if($res = $el->Update($contract_id, $arSaveData)) {
						Helper::log($this->file_recive_contract, "Обновлен договор с ID=".$contract_id);
					} else {
						Helper::log($this->file_recive_contract, "Не удалось обновить договор! ".$el->LAST_ERROR);
					}					
				} else {
					//добавляем договор
					$el = new CIBlockElement;
					if($contract_id = $el->Add($arSaveData)) {
						Helper::log($this->file_recive_contract, "Добавлен новый договор с ID=".$contract_id);
					} else {
						Helper::log($this->file_recive_contract, "Не удалось добавить договор! ".$el->LAST_ERROR);
					}
				}
			} else {
				Helper::log($this->file_recive_contract, "Бокс с таким внешним кодом не найден, договор не добавлен!");
			}		
		}	
		
		Helper::log($this->file_recive_contract, "######################################");
	}
	
	//получить ID договора по его GUID
	public function getContractByGuid($guid) 
	{
		$return = false;
		if (!$guid)
			return false;

		$arFilter = [
			'IBLOCK_ID' => 52,
			'PROPERTY_CONTRACT_GUID' => $guid,
		];

		$arSelect = [
			'ID',
			'IBLOCK_ID'
		];

		$res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
		if ($item = $res->Fetch()) {
			$return = $item['ID'];
		}

		return $return;
	}	
	
	
	//создание/обновление счета, прилетевшего из Б24
    function AddInvoice($data)
    {
		\Bitrix\Main\Loader::includeModule("iblock");
		\Bitrix\Main\Loader::includeModule("catalog");
		\Bitrix\Main\Loader::includeModule("sale");
		
		Helper::log($this->file_recive_invoice, $data);		
				
		$dataDoc = $data["dataDoc"];
		
		$error = false;
		if(strlen($dataDoc["COMPANY"]["ID_SITE_COMPANY"]) == 0) {	
			$error = true;
			Helper::log($this->file_recive_invoice, "Пользователь неизвестен, договор не добавлен!");
		}
		if(strlen($dataDoc["GUID_INVOICE"]) == 0) {
			$error = true;
			Helper::log($this->file_recive_invoice, "GUID счета неизвестен, договор не добавлен!");
		}
		if(strlen($dataDoc["GUID_CONTRACT"]) == 0) {
			$error = true;
			Helper::log($this->file_recive_invoice, "GUID договора неизвестен, договор не добавлен!");
		}

		if(!$error) {
			//формируем массив с данными для записи
			$invoiceStatusId = "";
			if($dataDoc["STATUS_ID"] == "N") {
				$invoiceStatusId = "354";
			} elseif($dataDoc["STATUS_ID"] == "P") {
				$invoiceStatusId = "355";
			} elseif($dataDoc["STATUS_ID"] == "1") {
				$invoiceStatusId = "356";
			} elseif($dataDoc["STATUS_ID"] == "D") {
				$invoiceStatusId = "400";
			}
			
			$arSaveData = Array(
				"IBLOCK_ID" => 53,
				"IBLOCK_SECTION_ID" => false,
				"PROPERTY_VALUES"=> Array(
					"USER" => $dataDoc["COMPANY"]["ID_SITE_COMPANY"],
					"PROFILE_TYPE" => ($dataDoc["COMPANY"]["PROFILE_TYPE"] == "1") ? "398":"399",
					"INVOICE_GUID" => $dataDoc["GUID_INVOICE"],
					"CONTRACT_GUID" => $dataDoc["GUID_CONTRACT"],
					"STATUS" => $invoiceStatusId,
					"NUMBER" => $dataDoc["ACCOUNT_NUMBER"],
					"DATE_CREATE" => $dataDoc["DATE_CREATE"],
					"CONTRACT_NUMBER" => "", // ???
					"DATE_FROM" => $dataDoc["DATE_BEGIN_OF_RENT"],
					"DATE_TO" => $dataDoc["DATE_END_OF_RENT"],
					"TYPE" => "", // ???
					"DATE_TO" => $dataDoc["DATE_END_OF_RENT"],
					"PRODUCTS" => "" // доделать
				),
				"NAME" => "Счет №".$dataDoc["ACCOUNT_NUMBER"],
				"PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/images/invoice_pic.png"),
				"ACTIVE" => "Y"
			);	

			//ищем, есть ли в БД счет с таким GUID_INVOICE
			$invoice_id = $this->getInvoiceByGuid($dataDoc["GUID_INVOICE"]);	
			if($invoice_id) {
				//обновляем счет
				$el = new CIBlockElement;
				if($res = $el->Update($invoice_id, $arSaveData)) {
					//Обновляем цену
					if(CCatalogProduct::Add(Array("ID"=>$invoice_id))) {
						$dbPrice = \Bitrix\Catalog\Model\Price::getList([
							"filter" => array(
								"PRODUCT_ID" => $invoice_id,
								"CATALOG_GROUP_ID" => "1"
						)]);
						if ($arPrice = $dbPrice->fetch()) {
							$res = \Bitrix\Catalog\Model\Price::update($arPrice["ID"], Array("PRODUCT_ID"=>$invoice_id, "CATALOG_GROUP_ID"=> "1", "PRICE"=>$dataDoc["PRICE"], "CURRENCY"=>"RUB"));
							if($res->isSuccess()) {
								Helper::log($this->file_recive_invoice, "Обновлен счет с ID=".$invoice_id);
							}						
						} else {
							$res = \Bitrix\Catalog\Model\Price::add(Array("PRODUCT_ID"=>$invoice_id, "CATALOG_GROUP_ID"=> "1", "PRICE"=>$dataDoc["PRICE"], "CURRENCY"=>"RUB"));
							if($res->isSuccess()) {
								Helper::log($this->file_recive_invoice, "Обновлен счет с ID=".$invoice_id);
							}						
						}
					}				
				} else {
					Helper::log($this->file_recive_invoice, "Не удалось обновить счет! ".$el->LAST_ERROR);
				}					
			} else {
				//добавляем счет
				$el = new CIBlockElement;
				if($invoice_id = $el->Add($arSaveData)) {
					//Обновляем цену
					if(CCatalogProduct::Add(Array("ID"=>$invoice_id))) {
						$dbPrice = \Bitrix\Catalog\Model\Price::getList([
							"filter" => array(
								"PRODUCT_ID" => $invoice_id,
								"CATALOG_GROUP_ID" => "1"
						)]);
						if ($arPrice = $dbPrice->fetch()) {
							$res = \Bitrix\Catalog\Model\Price::update($arPrice["ID"], Array("PRODUCT_ID"=>$invoice_id, "CATALOG_GROUP_ID"=> "1", "PRICE"=>$dataDoc["PRICE"], "CURRENCY"=>"RUB"));
							if($res->isSuccess()) {
								Helper::log($this->file_recive_invoice, "Добавлен новый счет с ID=".$invoice_id);
							}						
						} else {
							$res = \Bitrix\Catalog\Model\Price::add(Array("PRODUCT_ID"=>$invoice_id, "CATALOG_GROUP_ID"=> "1", "PRICE"=>$dataDoc["PRICE"], "CURRENCY"=>"RUB"));
							if($res->isSuccess()) {
								Helper::log($this->file_recive_invoice, "Добавлен новый счет с ID=".$invoice_id);
							}						
						}					
					}
				} else {
					Helper::log($this->file_recive_invoice, "Не удалось добавить счет! ".$el->LAST_ERROR);
				}
			}			
		}
		
		Helper::log($this->file_recive_invoice, "######################################");
	}

	//получить ID счета по его GUID
	public function getInvoiceByGuid($guid) 
	{
		$return = false;
		if (!$guid)
			return false;

		$arFilter = [
			'IBLOCK_ID' => 53,
			'PROPERTY_INVOICE_GUID' => $guid,
		];

		$arSelect = [
			'ID',
			'IBLOCK_ID'
		];

		$res = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
		if ($item = $res->Fetch()) {
			$return = $item['ID'];
		}

		return $return;
	}	
	
}


$Exchange = new Exchange();
$Exchange->start();