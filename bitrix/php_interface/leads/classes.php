<?
\Bitrix\Main\Loader::IncludeModule("iblock");

class LeadSender {

    function Send(&$arFields)
    {
        if($arFields['IBLOCK_ID']==CALL_IBLOCK){
            self::Brain($arFields['PROPERTY_VALUES'],'Обратный звонок',2);
        }
        if($arFields['IBLOCK_ID']==QUESTION_IBLOCK){
            self::Brain($arFields['PROPERTY_VALUES'],'Сообщение',5);
        }
        if($arFields['IBLOCK_ID']==SERVICES_IBLOCK){
            self::Brain($arFields['PROPERTY_VALUES'],'Заказ через менеджера',4);
        }
        if($arFields['IBLOCK_ID']==FEEDBACK_IBLOCK){
            self::Brain($arFields['PROPERTY_VALUES'],'Сообщение',6);
        }
	}

    function SendFromWebForm($WEB_FORM_ID, &$arFields, &$arrVALUES){
        if($arrVALUES['WEB_FORM_ID']==WEBFORM_1_ID){
            $arData['FIO'] = $arrVALUES['form_text_54'];
            $arData['PHONE'] = $arrVALUES['form_text_55'];
            $arData['EMAIL'] = $arrVALUES['form_email_57'];
            $arData['MESSAGE']['VALUE']['TEXT'] = $arrVALUES['form_text_58'];
            self::Brain($arData,'Заказ через менеджера',3);
        }
        if($arrVALUES['WEB_FORM_ID']==WEBFORM_2_ID){
            $arData['FIO'] = $arrVALUES['form_text_52'];
            $arData['PHONE'] = $arrVALUES['form_text_53'];
            self::Brain($arData,'Заказ через менеджера',3);
        }
        if($arrVALUES['WEB_FORM_ID']==WEBFORM_3_ID){
            $arData['FIO'] = $arrVALUES['form_text_60'];
            $arData['PHONE'] = $arrVALUES['form_text_61'];
            $arData['EMAIL'] = $arrVALUES['form_email_63'];
            $arData['MESSAGE']['VALUE']['TEXT'] = $arrVALUES['form_textarea_65'];
            self::Brain($arData,'Сообщение',3);
        }
        if($arrVALUES['WEB_FORM_ID']==WEBFORM_4_ID){
            $arData['FIO'] = $arrVALUES['form_text_66'];
            $arData['PHONE'] = $arrVALUES['form_text_67'];
            $arData['EMAIL'] = $arrVALUES['form_email_68'];
            self::Brain($arData,'Сообщение',3);
        }
    }

    function SendOrderUserData(&$arFields){
        if($arFields['ID']){
            self::Brain($arFields,'Новая регистрация на сайте', 8);
        }
    }

    private static function Brain($arEventFields, $titleContent, $sourceId){
        $arPostFields = [];

        if($titleContent=='Новая регистрация на сайте'){
            $arPostFields['TITLE'] = 'Новая регистрация на сайте';
        }else{
            $arPostFields['TITLE'] = 'Сообщение с формы: '.$titleContent;
        }

        if($arEventFields['FIO']){
            $arPostFields['NAME'] = $arEventFields['FIO'];
        }elseif($arEventFields['NAME']){
            $arPostFields['NAME'] = $arEventFields['NAME'];
        }

        if($arEventFields['PHONE']){
            $arPostFields["PHONE"] = array(array("VALUE" => $arEventFields['PHONE'], "VALUE_TYPE" => "WORK" ));
        }

        if($arEventFields['EMAIL']){
            $arPostFields["EMAIL"] = array(array("VALUE" => $arEventFields['EMAIL'], "VALUE_TYPE" => "WORK" ));
        }

        $arPostFields["COMMENTS"] = '';
        if($arEventFields['SITE_NAME']){
            $arPostFields["COMMENTS"].= '<br/> SITE_NAME: '.$arEventFields['SITE_NAME'];
        }
        if($arEventFields['FORM_NAME']){
            $arPostFields["COMMENTS"].= '<br/> FORM_NAME: '.$titleContent;
        }
        if($arEventFields['PAGE_LINK']){
            $arPostFields["COMMENTS"].= '<br/> PAGE_LINK: '.$arEventFields['PAGE_LINK'];
        }
        if($arEventFields['NEED_PRODUCT']){
            $arPostFields["COMMENTS"].= '<br/> NEED_PRODUCT: '.$arEventFields['NEED_PRODUCT'];
        }

        if($arEventFields['SERVICE']){
            $arPostFields["COMMENTS"].= '<br/> NEED_SERVICE: '.$arEventFields['SERVICE'];
        }

        if($arEventFields['POST']){
            $arPostFields["COMMENTS"].= '<br/> POST: '.$arEventFields['POST'];
        }

        if($arEventFields['MESSAGE']){
            $arPostFields["COMMENTS"] .= '<br/> MESSAGE: '.$arEventFields['MESSAGE']['VALUE']['TEXT'];
        }


        $arPostFields['STATUS_ID'] = "NEW";
        $arPostFields['OPENED'] = "Y";
        $arPostFields['ASSIGNED_BY_ID'] = '7';
        $arPostFields['SOURCE_ID'] = $sourceId;

        $queryUrl = 'https://crm.alfasklad.ru/rest/1/stgbrb8fqelepzse/crm.lead.add.json';
        $queryData = http_build_query(array(
         'fields' => $arPostFields,
         'params' => array("REGISTER_SONET_EVENT" => "Y")
         ));

        $curl = curl_init();
         curl_setopt_array($curl, array(
         CURLOPT_SSL_VERIFYPEER => 0,
         CURLOPT_POST => 1,
         CURLOPT_HEADER => 0,
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_URL => $queryUrl,
         CURLOPT_POSTFIELDS => $queryData,
         ));

         $result = curl_exec($curl);  curl_close($curl);
    }
}
?>
