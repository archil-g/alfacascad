<?php
//при создании нового заказа отправим его в Б24
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderSaved',
    array('\Enum\Rest', 'NewOrderSendToB24')
);

//создание нового пользователя
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'main',
    'OnAfterUserAdd',
    array('\Enum\Rest', 'OnAfterUserAddHandler')
);
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'main',
    'OnAfterUserAdd',
    array('\Enum\Rest', 'OnBeforeUserAddHandler')
);

//изменение пользователя
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'main',
    'OnAfterUserUpdate',
    array('\Enum\Rest', 'OnAfterUserUpdateHandler')
);

//после оплаты заказа
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderPaid',
    array('\Enum\Rest', 'OnSaleOrderPaidHandler')
);