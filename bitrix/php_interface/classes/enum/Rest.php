<?

namespace Enum;


class Rest
{
    public static $url = 'https://crm.alfasklad.ru/rest/101/78wukog3j7cb26wn';

    function NewOrderSendToB24(\Bitrix\Main\Event $event)
    {

        $order = $event->getParameter("ENTITY");
        $isNew = $event->getParameter("IS_NEW");

        $ID_SITE = $order->getSiteId();
        $ORDER_ID = $order->getId();
        $DATE_CREATE = $order->getDateInsert()->format('d.m.Y H:i:s');
        $PERSON_TYPE = $order->getPersonTypeId();
        $USER_ID = $order->getUserId();
        $basket = $order->getBasket();
        $comment = $order->getField("USER_DESCRIPTION");
        $ACCOUNT_NUMBER = $order->getField("ACCOUNT_NUMBER");

        $priceOrder = $basket->getPrice(); // Цена с учетом скидок
        $fullPriceOrder = $basket->getBasePrice(); // Цена без учета скидок
        $discount = $fullPriceOrder - $priceOrder; //скидка

        $paymentIds = $order->getPaymentSystemId(); // массив id способов оплат
        $deliveryIds = $order->getDeliverySystemId(); // массив id способов доставки
        $discountData = $order->getDiscount()->getApplyResult();//скидки

        /* deb($paymentIds);
         deb($deliveryIds);
         deb($discountData);
         die();
 */
        //достанем внешний код
        $rsUser = \CUser::GetByID($USER_ID);
        $arUser = $rsUser->Fetch();
        $XML_ID = '';//
        $ID_CRM = '';//todo по условию ТЗ пока пустой

        /*$order = \Bitrix\Sale\Order::load($ORDER_ID);


        deb($order);
        die();*/

        //получим поля профиля покупателя
        /* $db_sales = \CSaleOrderUserProps::GetList(
             array("DATE_UPDATE" => "DESC"),
             array("USER_ID" => $USER_ID)
         );

         //берем первый профиль, так как по умолчанию у пользователя может быть только один профиль
         if ($ar_sales = $db_sales->Fetch()) {
             $db_propVals = \CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $ar_sales['ID']));
             while ($arPropVals = $db_propVals->Fetch()) {
                 echo $arPropVals["USER_VALUE_NAME"] . "=" . $arPropVals["VALUE"] . "<br>";
             }
             die();
         }*/

        //$order = Sale\Order::loadByAccountNumber($orderNumber);


        if ($isNew) {

            Helper::log('/logs/rest.txt', '############################новый заказ с сайта################, №' . $ORDER_ID);

            $propertyCollection = $order->getPropertyCollection();    // Получаем свойства заказа

            $PRODUCTS = [];
            $EXTERNAL_ID_SECTION = '';
            foreach ($basket as $basketItem) {
                $price = $basketItem->getPrice();//за еденицу
                $price_total = $basketItem->getFinalPrice();//Сумма
                $price_base = $basketItem->getBasePrice();//Сумма
                $count = $basketItem->getQuantity();
                $name = $basketItem->getField('NAME');
                $xml_id_item = $basketItem->getField('PRODUCT_XML_ID');
                $ID_PRODUCT = $basketItem->getField('PRODUCT_ID');
				
				if(strlen($xml_id_item) == 0) {
					\Bitrix\Main\Loader::includeModule("iblock");
					$res = \CIBlockElement::GetList([], Array('IBLOCK_ID'=>Array(40, 48), 'ID'=>$ID_PRODUCT), false, false, Array('ID', 'IBLOCK_ID', 'XML_ID'));
					if($item = $res->Fetch()) {
						$xml_id_item = $item['XML_ID'];
					}				
				}

                // Helper::getValueUserField('iblock',$ID_PRODUCT,'');

                $basketPropertyCollection = $basketItem->getPropertyCollection();

                //свойства товара в корзине (размер бокса, планируемы срок и тд)
                $propertyCollectionProduct = $basketPropertyCollection->getPropertyValues();

                $product['IS_ARENDA_BOX'] = 'N';

                //если это аренда бокса
                $discount = 0;
                $elem = \CIBlockElement::GetByID($ID_PRODUCT)->Fetch();
                if ($elem['IBLOCK_ID'] == 40) {
                    $section = \CIBlockSection::GetByID($elem['IBLOCK_SECTION_ID'])->Fetch();
                    $EXTERNAL_ID_SECTION = $section['XML_ID'];
                    $product['IS_ARENDA_BOX'] = 'Y';

                    $discount = ($price_base * $propertyCollectionProduct['ORDER_MONTH_COUNT']['VALUE']) - $price_total;
                }

                $product['PROP'] = $propertyCollectionProduct;
                $product['PRICE_TOTAL'] = $price_total;
                $product['PRICE_BASE'] = $price_base;
                $product['DISCOUNT'] = $discount;
                $product['COUNT'] = $count;
                $product['NAME'] = $name;
                $product['EXTERNAL_CODE'] = $xml_id_item;

                $product['ID_PRODUCT'] = $ID_PRODUCT;

                $PRODUCTS[] = $product;
            }

            //$emailPropValue = $propertyCollection->getUserEmail();

            $userArr = [
                'PERSON_TYPE' => $PERSON_TYPE,
                'ID_SITE' => $USER_ID,
                'ID_CRM' => $ID_CRM,
                'ID' => $USER_ID,
            ];
			
			//флаг о том, что данный заказ создан на основе сделки из Б24
			$isOrderFromCrm = false;
			
			
            // Перебираем свойства заказа (ИНН почта фио и тд)
            foreach ($propertyCollection as $propertyItem) {
				if(($propertyItem->getField("CODE") == "IS_ORDER_FROM_CRM") and ($propertyItem->getValue() == "Y")) {
					$isOrderFromCrm = true;
				}
			
                $userArr[$propertyItem->getField("CODE")] = $propertyItem->getValue();
            }

            //если у пользователя нет xml_id то сгенерим его и сохраним
            if ($XML_ID == '') {
                //$CUser = new \CUser();

                //серия и номер паспорта - физ лицо
                if (isset($userArr['PASSPORT_SERIES']) && isset($userArr['PASSPORT_NUMBER'])) {
                    $arFieldsNew['XML_ID'] = md5($userArr['PASSPORT_SERIES'] . $userArr['PASSPORT_NUMBER']);
                    // $CUser->Update($USER_ID, $arFieldsNew);
                    $userArr['EXTERNAL_CODE'] = $arFieldsNew['XML_ID'];
                }

                //ИНН и КПП - юр лицо
                if (isset($userArr['INN']) && isset($userArr['KPP'])) {
                    $arFieldsNew['XML_ID'] = md5($userArr['INN'] . $userArr['KPP']);
                    //$CUser->Update($USER_ID, $arFieldsNew);
                    $userArr['EXTERNAL_CODE'] = $arFieldsNew['XML_ID'];
                }
            }
            /////////

            $orderArr = [
                'PAYMENT' => array_shift($paymentIds),
                'DELIVERY' => array_shift($deliveryIds),
                'ID_SITE' => $USER_ID,
                'ID_CRM' => $ID_CRM,
                'ORDER_ID' => $ORDER_ID,
                'DATE_CREATE' => $DATE_CREATE,
                'DISCOUNT' => $discount,
                'FULL_PRICE' => $fullPriceOrder,
                'PRICE' => $priceOrder,
                'COMMENT' => $comment,
                'ACCOUNT_NUMBER' => $ACCOUNT_NUMBER,
                'EXTERNAL_ID_SECTION' => $EXTERNAL_ID_SECTION,
                'VIEW_RENT' => '',//TODO вид аренды
                'PRODUCTS' => $PRODUCTS,
            ];

            $data['USER'] = $userArr;
            $data['ORDER'] = $orderArr;

			if(!$isOrderFromCrm) {
				//отправляем в Б24 только те заказы, которые были созданы на стороне сайта
				$answer = self::SendB24($data, '/enum.addDealFromSite/');
			}  else {
				Helper::log('/logs/rest.txt', "Заказ не отправлен в Б24, т.к. он был получен оттуда (был создан на основе сделки Б24)");
			}
			
            //deb($answer);die();
            Helper::log('/logs/rest.txt', $data);
            Helper::log('/logs/rest.txt', $answer);
        }
    }

    function OnAfterUserAddHandler(&$arFields)
    {
        $log = [];
        $data = [];

        $CUser = new \CUser();

        if ($arFields['XML_ID'] == '') {
            //серия и номер паспорта - физ лицо
            if (isset($_REQUEST['ORDER_PROP_9']) && isset($_REQUEST['ORDER_PROP_10'])) {
                $arFieldsNew['XML_ID'] = md5($_REQUEST['ORDER_PROP_9'] . $_REQUEST['ORDER_PROP_10']);
                $CUser->Update($arFields['ID'], $arFieldsNew);
            }

            //ИНН и КПП - юр лицо
            if (isset($_REQUEST['ORDER_PROP_11']) && isset($_REQUEST['ORDER_PROP_12'])) {
                $arFieldsNew['XML_ID'] = md5($_REQUEST['ORDER_PROP_9'] . $_REQUEST['ORDER_PROP_10']);
                $CUser->Update($arFields['ID'], $arFieldsNew);
            }
        }

        Helper::log('/logs/newUser.txt', $arFields);

        self::SendB24($data, '/enum.addCompanyFromSite/');

        return $arFields;
    }

    function OnBeforeUserAddHandler(&$arFields)
    {

    }
	
	
    function OnAfterUserUpdateHandler(&$arFields)
    {
		if(strlen($arFields["EMAIL"]) == 0) {
			$rsUser = \CUser::GetByID($arFields["ID"]);
			$arUser = $rsUser->Fetch();
			$arFields["EMAIL"] = $arUser["EMAIL"];
		}
	
        Helper::log('/logs/updateUser.txt', $arFields);

        self::SendB24($arFields, '/enum.updateCompanyFromSite/');

        return $arFields;
    }	
	

    function OnSaleOrderPaidHandler(\Bitrix\Main\Event $event)
    {
        $order = $event->getParameter("ENTITY");
        $isPaid = $event->getParameter("IS_PAID");
        $ORDER_ID = $order->getId();

        $data = [
            'ORDER_ID' => $ORDER_ID,
        ];

        if ($order->isPaid()) {
            $answer = self::SendB24($data, '/enum.payedDeal/');
        }

        $log = [
            'ORDER_ID' => $ORDER_ID,
            'isPaid' => $order->isPaid() ? 'Y' : 'N',
            'answer' => $answer,
        ];
        Helper::log('/logs/payedOrder.txt', $log);
    }

    //отправка в вебхук
    public function SendB24($data, $method)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0, //получать заголовки
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => self::$url . $method,
            CURLOPT_POSTFIELDS => http_build_query($data),
            /*CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_json)
            ]*/
        ));

        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }
}
