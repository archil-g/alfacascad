<?

namespace Enum;

use CCrmOwnerType;

class Helper
{

    public static $list_property_values = [];

    // запись в лог
    public static function log($file, $text, $append = true)
    {
        if (is_array($text)) {
            $text = print_r($text, true);
        }
        $text = date('d.m.Y H:i:s') . ' ' . $text . PHP_EOL;

        if ($append) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . $file, $text, FILE_APPEND);
        } else {
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . $file, $text);
        }
    }

    //достать все реквизиты
    private static function getRequisitesFromCompany($id_company)
    {
        $reqData = [];
        $requisite = new \Bitrix\Crm\EntityRequisite();
        $rs = $requisite->getList([
            "filter" => ["ENTITY_ID" => $id_company, "ENTITY_TYPE_ID" => CCrmOwnerType::Company,
            ]
        ]);
        $reqData = $rs->fetchAll();

        return $reqData;
    }

    //создать реквизит у компании ID - компания, $delete_old - удаляет существующие реквизиты
    public static function AddRequisitesCompany($ID, $arFields, $delete_old = false, $update_old = false)
    {
        global $DB;

        $arResult = [];

        if ($delete_old) {
            $req_old = self::getRequisitesFromCompany($ID);
            foreach ($req_old as $old) {
                $old['DELETED'] = 'Y';
                $arFields['REQUISITE'][$old['ID']] = $old;
            }
        }

        if ($update_old) {
            $UpdateFields = $arFields['REQUISITE']['n0'];
            $arFields['REQUISITE'] = [];
            $req_old = self::getRequisitesFromCompany($ID);
            $k = 0;
            foreach ($req_old as $old) {
                if ($old['PRESET_ID'] == 1) {
                    if ($k > 0) {
                        $UpdateFields['DELETED'] = 'Y';
                    }
                    $arFields['REQUISITE'][$old['ID']] = $UpdateFields;
                    $k++;
                }
            }
        }

        if (isset($arFields['REQUISITE']) && is_array($arFields['REQUISITE'])) {
            foreach ($arFields['REQUISITE'] as $requisiteID => $requisiteForm) {
                $requisiteFields = \Bitrix\Crm\EntityRequisite::parseFormData($requisiteForm);

                if (is_array($requisiteFields)) {
                    $requisiteFields['ENTITY_TYPE_ID'] = CCrmOwnerType::Company;
                    $requisiteFields['ENTITY_ID'] = $ID;

                    $requisiteFormData[$requisiteID] = $requisiteFields;

                    if (isset($requisiteForm['DELETED']) && $requisiteForm['DELETED'] === 'Y') {
                        $deletedRequisiteIDs[$requisiteID] = true;
                    }

                    // bank details
                    if (is_array($requisiteForm['BANK_DETAILS']) && !empty($requisiteForm['BANK_DETAILS'])) {
                        $bankDetailFormData[$requisiteID] = array();
                        foreach ($requisiteForm['BANK_DETAILS'] as $pseudoId => $bankDetailForm) {
                            $bankDetailFields = \Bitrix\Crm\EntityBankDetail::parseFormData($bankDetailForm);
                            if (is_array($bankDetailFields) && !empty($bankDetailFields))
                                $bankDetailFormData[$requisiteID][$pseudoId] = $bankDetailFields;

                            if (isset($bankDetailForm['DELETED']) && $bankDetailForm['DELETED'] === 'Y') {
                                $deletedBankDetailIDs[$requisiteID][$pseudoId] = true;
                            }
                        }
                    }
                }
            }
        }

        if (!empty($requisiteFormData)) {
            $requisite = new \Bitrix\Crm\EntityRequisite();
            $bankDetail = null;
            foreach ($requisiteFormData as $requisiteID => $requisiteFields) {
                if (isset($deletedRequisiteIDs[$requisiteID])) {
                    continue;
                }

                $result = $requisiteID > 0
                    ? $requisite->checkBeforeUpdate($requisiteID, $requisiteFields)
                    : $requisite->checkBeforeAdd($requisiteFields);

                if ($result !== null && !$result->isSuccess()) {
                    foreach ($result->getErrorMessages() as $errMsg) {
                        $arResult['ERROR_MESSAGE'] .= $errMsg;
                    }
                }

                if (is_array($bankDetailFormData[$requisiteID]) && !empty($bankDetailFormData[$requisiteID])) {
                    if ($bankDetail === null)
                        $bankDetail = new \Bitrix\Crm\EntityBankDetail();

                    foreach ($bankDetailFormData[$requisiteID] as $bankDetailID => $bankDetailFields) {
                        if (isset($deletedBankDetailIDs[$requisiteID][$bankDetailID])) {
                            continue;
                        }

                        $result = $bankDetailID > 0
                            ? $bankDetail->checkBeforeUpdate($bankDetailID, $bankDetailFields)
                            : $bankDetail->checkBeforeAdd($bankDetailFields);

                        if ($result !== null && !$result->isSuccess()) {
                            foreach ($result->getErrorMessages() as $errMsg) {
                                $arResult['ERROR_MESSAGE'] .= $errMsg;
                            }
                        }
                    }
                }
            }
            unset($bankDetail);
        }

        $DB->Commit();

        if (!empty($requisiteFormData)) {
            $requisite = new \Bitrix\Crm\EntityRequisite();
            foreach ($requisiteFormData as $requisiteID => &$requisiteFields) {
                //Refresh ID if entity has been created.
                if (!$isEditMode) {
                    $requisiteFields['ENTITY_ID'] = $ID;
                }

                $result = null;
                $operation = '';
                if (isset($deletedRequisiteIDs[$requisiteID])) {
                    if ($requisiteID > 0) {
                        $result = $requisite->delete($requisiteID);
                    }
                    $operation = 'delete';
                } elseif ($requisiteID > 0) {
                    $result = $requisite->update($requisiteID, $requisiteFields);
                    $operation = 'update';
                } else {
                    $result = $requisite->add($requisiteFields);
                    $operation = 'add';
                }

                // bank details
                if ($result !== null && $result->isSuccess()
                    && is_array($bankDetailFormData[$requisiteID])
                    && ($operation === 'add' || $operation === 'update' || $operation === 'delete')
                ) {
                    if ($operation === 'delete') {
                        unset($bankDetailFormData[$requisiteID]);
                    } else {
                        $bankDetail = new \Bitrix\Crm\EntityBankDetail();
                        foreach ($bankDetailFormData[$requisiteID] as $pseudoId => &$bankDetailFields) {
                            $bankDetailResult = null;
                            if (isset($deletedBankDetailIDs[$requisiteID][$pseudoId])) {
                                if ($pseudoId > 0) {
                                    $bankDetailResult = $bankDetail->delete($pseudoId);
                                }
                            } elseif ($pseudoId > 0) {
                                $bankDetailResult = $bankDetail->update($pseudoId, $bankDetailFields);
                            } else {
                                $bankDetailFields['ENTITY_TYPE_ID'] = \CCrmOwnerType::Requisite;
                                $bankDetailFields['ENTITY_ID'] = ($operation === 'add') ?
                                    $result->getId() : $requisiteID;
                                $bankDetailResult = $bankDetail->add($bankDetailFields);
                                if ($bankDetailResult && $bankDetailResult->isSuccess())
                                    $bankDetailFields['ID'] = $bankDetailResult->getId();
                            }

                            if ($bankDetailResult !== null && !$bankDetailResult->isSuccess()) {
                                $result->addErrors($bankDetailResult->getErrors());
                            }
                        }
                        unset($bankDetailFields);
                    }
                }

                if ($result !== null && !$result->isSuccess()) {
                    foreach ($result->getErrorMessages() as $errMsg) {
                        $arResult['ERROR_MESSAGE'] .= $errMsg;
                    }
                }
            }
            unset($requisiteFields);
        }

        /*[n0] => Array
            (
                [NAME] => Организация
                [ID] => 0
                [PSEUDO_ID] => n0
                [PRESET_ID] => 1
                [SORT] => 499
                [RQ_INN] => 1433000147
                [RQ_COMPANY_NAME] => АК "АЛРОСА" (ПАО)
                [RQ_COMPANY_FULL_NAME] => АКЦИОНЕРНАЯ КОМПАНИЯ "АЛРОСА" (ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО)
                [RQ_OGRN] => 1021400967092
                [RQ_KPP] => 143301001
                [RQ_COMPANY_REG_DATE] => 13.08.1992
                [RQ_OKPO] =>
                [RQ_OKTMO] =>
                [RQ_DIRECTOR] => ЖАРКОВ АНДРЕЙ ВЯЧЕСЛАВОВИЧ
                [RQ_ACCOUNTANT] =>
                [RQ_ADDR] => Array
                    (
                        [6] => Array
                            (
                                [ADDRESS_1] => УЛ. ЛЕНИНА, Д. 6
                                [ADDRESS_2] =>
                                [CITY] => МИРНЫЙ
                                [REGION] => МИРНИНСКИЙ
                                [PROVINCE] => САХА /ЯКУТИЯ/ РЕСПУБЛИКА
                                [POSTAL_CODE] => 678175
                                [COUNTRY] => Россия
                                [COUNTRY_CODE] =>
                            )

                    )

            )
    */
        return $arResult;
    }

    //достать реквииты у компании $id_company
    public function getAllRequisite($id_company, $select = [])
    {
        $sel = ['*'];
        if (!empty($select)) {
            $sel = array_merge($select, $sel);
        }
        $requisite = new \Bitrix\Crm\EntityRequisite();
        $rs = $requisite->getList(
            [
                "filter" => ["ENTITY_ID" => $id_company, "ENTITY_TYPE_ID" => \CCrmOwnerType::Company],
                "select" => $sel
            ]
        );
        $reqData = $rs->fetchAll();

        foreach ($reqData as &$data) {
            if ($data['ID'] > 0) {
                $addr = $requisite->getAddresses($data['ID']);
                $data['ADDR'] = $addr;
            }
        }
        return $reqData;
    }

    public static function getValueUserField($entity, $id, $code)
    {
        $row = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields($entity, $id);
        return $row[$code]['VALUE'];
    }
    public static function setValueUserField($entity, $id, $code, $value)
    {
        return $GLOBALS['USER_FIELD_MANAGER']->Update($entity, $id, [$code => $value]);
    }

    public static function getUfIdByCode($entity, $code)
    {
        if (empty($entity) || empty($code)) {
            return;
        }

        $prop = \CUserTypeEntity::GetList([], [
            'ENTITY_ID' => $entity,
            'FIELD_NAME' => $code
        ]);

        if ($prop = $prop->Fetch()) {
            return $prop['ID'];
        }
    }

    public static function getUfEnumValues($entity, $code)
    {
        if (empty($entity) || empty($code)) {
            return;
        }

        $id = self::getUfIdByCode($entity, $code);

        if ($id <= 0) {
            return;
        }

        $db = \CUserFieldEnum::GetList([], [
            'USER_FIELD_ID' => $id
        ]);

        $props = [];

        while ($prop = $db->GetNext()) {
            $props[] = $prop;
        }

        return $props;
    }

    public static function getPhones($ELEMENT_ID, $ENTITY_ID, $value = true)
    {
        if ($ELEMENT_ID <= 0) {
            return;
        }

        \Bitrix\Main\Loader::IncludeModule('crm');

        $phones = [];

        $filter = [
            'ELEMENT_ID' => $ELEMENT_ID,
            'ENTITY_ID' => $ENTITY_ID,
            'TYPE_ID' => \CCrmFieldMulti::PHONE
        ];

        $select = [
            'VALUE'
        ];

        $db = \CCrmFieldMulti::GetList([], $filter, false, false, $select);

        while ($field = $db->Fetch()) {
            $phones[] = $value ? $field['VALUE'] : $field;
        }

        return $phones;
    }

    public static function getEmails($ELEMENT_ID, $ENTITY_ID, $value = true)
    {
        if ($ELEMENT_ID <= 0) {
            return;
        }

        \Bitrix\Main\Loader::IncludeModule('crm');

        $emails = [];

        $filter = [
            'ELEMENT_ID' => $ELEMENT_ID,
            'ENTITY_ID' => $ENTITY_ID,
            'TYPE_ID' => \CCrmFieldMulti::EMAIL
        ];

        $select = [
            'VALUE'
        ];

        $db = \CCrmFieldMulti::GetList(['ID' => 'ASC'], $filter, false, false, $select);

        while ($field = $db->Fetch()) {
            $emails[] = $value ? $field['VALUE'] : $field;
        }

        return $emails;
    }

    public static function DeletePhoneEmail($ELEMENT_ID, $ENTITY_ID, $arr)
    {
        \Bitrix\Main\Loader::IncludeModule('crm');
        $object = new \CCrmFieldMulti();
        foreach ($arr as $item) {
            $object->Delete($item['ID']);
        }
    }

    private static function addEmailOrPhone($field, $ELEMENT_ID, $ENTITY_ID, $value, $type = 'WORK')
    {
        if (!in_array($field, [\CCrmFieldMulti::PHONE, \CCrmFieldMulti::EMAIL]) || $ELEMENT_ID <= 0 || empty($value)) {
            return;
        }

        \Bitrix\Main\Loader::IncludeModule('crm');

        $object = new \CCrmFieldMulti();

        $fields = [
            'ELEMENT_ID' => $ELEMENT_ID,
            'ENTITY_ID' => $ENTITY_ID,
            'TYPE_ID' => $field,
            'VALUE' => $value,
            'VALUE_TYPE' => $type
        ];

        $object->Add($fields);
    }

    public static function addPhone($ELEMENT_ID, $ENTITY_ID, $phone, $type = 'WORK', $first_insert = true)
    {
        if (self::phoneExist($phone, $ELEMENT_ID, $ENTITY_ID))
            return false;


        if ($first_insert) {
            $phones = self::getPhones($ELEMENT_ID, $ENTITY_ID, false);
            self::DeletePhoneEmail($ELEMENT_ID, $ENTITY_ID, $phones);

            self::addEmailOrPhone(\CCrmFieldMulti::PHONE, $ELEMENT_ID, $ENTITY_ID, $phone, $type);

            foreach ($phones as $item) {
                $phone = $item['VALUE'];
                self::addEmailOrPhone(\CCrmFieldMulti::PHONE, $ELEMENT_ID, $ENTITY_ID, $phone, $type);
            }

        } else {
            if (!self::phoneExist($phone, $ELEMENT_ID, $ENTITY_ID)) {
                return self::addEmailOrPhone(\CCrmFieldMulti::PHONE, $ELEMENT_ID, $ENTITY_ID, $phone, $type);
            }
        }
    }

    public static function addEmail($ELEMENT_ID, $ENTITY_ID, $email, $type = 'WORK', $first_insert = true)
    {
        if (self::emailExist($email, $ELEMENT_ID, $ENTITY_ID))
            return false;


        if ($first_insert) {
            $emails = self::getEmails($ELEMENT_ID, $ENTITY_ID, false);

            self::DeletePhoneEmail($ELEMENT_ID, $ENTITY_ID, $emails);

            self::addEmailOrPhone(\CCrmFieldMulti::EMAIL, $ELEMENT_ID, $ENTITY_ID, $email, $type);

            foreach ($emails as $item) {
                $email = $item['VALUE'];
                self::addEmailOrPhone(\CCrmFieldMulti::EMAIL, $ELEMENT_ID, $ENTITY_ID, $email, $type);
            }

        } else {
            if (!self::emailExist($email, $ELEMENT_ID, $ENTITY_ID)) {
                self::addEmailOrPhone(\CCrmFieldMulti::EMAIL, $ELEMENT_ID, $ENTITY_ID, $email, $type);
            }
        }
    }

    private static function phoneExist($phone, $ELEMENT_ID, $ENTITY_ID)
    {

        \Bitrix\Main\Loader::IncludeModule('crm');

        $filter = [
            'ELEMENT_ID' => $ELEMENT_ID,
            '%VALUE' => $phone,
            'ENTITY_ID' => $ENTITY_ID,
            'TYPE_ID' => \CCrmFieldMulti::PHONE
        ];

        $select = [
            'ELEMENT_ID'
        ];

        $db = \CCrmFieldMulti::GetList([], $filter, false, false, $select);

        if ($db->Fetch()) {
            return true;
        }

        return false;
    }

    private static function emailExist($email, $ELEMENT_ID, $ENTITY_ID)
    {

        \Bitrix\Main\Loader::IncludeModule('crm');

        $filter = [
            'ELEMENT_ID' => $ELEMENT_ID,
            'VALUE' => $email,
            'ENTITY_ID' => $ENTITY_ID,
            'TYPE_ID' => \CCrmFieldMulti::EMAIL
        ];

        $select = [
            'ELEMENT_ID'
        ];

        $db = \CCrmFieldMulti::GetList([], $filter, false, false, $select);

        if ($db->Fetch()) {
            return true;
        }

        return false;
    }

    //получим id значения из поля типа список - должность
    //$val - текстовое значение поля типа список
    //$code - символьный код свойства CRM
    //$name_field - имя переменной для временного хранения в объекте значений
    public static function get_id_value_enumeration($val, $code_property, $entity_type)
    {
        global $USER_FIELD_MANAGER;
        $CCrmFields = new \CCrmFields($USER_FIELD_MANAGER, $entity_type);//объект всех полей
        $arResult['FIELD'] = $CCrmFields->GetByName($code_property);//получаем нужное поле

        self::$list_property_values[$code_property] = self::getValuesFieldList($arResult);

        //если нет такого значения в списке то будем его добавлять
        if (!array_key_exists($val, self::$list_property_values[$code_property])) {

            if ($arResult['FIELD']['USER_TYPE']['USER_TYPE_ID'] == 'iblock_element') {
                $iblock_id = $arResult['FIELD']['SETTINGS']['IBLOCK_ID'];
                $arFields = [
                    'IBLOCK_ID' => $iblock_id,
                    'ACTIVE' => 'Y',
                    'NAME' => $val
                ];
                $obj = new \CIBlockElement();
                $res = $obj->Add($arFields);
            } else {
                $arField = $arResult['FIELD'];
                $arField['LIST']['n0']['VALUE'] = $val;
                $arField['LIST']['n0']['SORT'] = 200;
                $arField['LIST']['n0']['XML_ID'] = md5($val);
                $res = $CCrmFields->UpdateField($arResult['FIELD']['ID'], $arField);
            }

            self::$list_property_values[$code_property] = self::getValuesFieldList($arResult);
        } else {
            return self::$list_property_values[$code_property][$val];
        }

        return self::$list_property_values[$code_property][$val];
    }

    //актуальные значения поля типа спсиок
    private static function getValuesFieldList($arResult)
    {
        $array = array();
        if (is_callable(array($arResult['FIELD']['USER_TYPE']['CLASS_NAME'], 'GetList'))) {
            $rsEnum = call_user_func_array(array($arResult['FIELD']['USER_TYPE']['CLASS_NAME'], 'GetList'), array($arResult['FIELD']));
            while ($ar = $rsEnum->GetNext()) {
                $array[$ar['VALUE']] = $ar['ID'];
                $arResult['LIST'][$ar['ID']] = $ar;
                if ($ar['DEF'] == 'Y')
                    $arResult['LIST_DEF'][$ar['ID']] = true;
            }
        }

        return $array;
    }

}