<?

include_once __DIR__ . '/constants.php';
include_once __DIR__ . '/functions.php';
include_once __DIR__ . '/handlers.php';

CModule::AddAutoloadClasses(
    '', // не указываем имя модуля
    [
        'Enum\Rest' => '/bitrix/php_interface/classes/enum/Rest.php',
        'Enum\Helper' => '/bitrix/php_interface/classes/enum/Helper.php',
    ]
);


// constants leads
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/leads/constants.php"))
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/leads/constants.php");
// classes leads
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/leads/classes.php"))
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/leads/classes.php");
// handlers leads
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/leads/handlers.php"))
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/leads/handlers.php");


/*
Константы
*/
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/constants.php")) {
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/php_interface/include/constants.php");
}

/*
Функция для склонения слов
*/
function num_decline($number, $titles, $param2 = '', $param3 = '')
{
    if ($param2)
        $titles = [$titles, $param2, $param3];
    if (is_string($titles))
        $titles = preg_split('/, */', $titles);
    if (empty($titles[2]))
        $titles[2] = $titles[1]; // когда указано 2 элемента

    $cases = [2, 0, 1, 1, 1, 2];

    $intnum = abs(intval(strip_tags($number)));

    return "$number " . $titles[($intnum % 100 > 4 && $intnum % 100 < 20) ? 2 : $cases[min($intnum % 10, 5)]];
}


require_once(dirname(__FILE__) . '/classes/ComponentHelper.php');
function ShowNavChain($template = 'corp')
{
    global $APPLICATION;
    $APPLICATION->IncludeComponent(
        "bitrix:breadcrumb",
        $template,
        array(
            "COMPONENT_TEMPLATE" => $template,
            "START_FROM" => "0",
            "PATH" => "",
            "SITE_ID" => "s1"
        )
    );
}


/*
Парсинг данных о складах с Яндекс карты
*/
function Parse($p1, $p2, $p3)
{
    $num1 = strpos($p1, $p2);
    if ($num1 === false) return 0;
    $num2 = substr($p1, $num1);
    return strip_tags(substr($num2, 0, strpos($num2, $p3)));
}

function getDataAboutSklad_YMAP()
{
    if (CModule::IncludeModule("iblock")) {
        $arFilter = Array("IBLOCK_ID" => 40, "DEPTH_LEVEL" => 1, "ACTIVE" => "Y");
        $arSelect = Array("IBLOCK_ID", "ID", "UF_YMAP_URL");
        $rsSect = CIBlockSection::GetList(Array("sort" => "asc"), $arFilter, false, $arSelect);
        while ($arSklad = $rsSect->GetNext()) {
            $data = file_get_contents($arSklad["UF_YMAP_URL"]);

            $reviewsCount = 0;
            $reviewsCount = intval(preg_replace("/[^0-9]/", '', Parse($data, '<span class="business-header-rating-view__text _clickable">', '</span>')));

            $votesCount = 0;
            $votesCount = intval(preg_replace("/[^0-9]/", '', Parse($data, '<div class="orgpage-reviews-view__rating-reviews-count">', '</div>')));

            $ratingValue = 0;
            $ratingValue = floatval(preg_replace("/[^.0-9]/", '', Parse($data, '<div class="orgpage-reviews-view__rating-score">', '</div>')));

            $bs = new CIBlockSection;
            if ($reviewsCount > 0) {
                $res = $bs->Update($arSklad["ID"], Array("UF_YMAP_REVIEWS" => $reviewsCount));
            }
            if ($votesCount > 0) {
                $res = $bs->Update($arSklad["ID"], Array("UF_YMAP_VOTES" => $votesCount));
            }
            if ($ratingValue > 0) {
                $res = $bs->Update($arSklad["ID"], Array("UF_YMAP_RATING" => $ratingValue));
            }
        }
    }
    return "getDataAboutSklad_YMAP();";
}

/*
************ end ***************
*/


/*
Функция пересчета стоимости аренды бокса для корзины
*/
function calculateBoxPriceInBasket($basket, $basketItemId)
{
    Bitrix\Main\Loader::includeModule("sale");
    Bitrix\Main\Loader::includeModule("catalog");
    Bitrix\Main\Loader::includeModule("iblock");

    $newBasketItem = $basket->getItemById($basketItemId);
    $basketItemPrice = $newBasketItem->getPrice();
    $basketItemQuntity = $newBasketItem->getQuantity();

    for ($i = 0; $i <= $basketItemQuntity - 1; $i++) {
        if ($i == 0) {
            $today = time();
            $dateStartOrderPeriod = date("d.m.Y", $today);
            $dateEndOrderPeriod = date(date("t", $today) . "." . date("m", $today) . "." . date("Y", $today));

            $countDays = date("d", MakeTimeStamp($dateEndOrderPeriod, "DD.MM.YYYY")) - date("d", MakeTimeStamp($dateStartOrderPeriod, "DD.MM.YYYY"));
            $countDays = $countDays + 1;

            $daysInMonth = date("t", MakeTimeStamp($dateStartOrderPeriod, "DD.MM.YYYY"));
            $priceOrderPeriod = intval(($basketItemPrice / $daysInMonth) * $countDays);
        } else {
            if (strlen($today) == 0) {
                $today = time();
            }

            $day = "01";
            if ((date("m", $today) + 1) > 12) {
                $month = "01";
                $year = date("Y", $today) + 1;
            } else {
                $month = date("m", $today) + 1;
                $year = date("Y", $today);
            }

            $today = MakeTimeStamp($day . "." . $month . "." . $year, "DD.MM.YYYY");

            $dateStartOrderPeriod = date("d.m.Y", $today);
            $dateEndOrderPeriod = date(date("t", $today) . "." . date("m", $today) . "." . date("Y", $today));

            $priceOrderPeriod = $basketItemPrice;
        }

        $newRes["ORDER_PERIOD"][] = Array("DATE_START" => $dateStartOrderPeriod, "DATE_END" => $dateEndOrderPeriod, "PRICE" => $priceOrderPeriod);
    }

    $totalPrice = 0;
    foreach ($newRes["ORDER_PERIOD"] as $period) {
        $totalPrice = $totalPrice + $period["PRICE"];
    }
    $resProp = CIBlockElement::GetProperty(STORAGES_CATALOG_IBLOCK, $newBasketItem->getProductId(), "sort", "asc", array("CODE" => "PRICE_GUARANTEE"));
    if ($propBasketItem = $resProp->GetNext()) {
        $priceGuarantee = intval($propBasketItem["VALUE"]);
        if ($priceGuarantee > 0) {
            $totalPrice = $totalPrice + $priceGuarantee;
            $newRes["CUSTOM_PRICE_GUARANTEE"] = $priceGuarantee;
        }
    }
    $resProp = CIBlockElement::GetProperty(STORAGES_CATALOG_IBLOCK, $newBasketItem->getProductId(), "sort", "asc", array("CODE" => "PRICE_INSURANCE"));
    if ($propBasketItem = $resProp->GetNext()) {
        $priceInsurance = intval($propBasketItem["VALUE"]) * $basketItemQuntity;
        if ($priceInsurance > 0) {
            $totalPrice = $totalPrice + $priceInsurance;
            $newRes["CUSTOM_PRICE_INSURANCE"] = $priceInsurance;
        }
    }

    $basketPropertyCollection = $newBasketItem->getPropertyCollection();

    $arForPropCollection = Array(
        array(
            'NAME' => 'Сумма',
            'CODE' => 'CUSTOM_SUMMA',
            'VALUE' => $totalPrice,
            'SORT' => 100,
        ),
        array(
            'NAME' => 'Дата начала аренды',
            'CODE' => 'DATE_START',
            'VALUE' => $newRes["ORDER_PERIOD"][0]["DATE_START"],
            'SORT' => 100,
        ),
        array(
            'NAME' => 'Дата окончания аренды',
            'CODE' => 'DATE_END',
            'VALUE' => $newRes["ORDER_PERIOD"][count($newRes["ORDER_PERIOD"]) - 1]["DATE_END"],
            'SORT' => 100,
        ),
        array(
            'NAME' => 'Количество месяцев',
            'CODE' => 'ORDER_MONTH_COUNT',
            'VALUE' => count($newRes["ORDER_PERIOD"]),
            'SORT' => 100,
        ),
        array(
            'NAME' => 'Гарантийный депозит',
            'CODE' => 'PRICE_GUARANTEE',
            'VALUE' => (!empty($newRes["CUSTOM_PRICE_GUARANTEE"]) ? $newRes["CUSTOM_PRICE_GUARANTEE"] : ""),
            'SORT' => 100,
        ),
        array(
            'NAME' => 'Страховка',
            'CODE' => 'PRICE_INSURANCE',
            'VALUE' => (!empty($newRes["CUSTOM_PRICE_INSURANCE"]) ? $newRes["CUSTOM_PRICE_INSURANCE"] : ""),
            'SORT' => 100,
        ),
    );
    foreach ($newRes["ORDER_PERIOD"] as $p => $period) {
        $arForPropCollection[] = Array(
            'NAME' => 'Аренда с ' . $period["DATE_START"] . " по " . $period["DATE_END"],
            'CODE' => 'ORDER_MONTH_' . ($p + 1),
            'VALUE' => json_encode($period),
            'SORT' => 100,
        );
    }

    $basketPropertyCollection->setProperty($arForPropCollection);
    $basketPropertyCollection->save();
}

/*
************ end ***************
*/



/*
Ограничиваем список возможных способов оплаты и типов плательщика, если в корзине товар "Счет" или "Пополнение баланса"
*/
\Bitrix\Main\EventManager::getInstance()->addEventHandlerCompatible(
    'sale',
    'OnSaleComponentOrderJsData',
    'setCustomListOfPaySystems'
);
function setCustomListOfPaySystems(&$arResult, &$arParams)
{
    Bitrix\Main\Loader::includeModule("sale");
    Bitrix\Main\Loader::includeModule("catalog");
    Bitrix\Main\Loader::includeModule("iblock");
    Bitrix\Main\Loader::includeModule("currency");

    foreach ($arResult["JS_DATA"]["GRID"]["ROWS"] as $key => $arItem) {
        $itemIblock = CIBlockElement::GetIBlockByID($arItem["data"]["PRODUCT_ID"]);
        if (($itemIblock == INVOICES_IBLOCK) or
			($itemIblock == BALANCE_ITEMS_IBLOCK)) 
		{		

			//способы оплаты
			$arInvoicePaySystem = Array();
			foreach($arResult["PAY_SYSTEM"] as $paySystem) {
				if($paySystem["ID"] == "3") {
					$arInvoicePaySystem = $paySystem;
					$arInvoicePaySystem["CHECKED"] = "Y";
					break;
				}
			}
			if(!empty($arInvoicePaySystem)) {
				//устанавливаем "Оплату через сайт" как единственный способ оплаты для товара "Счет" или "Пополнение баланса"
				$arResult["PAY_SYSTEM"] = Array();
				$arResult["PAY_SYSTEM"][0] = $arInvoicePaySystem;
				
				$arResult["JS_DATA"]["PAY_SYSTEM"] = Array();
				$arResult["JS_DATA"]["PAY_SYSTEM"][0] = $arInvoicePaySystem;
			}
			
			//тип плательщика
			$arInvoicePersonType = Array();
			foreach($arResult["PERSON_TYPE"] as $personType) {
				if($personType["ID"] == "1") {
					$arInvoicePersonType = $personType;
					$arInvoicePersonType["CHECKED"] = "Y";
					break;
				}
			}
			if(!empty($arInvoicePersonType)) {
				//устанавливаем "Физ. лицо" как единственный тип плательщика для товара "Счет" или "Пополнение баланса"
				$arResult["PERSON_TYPE"] = Array();
				$arResult["PERSON_TYPE"][0] = $arInvoicePersonType;
				
				$arResult["JS_DATA"]["PERSON_TYPE"] = Array();
				$arResult["JS_DATA"]["PERSON_TYPE"][0] = $arInvoicePersonType;				
			}			
        }
    }
}

/*
************ end ***************
*/



/*
Подменяем цену бокса в компоненте оформления заказа
*/
\Bitrix\Main\EventManager::getInstance()->addEventHandlerCompatible(
    'sale',
    'OnSaleComponentOrderJsData',
    'calculateBoxPriceInOrderComponent'
);
function calculateBoxPriceInOrderComponent(&$arResult, &$arParams)
{
    Bitrix\Main\Loader::includeModule("sale");
    Bitrix\Main\Loader::includeModule("catalog");
    Bitrix\Main\Loader::includeModule("iblock");
    Bitrix\Main\Loader::includeModule("currency");

    foreach ($arResult["JS_DATA"]["GRID"]["ROWS"] as $key => $arItem) {
        $itemIblock = CIBlockElement::GetIBlockByID($arItem["data"]["PRODUCT_ID"]);
        if ($itemIblock == STORAGES_CATALOG_IBLOCK) {
            foreach ($arItem["data"]["PROPS"] as $arItemProp) {
                if ($arItemProp["CODE"] == "CUSTOM_SUMMA") {
                    if (strlen($arItemProp["VALUE"]) > 0) {
                        $boxPriceCorrection = intval($arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"]["SUM_NUM"]) - intval($arItemProp["VALUE"]);
                        $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"]["SUM_NUM"] = $arItemProp["VALUE"];
                        $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"]["SUM"] = CCurrencyLang::CurrencyFormat($arItemProp["VALUE"], $arItem["data"]["CURRENCY"], true);
                        $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"]["SUM_BASE"] = "";
                        $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"]["SUM_BASE_FORMATED"] = "";
                        $arResult["JS_DATA"]["TOTAL"]["ORDER_PRICE"] = intval($arResult["JS_DATA"]["TOTAL"]["ORDER_PRICE"]) - $boxPriceCorrection;
                        $arResult["JS_DATA"]["TOTAL"]["ORDER_PRICE_FORMATED"] = CCurrencyLang::CurrencyFormat(intval($arResult["JS_DATA"]["TOTAL"]["ORDER_PRICE"]), $arItem["data"]["CURRENCY"], true);
                        $arResult["JS_DATA"]["TOTAL"]["PRICE_WITHOUT_DISCOUNT_VALUE"] = "";
                        $arResult["JS_DATA"]["TOTAL"]["PRICE_WITHOUT_DISCOUNT"] = "";
                        $arResult["JS_DATA"]["TOTAL"]["ORDER_TOTAL_PRICE"] = intval($arResult["JS_DATA"]["TOTAL"]["ORDER_TOTAL_PRICE"]) - $boxPriceCorrection;
                        $arResult["JS_DATA"]["TOTAL"]["ORDER_TOTAL_PRICE_FORMATED"] = CCurrencyLang::CurrencyFormat(intval($arResult["JS_DATA"]["TOTAL"]["ORDER_TOTAL_PRICE"]), $arItem["data"]["CURRENCY"], true);
                        $arResult["JS_DATA"]["TOTAL"]["DISCOUNT_PRICE"] = "";
                        $arResult["JS_DATA"]["TOTAL"]["DISCOUNT_PRICE_FORMATED"] = "";
						
						$resPropProp = CIBlockElement::GetProperty(STORAGES_CATALOG_IBLOCK, $arItem["data"]["PRODUCT_ID"], array("sort"=>"asc"), Array("CODE"=>"NAME_FOR_SITE"));
						if($obPropProp = $resPropProp->GetNext()) {
							if(strlen($obPropProp["VALUE"]) > 0) {
								$arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"]["NAME"] = $obPropProp["VALUE"];
							}
						}						
                    }
                }

                //добавляем расшировку стоимости аренды бокса
                if ($arItemProp["CODE"] == "PRICE_GUARANTEE") {
                    if (strlen($arItemProp["VALUE"]) > 0) {
                        $arResult["JS_DATA"]["GRID"]["HEADERS_HIDDEN"][] = Array("id" => $arItemProp["CODE"], "name" => $arItemProp["NAME"]);
                        $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"][$arItemProp["CODE"]] = $arItemProp["VALUE"] . " руб.";
                    }
                }
                if ($arItemProp["CODE"] == "ORDER_MONTH_COUNT") {
                    $monthsCount = $arItemProp["VALUE"];
                    for ($i = 1; $i <= $monthsCount; $i++) {
                        foreach ($arItem["data"]["PROPS"] as $arItemProp2) {
                            if ($arItemProp2["CODE"] == "ORDER_MONTH_" . $i) {
                                $arOrderPeriod = json_decode($arItemProp2["VALUE"], true);
                                $arResult["JS_DATA"]["GRID"]["HEADERS_HIDDEN"][] = Array("id" => $arItemProp2["CODE"], "name" => $arItemProp2["NAME"]);
                                $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"][$arItemProp2["CODE"]] = $arOrderPeriod["PRICE"] . " руб.";
                                break;
                            }
                        }
                    }
                }
                if ($arItemProp["CODE"] == "PRICE_INSURANCE") {
                    if (strlen($arItemProp["VALUE"]) > 0) {
                        $arResult["JS_DATA"]["GRID"]["HEADERS_HIDDEN"][] = Array("id" => $arItemProp["CODE"], "name" => $arItemProp["NAME"]);
                        $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"][$arItemProp["CODE"]] = $arItemProp["VALUE"] . " руб.";
                    }
                }
            }

            //добавляем данные о складе
            $resProd = CIBlockElement::GetByID($arItem["data"]["PRODUCT_ID"]);
            if ($arProd = $resProd->GetNext()) {
                if (strlen($arProd["IBLOCK_SECTION_ID"]) > 0) {
                    $rsSect = CIBlockSection::GetList(array("sort" => "asc"), Array("IBLOCK_ID" => STORAGES_CATALOG_IBLOCK, "DEPTH_LEVEL" => 1, "ID" => $arProd["IBLOCK_SECTION_ID"]), false, Array("ID", "IBLOCK_ID", "NAME", "CODE", "UF_ADDRESS"));
                    if ($sklad = $rsSect->GetNext()) {
                        array_unshift($arResult["JS_DATA"]["GRID"]["HEADERS_HIDDEN"], Array("id" => "SKLAD_ADDRESS", "name" => "Адрес"));
                        $arResult["JS_DATA"]["GRID"]["ROWS"][$key]["data"]["SKLAD_ADDRESS"] = $sklad["NAME"] . " (" . $sklad["UF_ADDRESS"] . ")";
                    }
                }
            }
        }
    }
}

/*
************ end ***************
*/


/*
Редактируем товар "бокс" в корзине заказа
*/

\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderSaved',
    'calculateBoxPriceInOrder'
);
function calculateBoxPriceInOrder(\Bitrix\Main\Event $event)
{
    Bitrix\Main\Loader::includeModule("sale");
    Bitrix\Main\Loader::includeModule("catalog");
    Bitrix\Main\Loader::includeModule("iblock");
    Bitrix\Main\Loader::includeModule("currency");

    $order = $event->getParameter("ENTITY");
    $isNew = $event->getParameter("IS_NEW");

    if ($isNew) {
	
		$isPublicOrder = false; //оформлен ли заказ из публичной части сайта

		$propertyCollection = $order->getPropertyCollection();
		$arPropCollection = $propertyCollection->getArray();
		foreach($arPropCollection["properties"] as $propCol) {
			if($propCol["CODE"] == "IS_PUBLIC_ORDER") {
				if($propCol["VALUE"][0] == "Y") {
					$isPublicOrder = true;
				}
			}
		}		
	
		if($isPublicOrder) {	
	
			$basket = $order->getBasket();

			foreach ($basket as $basketItem) {
				$itemIblock = CIBlockElement::GetIBlockByID($basketItem->getProductId());
				if ($itemIblock == STORAGES_CATALOG_IBLOCK) {
					$boxId = $basketItem->getProductId();
					$basketItemID = $basketItem->getId();
					$quantity = $basketItem->getQuantity();
					
					//ищем примененные скидки на бокс
					$arResDiscounts = Array();
					$discounts = $order->getDiscount()->getApplyResult(true);
					foreach($discounts["RESULT"]["BASKET"] as $key_arDiscItem=>$arDiscItem) {
						if($key_arDiscItem == $basketItemID) {
							foreach($arDiscItem as $discItem) {
								if($discItem["APPLY"] == "Y") {
									$arResDiscounts[] = $discounts["DISCOUNT_LIST"][$discItem["DISCOUNT_ID"]];
								}
							}
						}
					}
					
					
					
					$basketPropertyCollection = $basketItem->getPropertyCollection();
					$basketItemProps = $basketPropertyCollection->getPropertyValues();
					$monthsCount = $basketItemProps["ORDER_MONTH_COUNT"]["VALUE"];
					for ($i = 1; $i <= $monthsCount; $i++) {
						$arOrderPeriod[] = json_decode($basketItemProps["ORDER_MONTH_" . $i]["VALUE"], true);
					}
					
					//Добавляем отдельными товарами цену страховки и гарантийного депозита
					if (strlen($basketItemProps["PRICE_GUARANTEE"]["VALUE"]) > 0) {
						$item = $basket->createItem("catalog", ID_ITEM_GUARANTEE);
						$item->setFields(array(
							"NAME" => "Гарантийный депозит",
							"QUANTITY" => 1,
							"CURRENCY" => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
							"LID" => Bitrix\Main\Context::getCurrent()->getSite(),
							"PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider",
							"CUSTOM_PRICE" => "Y",
							"PRICE" => $basketItemProps["PRICE_GUARANTEE"]["VALUE"],
						));					
						//корректируем цену на бокс
						$basketItemProps["CUSTOM_SUMMA"]["VALUE"] = floatval($basketItemProps["CUSTOM_SUMMA"]["VALUE"]) - floatval($basketItemProps["PRICE_GUARANTEE"]["VALUE"]);
					}
					if (strlen($basketItemProps["PRICE_INSURANCE"]["VALUE"]) > 0) {	
						$item = $basket->createItem("catalog", ID_ITEM_INSURANCE);
						$item->setFields(array(
							"NAME" => "Страховка",
							"QUANTITY" => 1,
							"CURRENCY" => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
							"LID" => Bitrix\Main\Context::getCurrent()->getSite(),
							"PRODUCT_PROVIDER_CLASS" => "CCatalogProductProvider",
							"CUSTOM_PRICE" => "Y",
							"PRICE" => $basketItemProps["PRICE_INSURANCE"]["VALUE"],
						));	
						//корректируем цену на бокс
						$basketItemProps["CUSTOM_SUMMA"]["VALUE"] = floatval($basketItemProps["CUSTOM_SUMMA"]["VALUE"]) - floatval($basketItemProps["PRICE_INSURANCE"]["VALUE"]);					
					}				

					//Редактируем бокс в корзине
					$basketItem->setFields(array(
						"NAME" => "Бокс - ".$basketItem->getField("NAME"),
						"QUANTITY" => 1,
						"CUSTOM_PRICE" => "Y",
						"PRICE" => $basketItemProps["CUSTOM_SUMMA"]["VALUE"],
					));
					$arForPropCollection = Array(
						array(
							'NAME' => 'Количество месяцев',
							'CODE' => 'ORDER_MONTH_COUNT',
							'VALUE' => $basketItemProps["ORDER_MONTH_COUNT"]["VALUE"],
							'SORT' => 10,
						),
						array(
							'NAME' => 'Дата начала аренды',
							'CODE' => 'DATE_START',
							'VALUE' => $basketItemProps["DATE_START"]["VALUE"],
							'SORT' => 20,
						),
						array(
							'NAME' => 'Дата окончания аренды',
							'CODE' => 'DATE_END',
							'VALUE' => $basketItemProps["DATE_END"]["VALUE"],
							'SORT' => 30,
						),
					);
					
					/*
					if (strlen($basketItemProps["PRICE_GUARANTEE"]["VALUE"]) > 0) {
						$arForPropCollection[] = Array(
							'NAME' => 'Гарантийный депозит, руб.',
							'CODE' => 'PRICE_GUARANTEE',
							'VALUE' => $basketItemProps["PRICE_GUARANTEE"]["VALUE"],
							'SORT' => 40,
						);
					}
					if (strlen($basketItemProps["PRICE_INSURANCE"]["VALUE"]) > 0) {
						$arForPropCollection[] = Array(
							'NAME' => 'Страховка, руб.',
							'CODE' => 'PRICE_INSURANCE',
							'VALUE' => $basketItemProps["PRICE_INSURANCE"]["VALUE"],
							'SORT' => 50,
						);
					}
					*/
					
					for ($i = 0; $i < $quantity; $i++) {
						$arForPropCollection[] = Array(
							'NAME' => 'Аренда с ' . $arOrderPeriod[$i]["DATE_START"] . " по " . $arOrderPeriod[$i]["DATE_END"] . ", руб.",
							'CODE' => 'ORDER_MONTH_' . ($i + 1),
							'VALUE' => $arOrderPeriod[$i]["PRICE"],
							'SORT' => 100,
						);
					}
					
					if(count($arResDiscounts) > 0) {
						$propCollSort = 200;
						foreach($arResDiscounts as $key_arResDisc=>$arResDisc) {
							$propCollSort = $propCollSort + intval($key_arResDisc);
							$arForPropCollection[] = Array(
								"NAME" => "ID скидки",
								"CODE" => "DISCOUNT_".($key_arResDisc)."_ID",
								"VALUE" => $arResDisc["REAL_DISCOUNT_ID"],
								"SORT" => $propCollSort,
							);						
							$arForPropCollection[] = Array(
								"NAME" => "Название скидки",
								"CODE" => "DISCOUNT_".($key_arResDisc)."_NAME",
								"VALUE" => $arResDisc["NAME"],
								"SORT" => $propCollSort,
							);
							$arForPropCollection[] = Array(
								"NAME" => "Размер скидки",
								"CODE" => "DISCOUNT_".($key_arResDisc)."_VALUE",
								"VALUE" => $arResDisc["ACTIONS_DESCR"]["BASKET"],
								"SORT" => $propCollSort,
							);						
						}
					}
					
					
					$basketPropertyCollection = $basketItem->getPropertyCollection();
					$basketPropertyCollection->setProperty($arForPropCollection);
					$basketPropertyCollection->save();

					//сохраняем корзину и заказ
					$basket->save();
					$order->save();

					/*$event->addResult(
						new Bitrix\Main\EventResult(
							Bitrix\Main\EventResult::SUCCESS, $order
						)
					);*/	

					Bitrix\Sale\TradingPlatform\Helper::notifyNewOrder($order->getId(), "s1");

					break;
				}
			}
		}
		
        //file_put_contents("/home/bitrix/www/test/debug.txt", print_r($order, true));
    }
}

/*
************ end ***************
*/


/*
Прописываем значения свойства DEAL_CATEGORY_ID в зависимости от содержимого корзины заказа
*/
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderSaved',
    'SetDealCategoryIdAfterSaveNewOrder'
);
function SetDealCategoryIdAfterSaveNewOrder(\Bitrix\Main\Event $event)
{
    Bitrix\Main\Loader::includeModule("sale");
    Bitrix\Main\Loader::includeModule("catalog");
    Bitrix\Main\Loader::includeModule("iblock");
    Bitrix\Main\Loader::includeModule("currency");

    $order = $event->getParameter("ENTITY");
    $isNew = $event->getParameter("IS_NEW");

    //if ($isNew) {
		$basket = $order->getBasket();
		$basketItems = $basket->getBasketItems();
		//ищем, есть ли в заказе счета
		$is_invoice = false;
		foreach ($basketItems as $basketItem) {
			$itemIblock = CIBlockElement::GetIBlockByID($basketItem->getProductId());
			if($itemIblock == INVOICES_IBLOCK) {
				$is_invoice = true;
			}
		}
		//ищем, есть ли в заказе пополнение баланса
		$is_balance = false;
		foreach ($basketItems as $basketItem) {
			$itemIblock = CIBlockElement::GetIBlockByID($basketItem->getProductId());
			if($itemIblock == BALANCE_ITEMS_IBLOCK) {
				$is_balance = true;
			}
		}
		//задаем значение свойству DEAL_CATEGORY_ID
		$dealCatID = "";
		if($is_invoice) {
			$dealCatID = "3";
		} elseif($is_balance) {
			$dealCatID = "4";
		}
		
		//если $dealCatID не пустой, то прописываем его в свойство заказа DEAL_CATEGORY_ID
		if(strlen($dealCatID) > 0) {
			$propertyCollection = $order->getPropertyCollection();
			$arProps = $propertyCollection->getArray();
			foreach($arProps["properties"] as $prop) {
				if($prop["CODE"] == "DEAL_CATEGORY_ID") {
					$myPropValue = $propertyCollection->getItemByOrderPropertyId($prop["ID"]);
					$myPropValue->setValue($dealCatID);
					$myPropValue->save();
					
					break;
				}
			}
		}        
    //}
}

/*
************ end ***************
*/


/*
Очищаем корзину после сохранения заказа
*/
\Bitrix\Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderSaved',
    'ClearBasketAfterSaveNewOrder'
);
function ClearBasketAfterSaveNewOrder(\Bitrix\Main\Event $event)
{
    Bitrix\Main\Loader::includeModule("sale");

    $order = $event->getParameter("ENTITY");
    $isNew = $event->getParameter("IS_NEW");

    if ($isNew) {
        CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
    }
}

/*
************ end ***************
*/





/*
Изменение письма события SALE_NEW_ORDER
*/
AddEventHandler("sale", "OnOrderNewSendEmail", "OnOrderNewSendEmailHandler");
function OnOrderNewSendEmailHandler($orderID, &$eventName, &$arFields) { 
	Bitrix\Main\Loader::includeModule("sale");
	Bitrix\Main\Loader::includeModule("iblock");
	Bitrix\Main\Loader::includeModule("catalog");
	
	$order = \Bitrix\Sale\Order::load($orderID);
	$basket = $order->getBasket();
	$propertyCollection = $order->getPropertyCollection();	
	
	//подмена имени пользователя
	$rsUser = CUser::GetByID($order->getUserId());
	$arUser = $rsUser->Fetch();	
	if(strlen($arUser["NAME"]) > 0) {
		$arFields["ORDER_USER"] = $arUser["NAME"];
	} else {
		$arFields["ORDER_USER"] = "покупатель";
	}
	//Логин
	$arFields["ORDER_USER_LOGIN"] = $arUser["LOGIN"];
	
	//Поиск названия и адреса склада
	$arFields["SKLAD_INFO"] = "";
	foreach ($basket as $basketItem) {
		if(strlen($arFields["SKLAD_INFO"]) == 0) {
			$itemIblock = CIBlockElement::GetIBlockByID($basketItem->getProductId());
			if ($itemIblock == STORAGES_CATALOG_IBLOCK) {
				$res = CIBlockElement::GetByID($basketItem->getProductId());
				if($arProd = $res->GetNext()) {	 
					$rsSect = CIBlockSection::GetList(array("id"=>"asc"), Array("IBLOCK_ID"=>STORAGES_CATALOG_IBLOCK, "ID"=>$arProd["IBLOCK_SECTION_ID"]), false, Array("IBLOCK_ID", "ID", "NAME", "UF_*"));
					if($arSect = $rsSect->GetNext()) {
						$arFields["SKLAD_INFO"] = "Склад и адрес - ".$arSect["NAME"].", ".$arSect["UF_ADDRESS"]."<br/>";
					}	
				}			
			}
		}
	}
	$arFields["ORDER_LIST"] = $arFields["SKLAD_INFO"].$arFields["ORDER_LIST"];
	
	//fix <br> in ORDER_LIST
	$arFields["ORDER_LIST"] = str_replace("</br>", "<br/>", $arFields["ORDER_LIST"]);
	
	if(strlen($arFields["EMAIL"]) == 0) {
		//email
		$emailPropValue = $propertyCollection->getUserEmail();
		$arFields["EMAIL"] = $emailPropValue->getValue();
	}

	
	$arProps = $propertyCollection->getArray();
	foreach($arProps["properties"] as $prop) {
		if($prop["CODE"] == "IS_ORDER_FROM_CRM") {
			$myPropValue = $propertyCollection->getItemByOrderPropertyId($prop["ID"]);
			$IS_ORDER_FROM_CRM = $myPropValue->getValue();
						
			break;
		}
	}
	
	//отменяем отправку письма, если заказ создан на основе сделки Б24
	if($IS_ORDER_FROM_CRM == "Y") {
		return false;
	}
}
?>
