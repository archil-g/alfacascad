<?php
function deb($data, $admin = false)
{
    global $USER;

    if (!is_object($USER)) {
        return;
    }

    if ($USER->IsAdmin() || !$admin) {
        if (is_array($data) || is_object($data)) {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
        } else {
            echo $data . '<br />' . PHP_EOL;
        }
    }
}