<?if(!$isIndex):?>						<?CPriority::checkRestartBuffer();?>
					<?endif;?>
					<?IncludeTemplateLangFile(__FILE__);?>
					<?global $arTheme, $isIndex, $is404, $isCatalog, $isServices;?>
					<?if(!$isIndex && !$isCatalog && !$isProjects):?>
							<?if($is404):?>
								</div>
							<?else:?>
									<?if(!$isMenu):?>
										</div><?// class=col-md-12 col-sm-12 col-xs-12 content-md?>
									<?elseif($isMenu && $arTheme["SIDE_MENU"]["VALUE"] == "LEFT" && !$isBlog):?>
										<?CPriority::get_banners_position('CONTENT_BOTTOM');?>
										</div><?// class=col-md-9 col-sm-9 col-xs-8 content-md?>
									<?elseif($isMenu && ($arTheme["SIDE_MENU"]["VALUE"] == "RIGHT" || $isBlog)):?>
										<?CPriority::get_banners_position('CONTENT_BOTTOM');?>
										</div><?// class=col-md-9 col-sm-9 col-xs-8 content-md?>
										<div class="col-md-3 col-sm-3 hidden-xs hidden-sm right-menu-md">
											<?CPriority::ShowPageType('left_block')?>
										</div>
									<?endif;?>
								<?endif;?>
								</div><?// class=row?>
						<?if($APPLICATION->GetProperty("FULLWIDTH")!=='Y'):?>
							</div><?// class="maxwidth-theme?>
						<?endif;?>
					<?elseif($isIndex):?>
						<?CPriority::ShowPageType('indexblocks');?>
					<?endif;?>
				</div><?// class=container?>
				<?CPriority::get_banners_position('FOOTER');?>
			</div><?// class=main?>
		</div><?// class=body?>
		<!-- Global site tag (gtag.js) - Google Ads: 1015215436 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-1015215436"></script><script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-1015215436'); </script>

		<?CPriority::ShowPageType('footer', '', 'FOOTER_TYPE');?>
		<div class="bx_areas">
			<?CPriority::ShowPageType('bottom_counter');?>
		</div>
		<?CPriority::SetMeta();?>
		<?CPriority::ShowPageType('search_title_component');?>
		<?CPriority::ShowPageType('basket_component');?>
		<?CPriority::AjaxAuth();?>
		<script>
		jQuery(document).on('click', 'form[name="aspro_priority_callback"] button[type="submit"]', function() {
			var m = jQuery(this).closest('form');
			var fio = jQuery('#POPUP_FIO').val();
			var phone = jQuery('#POPUP_PHONE').val();
			var ct_site_id = '26167';
			var sub = 'Обратный звонок';
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!phone && !!fio){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
		<script>
		jQuery(document).on('click', 'form[name="formManagerOrder_2"] button[class*="btn"]', function() {
			var m = jQuery(this).closest('form');
			var fio = m.find('input[name="form_text_54"]').val();
			var phone = m.find('input[name="form_text_55"]').val();
			var mail = m.find('input[name="form_email_57"]').val();
			var comment = m.find('input[name="form_text_58"]').val();
			var ct_site_id = '26167';
			var sub = 'Форма Возможности и преимущества АльфаСклад';
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				email: mail,
				comment: comment,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!phone && !!fio && !!mail){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
		<script>
		jQuery(document).on('click', 'form[name="formManagerOrder_3"] button[class*="btn"]', function() {
			var m = jQuery(this).closest('form');
			var fio = m.find('input[name="form_text_60"]').val();
			var phone = m.find('input[name="form_text_61"]').val();
			var mail = m.find('input[name="form_email_63"]').val();
			var ct_site_id = '26167';
			var sub = 'Форма Отправить информацию менеджеру';
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				email: mail,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!phone && !!fio){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
		<script>
		jQuery(document).on('click', 'form[name="aspro_priority_question"] button[type="submit"]', function() {
			var m = jQuery(this).closest('form');
			var fio = jQuery('#POPUP_NAME').val();
			var phone = jQuery('#POPUP_PHONE').val();
			var mail = jQuery('#POPUP_EMAIL').val();
			var comment = m.find('#POPUP_MESSAGE').val();
			var ct_site_id = '26167';
			var sub = 'Форма Задать вопрос';
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				email: mail,
				comment: comment,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!phone && !!fio && !!comment){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
		<script>
		jQuery(document).on('click', 'form[name="aspro_priority_add_review"] button[type="submit"]', function() {
			var m = jQuery(this).closest('form');
			var fio = jQuery('#POPUP_NAME').val();
			var comment = m.find('#POPUP_MESSAGE').val();
			var ct_site_id = '26167';
			var sub = 'Форма Оставьте свой отзыв';
			var ct_data = {
				fio: fio,
				comment: comment,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!fio && !!comment){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
		<script>
		jQuery(document).on('click', 'form[name="formManagerOrder"] button[clas*="btn"]', function() {
			var m = jQuery(this).closest('form');
			var fio = m.find('input[name="form_text_52"]').val();
			var phone = m.find('input[name="form_text_53"]').val();
			var ct_site_id = '26167';
			var sub = 'Форма Заявка через менеджера';
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!fio){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
		<script>
		jQuery(document).on('click', 'form[name="formManagerOrder_5"] button[type="submit"]', function() {
			var m = jQuery(this).closest('form');
			var fio = jQuery('#POPUP_NAME').val();
			var phone = jQuery('#POPUP_PHONE').val();
			var ct_site_id = '26167';
			var sub = 'Форма Сообщить об освобождении бокса';
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!phone && !!fio){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
		<script>
		jQuery(document).on('click', 'form[name="aspro_priority_order_services"] button[type="submit"]', function() {
			var m = jQuery(this).closest('form');
			var fio = jQuery('#POPUP_NAME').val();
			var phone = jQuery('#POPUP_PHONE').val();
			var mail = jQuery('#POPUP_EMAIL').val();
			var comment = m.find('#POPUP_MESSAGE').val();
			var ct_site_id = '26167';
			var sub = 'Форма Заказать услугу';
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				email: mail,
				comment: comment,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!phone && !!fio && !!comment){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
		<script>
		jQuery(document).on('click', 'form[name="formManagerOrder_4"] button[class*="btn"]', function() {
			var m = jQuery(this).closest('form');
			var fio = m.find('input[name="form_text_66"]').val();
			var phone = m.find('input[name="form_text_67"]').val();
			var mail = m.find('input[name="form_email_68"]').val();
			var ct_site_id = '26167';
			var sub = 'Форма Отправьте заявку прямо сейчас';
			var ct_data = {
				fio: fio,
				phoneNumber: phone,
				email: mail,
				subject: sub,
				sessionId: window.call_value
			};
			if (!!phone && !!fio && !!mail){
				jQuery.ajax({
					url: 'https://api-node8.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
					dataType: 'json', type: 'POST', data: ct_data, async: false
				});
			}
		});
		</script>
<!--        <link rel="stylesheet" href="--><?//=SITE_TEMPLATE_PATH?><!--/_template__styles.css">-->
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/responsive.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/custom.css">
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/custom-2.css">
	</body>
</html>