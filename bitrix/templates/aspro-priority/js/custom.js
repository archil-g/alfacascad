/* Add here all your JS customizations */


$(function() {

	/******** fix for tabs url ********/
	var hash = window.location.hash;
	hash && $('.faq_list ul.nav a[href="' + hash + '"]').tab('show');

	$('.faq_list .nav-tabs a').click(function (e) {
		window.location.hash = this.hash;
	});
	/********************************/


	/******* scroll do yakorya *******/
	$('a.scroll[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top-100
				}, 1000);
				return false;
			}
		}
	});
	/**************************************/


	/***** masked input *****/
	$(".form-managerOrder input[type='tel']").inputmask({"mask": "+7 (999) 999-99-99"});
	$(".form-managerOrder2 input[type='tel']").inputmask({"mask": "+7 (999) 999-99-99"});
	$(".form-managerOrder4 input[type='tel']").inputmask({"mask": "+7 (999) 999-99-99"});
	/*************************************/

	/********* basket page ***********/
	$('html').on('click', '#basket-item-table .box-more-info .item-price-info-open a', function() {
		$(this).parents('.box-more-info').find('.item-price-info').slideToggle();
	});
	/*********************/

	/********* mobile footer menu slideToggle ***********/
	$('html').on('click', '#footer .showMobileFooterMenu', function() {
		$('#footer .mobileFooterMenu').slideToggle();
	});
	/*********************/

	/************ forms input-date ************/
	$('html').on('click', '.form.popup .input .form-control.input-date, .form.popup .input .form-control.input-date', function() {
		$(this).parents('.input').find('.calendar-icon').trigger('click');
	})
	/*********************/


	/************ LazyLoad ************/
	var lazyLoadInstance = new LazyLoad({
		elements_selector: ".lazy"
	});
	lazyLoadInstance.update();
	/***********************/

	/************ Optimisation ********/
	var map = $('.map-shadow');
	var map2 = $('.ajax_map');
	var gallery_mainpage = $('.gallery_mainpage');
	var mainpage_price_storages = $('.mainpage_price_storages');



	setTimeout(function() {
		mainpage_price_storages.removeClass('hidden');
	}, 2000);

	setTimeout(function() {
		map.removeClass('hidden');
		map2.removeClass('hidden');
		gallery_mainpage.removeClass('hidden');
		$('.gallery_mainpage #carousel').flexslider({
			animation: 'slide',
			controlNav: false,
			animationLoop: true,
			slideshow: true,
			itemWidth: 200,
			itemMargin: 12,
			directionNav: true,
			touch: true,
			minItems: 2,
			maxItems: 8,
			start: function(){
				$('.gallery_mainpage #carousel').height('auto');
				$('.gallery_mainpage #carousel').css({'width': 'auto', 'opacity': 1});
			}
		});


		$('.banners-big-loader-mobile').hide();
		$('.banners-big.front').css('display', 'block');
		$('.banners-big.front .flexslider').flexslider();



	}, 4000);



	setTimeout(function() {
		$('.mainpage_price_storages').css('display', 'block');
		$('.storage_list_withmap').css('display', 'block');
		$('.storage_list_withmap .flexslider').flexslider();

		$('.map-shadow').css('display', 'block');

		const head = document.getElementsByTagName('head')[0];
		const appCss = document.getElementById('app-css');
		const link = document.createElement('link');
		link.rel = 'stylesheet';
		link.href = 'bitrix/templates/aspro-priority/_template__styles.css';
		head.before(link);
		//$('.bx_areas').load('/include/invis-counter.php');
	}, 10000);

	/***********         ************/
});




