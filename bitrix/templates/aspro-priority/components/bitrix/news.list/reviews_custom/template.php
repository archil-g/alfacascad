<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?
$this->setFrameMode(true);
?>


<?if(count($arResult["ITEMS"]) > 0) {?>
	<?if($arParams["AJAX_LOAD"] != "Y") {?>
		<div class="reviews-block">
			<div class="reviews-list">
	<?}?>
	
				<?foreach($arResult["ITEMS"] as $key=>$arItem) {?>
					<div class="item">
						<div class="head">
							<?if($arItem["PROPERTIES"]["HIDE_NAME"]["VALUE"] == "Y") {?>
								<div class="name">Пользователь скрыл свои данные</div>
							<?} else {?>
								<?if(strlen($arItem["PREVIEW_PICTURE"]["RESIZE"]["src"]) > 0) {?>
									<div class="image">
										<img src="<?=$arItem["PREVIEW_PICTURE"]["RESIZE"]["src"]?>" />
									</div>
								<?}?>
								<div class="name"><?=$arItem["NAME"]?></div>
							<?}?>
						</div>
						<div class="rating-block">
							<?
							$ratingValue = ($arItem["PROPERTIES"]["RATING"]["VALUE"] ? $arItem["PROPERTIES"]["RATING"]["VALUE"] : 0);
							?>
							<div class="rating_wrap clearfix">
								<div class="rating current_<?=$ratingValue?>">
									<span class="stars_current "></span>
								</div>
								<?if(!empty($arItem["SKLAD"])) {?>
									<div class="sklad"><a href="/about/reviews/<?=$arItem["SKLAD"]["CODE"]?>/"><?=$arItem["SKLAD"]["NAME"]?></a></div>
								<?}?>
							</div>
						</div>
						<div class="message">
							<?=$arItem["PROPERTIES"]["MESSAGE"]["~VALUE"]["TEXT"]?>
						</div>
						<div class="date">
							<?=$arItem["DISPLAY_ACTIVE_FROM"]?>
						</div>
					</div>
				<?}?>
				
				<?
				$NavPageNomer = intval($arResult["NAV_RESULT"]->NavPageNomer);
				$NavPageCount = intval($arResult["NAV_RESULT"]->NavPageCount);
				?>
				<?if($NavPageNomer < $NavPageCount) {?>
					<div class="pagination-block">
						<a class="btn btn-default wc ajax_load_reviews" href="javascript:void(0);" data-page="<?=$NavPageNomer+1?>" data-section-id="<?=$arResult["SECTION"]["PATH"][0]["ID"]?>" data-sklad-id="<?=$GLOBALS["arrFilterSkladList"]["PROPERTY_SKLAD"]?>" data-count="<?=$arParams["NEWS_COUNT"]?>"><i class="fa fa-plus"></i><span>Показать больше отзывов</span></a>
					</div>
				<?}?>
	<?if($arParams["AJAX_LOAD"] != "Y") {?>				
			</div>	
		</div>
	<?}?>	
<?} else {?>
	<?if($arParams["AJAX_LOAD"] != "Y") {?>
		<p>Отзывы не найдены</p>
	<?}?>
<?}?>


