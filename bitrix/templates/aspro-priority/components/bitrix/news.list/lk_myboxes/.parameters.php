<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters = array(
	'IS_CANCELED' => array(
		'NAME' => 'Выводить ранее арендованные боксы',
		'TYPE' => 'CHECKBOX',
		'DEFAULT' => '',
	),
);
?>
