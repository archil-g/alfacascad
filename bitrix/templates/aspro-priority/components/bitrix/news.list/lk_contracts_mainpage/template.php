<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
ob_start();
?>

<?if(count($arResult["ITEMS"]) > 0) {?>
	<div class="contracts_list_mainpage lk_table">
		<table class="table">
			<thead>
				<tr>
					<th>Номер бокса</th>
					<th>Оплачено до</th>
					<th>Баланс по договору</th>
					<th>Неоплаченные счета</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?foreach($arResult["ITEMS"] as $arItem) {?>
					<tr>
						<td><?=$arItem["BOX"]["PROPERTY_BOX_NUMBER_VALUE"]?></td>
						<td><?=$arItem["PROPERTIES"]["PAID_DATE_TO"]["VALUE"]?></td>
						<td><?=FormatCurrency($arItem["PROPERTIES"]["BALANCE"]["VALUE"], "RUB")?></td>
						<td>
							<?if(count($arItem["INVOICES"]) > 0) {?>
								<?foreach($arItem["INVOICES"] as $arInvoice) {?>
									<div class="invoice_link">Счет за <a href="/cabinet/docs/invoices_not_paid/"><?=$arInvoice["MONTH"]?> г.</a></div>
								<?}?>
							<?} else {?>
								-
							<?}?>
						</td>
						<td style="text-align: right;">
							<a class="btn btn-transparent btn-xs" href="/cabinet/balance/?CONTRACT_ID=<?=$arItem["ID"]?>">Пополнить баланс</a>
						</td>
					</tr>
				<?}?>
			</tbody>
		</table>
	</div>
<?}?>

<?
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean();
?>