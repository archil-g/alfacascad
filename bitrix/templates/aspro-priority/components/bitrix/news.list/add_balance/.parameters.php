<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters = array(
	'USER_ID' => array(
		'NAME' => 'USER_ID',
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
	'CONTRACT_ID' => array(
		'NAME' => 'CONTRACT_ID',
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
);
?>
