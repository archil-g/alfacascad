<?require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";?>

<?
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");
Bitrix\Main\Loader::includeModule("iblock");


if($_REQUEST["ACTION"] == "ADD_BALANCE") {
	if(strlen($_REQUEST["PRODUCT_ID"]) > 0) {
		//очищаем корзину
		CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
		
		//добавляем в корзину товар
		$basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
		$productId = intval($_REQUEST["PRODUCT_ID"]);
		$quantity = 1;		
        $item = $basket->createItem("catalog", $productId);
		
		$arSetFields = array(
            "QUANTITY" => $quantity,
            "CURRENCY" => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
            "LID" => Bitrix\Main\Context::getCurrent()->getSite(),
            "PRODUCT_PROVIDER_CLASS" => 'CCatalogProductProvider',
        );
		if(strlen($_REQUEST["CUSTOM_SUM"]) > 0) {
			$arSetFields["CUSTOM_PRICE"] = "Y";
			$arSetFields["PRICE"] = $_REQUEST["CUSTOM_SUM"];
		}
		
		if(strlen($_REQUEST["CONTRACT_GUID"]) > 0) {			
			//записываем свойства
			$basketPropertyCollection = $item->getPropertyCollection();
			$arForPropCollection = Array(
				Array(
					"NAME" => "Номер договора",
					"CODE" => "CONTRACT_NUMBER",
					"VALUE" => $_REQUEST["CONTRACT_NUMBER"],
					"SORT" => 100,
				),
				Array(
					"NAME" => "ID договора",
					"CODE" => "CONTRACT_ID",
					"VALUE" => $_REQUEST["CONTRACT_ID"],
					"SORT" => 110,
				),
				Array(
					"NAME" => "GUID договора",
					"CODE" => "CONTRACT_GUID",
					"VALUE" => $_REQUEST["CONTRACT_GUID"],
					"SORT" => 120,
				)			
			);
			
			//ищем склад по боксу в договоре
			if(strlen($_REQUEST["BOX_ID"]) > 0) {
				$db_sections = CIBlockElement::GetElementGroups($_REQUEST["BOX_ID"], true);
				if($arSect = $db_sections->Fetch()) {
					$arForPropCollection[] = Array(
						"NAME" => "Внешний код склада",
						"CODE" => "SKLAD_XML_ID",
						"VALUE" => $arSect["XML_ID"],
						"SORT" => 130,
					);
				}			
			}
			
			$basketPropertyCollection->setProperty($arForPropCollection);
		}
		
        $item->setFields($arSetFields);
        $basket->save();

		echo "OK";
	}
}
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>