<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<?
\Bitrix\Main\Loader::includeModule("sale");

$arResult["ADDITIONAL_FIELDS"] = Array();
$arSkipFilelds = Array("NAME", "LAST_NAME", "SECOND_NAME", "EMAIL");

$db_sales = CSaleOrderUserProps::GetList(
    array("USER_ID" => "ASC"),
    array("USER_ID" => $arResult["arUser"]["ID"]),
    false,
    false,
    array("*")
);

if($arSaleProfile = $db_sales->Fetch()) {
	$db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID"=>$arSaleProfile["ID"]));
	while ($arPropVals = $db_propVals->Fetch()) {        
		if(!in_array($arPropVals["PROP_CODE"], $arSkipFilelds)) {
			$arResult["ADDITIONAL_FIELDS"][] = $arPropVals;
		}
	}	
}
?>