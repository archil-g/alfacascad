<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//ищем продукты для детализации
$arResult["PRODUCTS"] = Array();
if(!empty($arResult["PROPERTIES"]["PRODUCTS"]["VALUE"])) {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_*");
	$arFilter = Array("IBLOCK_ID"=>INVOICES_PRODUCTS_IBLOCK, "ID"=>$arResult["PROPERTIES"]["PRODUCTS"]["VALUE"]);
	$res = CIBlockElement::GetList(Array("ID"=>"ASC"), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement()) { 
		$arFields = $ob->GetFields();  
		$arProps = $ob->GetProperties();
		$arResult["PRODUCTS"][] = Array("FIELDS"=>$arFields, "PROPS"=>$arProps);
	}	
}
?>

<?$this->__component->SetResultCacheKeys(array("PROPERTIES"));?>
