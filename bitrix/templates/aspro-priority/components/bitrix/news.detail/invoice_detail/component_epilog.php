<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
global $APPLICATION;
global $USER;

//заголовки
$APPLICATION->SetTitle("Детализация счета №".$arResult["PROPERTIES"]["NUMBER"]["VALUE"]);
$APPLICATION->AddChainItem("Детализация счета №".$arResult["PROPERTIES"]["NUMBER"]["VALUE"], "");

//проверка на принадлежность счета пользователю
if($USER->GetID() != $arResult["PROPERTIES"]["USER"]["VALUE"]) {
	//редирект на общую страницу счетов, если счет не принадлежит пользователю
	LocalRedirect("/cabinet/docs/invoices/");
}

?>