<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>



<?if(count($arResult["PRODUCTS"]) > 0) {?>
	<div class="invoices_list lk_table">
		<table class="table">
			<thead>
				<tr>
					<th>Название</th>
					<th>Сумма</th>
				</tr>
			</thead>
			<tbody>
				<?foreach($arResult["PRODUCTS"] as $arItem) {?>
					<tr>
						<td><?=$arItem["FIELDS"]["NAME"]?></td>
						<td><?=FormatCurrency($arItem["PROPS"]["PRICE"]["VALUE"], "RUB")?></td>						
					</tr>
				<?}?>
			</tbody>
		</table>		
	</div>
<?}?>