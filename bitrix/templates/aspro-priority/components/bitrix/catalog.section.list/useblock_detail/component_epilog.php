<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
global $APPLICATION;

if(isset($arResult["SECTION"]["NAME"])) {
    $APPLICATION->SetTitle($arResult["SECTION"]["NAME"]);
	$APPLICATION->AddChainItem($arResult["SECTION"]["NAME"], "");
}
if(isset($arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_TITLE"])) {
	$APPLICATION->SetPageProperty("title", $arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_TITLE"]);
}
if(isset($arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"])) {
	$APPLICATION->SetPageProperty("keywords", $arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"]);
}
if(isset($arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"])) {
	$APPLICATION->SetPageProperty("description", $arResult["SECTION"]["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]);
}



if($arResult["SECTION"]["ID"] == 0) {
	if (Bitrix\Main\Loader::includeModule("iblock")) {
		Bitrix\Iblock\Component\Tools::process404(
			'Страница не найдена'
			,true
			,true
			,true
			,'/404.php'
		);
	}
}
?>


<?
global $arrElementsFilter;
$arrElementsFilter["SECTION_ID"] = $arResult["SECTION"]["ID"];
?>
<?$arResult["CACHED_TPL"] = preg_replace_callback(
    "/#ELEMENTS#/is".BX_UTF_PCRE_MODIFIER,
    create_function('$matches', 'ob_start();
    $GLOBALS["APPLICATION"]->IncludeComponent(
        "bitrix:news.list", 
        "useblock_detail", 
        array(
            "COMPONENT_TEMPLATE" => "useblock_detail",
            "IBLOCK_TYPE" => "aspro_priority_content",
            "IBLOCK_ID" => "41",
            "NEWS_COUNT" => "30",
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_BY2" => "ID",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "arrElementsFilter",
            "FIELD_CODE" => array(
                0 => "NAME",
                1 => "PREVIEW_PICTURE",
                2 => "PREVIEW_TEXT",
            ),
            "PROPERTY_CODE" => array(
                0 => "HIDE_TITLE",
                1 => "",
            ),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "60",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "j F Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_LAST_MODIFIED" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "b-mainpage-news",
            "INCLUDE_SUBSECTIONS" => "N",
            "STRICT_SECTION_CHECK" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "N",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "PAGER_TEMPLATE" => ".default",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "Новости",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "SET_STATUS_404" => "N",
            "SHOW_404" => "N",
            "MESSAGE_404" => ""
        ),
        false,
		Array("HIDE_ICONS"=>"Y")
    );
    $retrunStr = @ob_get_contents();
    ob_get_clean();
    return $retrunStr;'),
    $arResult["CACHED_TPL"]);
?>


<?// вывод
echo $arResult["CACHED_TPL"];
?>