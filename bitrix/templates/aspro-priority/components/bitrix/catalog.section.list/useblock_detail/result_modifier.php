<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
$arResult["SECTION"]["TISERS"] = Array();
if(count($arResult["SECTION"]["UF_TISERS_ITEMS"]) > 0) {
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE","PROPERTY_*");
	$arFilter = Array("IBLOCK_ID"=>42, "ACTIVE"=>"Y", "ID"=>$arResult["SECTION"]["UF_TISERS_ITEMS"]);
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement()) { 
		$arFields = $ob->GetFields();  
		$arProps = $ob->GetProperties();
		$arResult["SECTION"]["TISERS"][] = Array("ID"=>$arFields["ID"], "NAME"=>$arFields["NAME"], "PICTURE"=>CFile::GetPath($arFields["PREVIEW_PICTURE"]), "LINK"=>$arProps["LINK"]["VALUE"], "LINK_TEXT"=>$arProps["LINK"]["DESCRIPTION"]);
	}
}
?>

<?$this->__component->SetResultCacheKeys(array("CACHED_TPL"));?>