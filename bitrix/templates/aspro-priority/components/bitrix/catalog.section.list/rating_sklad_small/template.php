<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(!empty($arResult["DATA"])) {?>
	<div class="rating_small_block">
		<div class="row">
			<div class="col-md-12">
				<div class="review_block">
					<div class="head">
						<div class="name"><?=$arResult["DATA"]["REVIEW"]["PROPERTY_NAME_VALUE"]?></div>
					</div>
					<div class="rating-block">
						<?
						$ratingValue = ($arResult["DATA"]["REVIEW"]["PROPERTY_RATING_VALUE"] ? $arResult["DATA"]["REVIEW"]["PROPERTY_RATING_VALUE"] : 0);
						?>
						<div class="rating_wrap clearfix">
							<div class="rating current_<?=$ratingValue?>">
								<span class="stars_current "></span>
							</div>														
						</div>
					</div>
					<div class="message">
						<?=$arResult["DATA"]["REVIEW"]["~PROPERTY_MESSAGE_VALUE"]["TEXT"]?>
					</div>
					<div class="date">
						<?=$arResult["DATA"]["REVIEW"]["ACTIVE_FROM"]?>
					</div>
				</div>				
			</div>
			<div class="col-md-12">
				<div class="buttons">
					<?
					$link = "/about/reviews/";
					if(strlen($arResult["DATA"]["CODE"]) > 0) {
						$link = "/about/reviews/".$arResult["DATA"]["CODE"]."/";
					}
					?>
					<a class="btn btn-default btn-xs btn-transparent" href="<?=$link?>">Все отзывы</a>
					<span class="btn btn-default btn-xs btn-transparent" data-event="jqm" data-param-id="30" data-name="add_review">Оставить отзыв</span>
				</div>			
			</div>			
		</div>
	</div>
<?}?>