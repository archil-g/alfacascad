<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters = array(
	'SQUARE_FROM' => array(
		'NAME' => 'SQUARE_FROM',
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
	'SQUARE_TO' => array(
		'NAME' => 'SQUARE_TO',
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
);
?>