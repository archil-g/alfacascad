<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters = array(
	'NEWS_COUNT' => array(
		'NAME' => 'Количество отзывов на странице',
		'TYPE' => 'STRING',
		'DEFAULT' => '3',
	),
);
?>