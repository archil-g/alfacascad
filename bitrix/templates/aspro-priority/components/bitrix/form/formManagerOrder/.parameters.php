 <?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters = array(
    'PHONE' => array(
        'NAME' => 'Номер телефона',
        'TYPE' => 'STRING',
        'DEFAULT' => '',
    ),
    'EMAIL' => array(
        'NAME' => 'Адрес e-mail',
        'TYPE' => 'STRING',
        'DEFAULT' => '',
    ),
);
?> 