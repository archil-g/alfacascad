<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
global $APPLICATION;

$GLOBALS["BUY_ONE_CLICK_BOX_ID"] = $arParams["PRODUCT_ID"];
?>


<?$arResult["CACHED_TPL"] = preg_replace_callback(
    "/#BOX_INFO#/is".BX_UTF_PCRE_MODIFIER,
    create_function('$matches', 'ob_start();
    $GLOBALS["APPLICATION"]->IncludeComponent(
        "bitrix:catalog.element", 
        "buy_one_click_box_info", 
        array(
            "COMPONENT_TEMPLATE" => "buy_one_click_box_info",
            "IBLOCK_TYPE" => "aspro_priority_catalog",
            "IBLOCK_ID" => "40",
            "ELEMENT_ID" => $GLOBALS["BUY_ONE_CLICK_BOX_ID"],
            "ELEMENT_CODE" => "",
            "SECTION_ID" => "",
            "SECTION_CODE" => "",
            "SHOW_DEACTIVATED" => "N",
            "HIDE_NOT_AVAILABLE_OFFERS" => "N",
            "OFFERS_LIMIT" => "0",
            "BACKGROUND_IMAGE" => "-",
            "TEMPLATE_THEME" => "blue",
            "PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
            "PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
            "ADD_PICT_PROP" => "-",
            "LABEL_PROP" => array(
            ),
            "DISPLAY_NAME" => "Y",
            "IMAGE_RESOLUTION" => "16by9",
            "SHOW_SLIDER" => "N",
            "DETAIL_PICTURE_MODE" => array(
                0 => "POPUP",
                1 => "MAGNIFIER",
            ),
            "ADD_DETAIL_TO_SLIDER" => "N",
            "DISPLAY_PREVIEW_TEXT_MODE" => "E",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "N",
            "SHOW_MAX_QUANTITY" => "N",
            "SHOW_CLOSE_POPUP" => "N",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "USE_VOTE_RATING" => "N",
            "USE_COMMENTS" => "N",
            "BRAND_USE" => "N",
            "MESS_PRICE_RANGES_TITLE" => "Цены",
            "MESS_DESCRIPTION_TAB" => "Описание",
            "MESS_PROPERTIES_TAB" => "Характеристики",
            "MESS_COMMENTS_TAB" => "Комментарии",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "CHECK_SECTION_ID_VARIABLE" => "N",
            "SEF_MODE" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "Y",
            "SET_TITLE" => "N",
            "SET_CANONICAL_URL" => "N",
            "SET_BROWSER_TITLE" => "N",
            "BROWSER_TITLE" => "-",
            "SET_META_KEYWORDS" => "N",
            "META_KEYWORDS" => "-",
            "SET_META_DESCRIPTION" => "N",
            "META_DESCRIPTION" => "-",
            "SET_LAST_MODIFIED" => "N",
            "USE_MAIN_ELEMENT_SECTION" => "N",
            "STRICT_SECTION_CHECK" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "ADD_ELEMENT_CHAIN" => "N",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "DISPLAY_COMPARE" => "N",
            "PRICE_CODE" => array(
                0 => "BASE",
            ),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "PRICE_VAT_SHOW_VALUE" => "N",
            "CONVERT_CURRENCY" => "N",
            "USE_RATIO_IN_RANGES" => "N",
            "BASKET_URL" => "/personal/basket.php",
            "USE_PRODUCT_QUANTITY" => "N",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "ADD_PROPERTIES_TO_BASKET" => "N",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "ADD_TO_BASKET_ACTION" => array(
                0 => "BUY",
            ),
            "ADD_TO_BASKET_ACTION_PRIMARY" => array(
                0 => "BUY",
            ),
            "LINK_IBLOCK_TYPE" => "",
            "LINK_IBLOCK_ID" => "",
            "LINK_PROPERTY_SID" => "",
            "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
            "USE_GIFTS_DETAIL" => "N",
            "USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
            "USE_ENHANCED_ECOMMERCE" => "N",
            "SET_STATUS_404" => "N",
            "SHOW_404" => "N",
            "MESSAGE_404" => "",
            "COMPATIBLE_MODE" => "Y",
            "USE_ELEMENT_COUNTER" => "N",
            "DISABLE_INIT_JS_IN_COMPONENT" => "N",
            "SET_VIEWED_IN_COMPONENT" => "N"
        ),
        false,
        Array("HIDE_ICONS"=>"Y")
    );
    $retrunStr = @ob_get_contents();
    ob_get_clean();
    return $retrunStr;'),
    $arResult["CACHED_TPL"]);
?>



<?// вывод
echo $arResult["CACHED_TPL"];
?>