<?if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true ) die();?>

<?
//подставляем значение поля "Срок аренды"
if(strlen($arParams["COUNT"]) > 0) {
	foreach($arResult["QUESTIONS"] as $key=>$arQuestion) {
		if($key == "COUNT") {
			$arResult["QUESTIONS"][$key]["HTML_CODE"] = str_replace('value=""', 'value="'.$arParams["COUNT"].' мес."', $arResult["QUESTIONS"][$key]["HTML_CODE"]);
			$arResult["QUESTIONS"][$key]["VALUE"] = $arParams["COUNT"]." мес.";
		}
	}
}

//добавление поля PRODUCT_ID
$arNewQuestion = Array();
if(strlen($arParams["PRODUCT_ID"]) > 0) {
	foreach($arResult["arQuestions"] as $key=>$arQuestion) {
		if($arQuestion["CODE"] == "PRODUCT_ID") {
			$arNewQuestion = array(
				"NAME" => $arQuestion["NAME"],
				"CODE" => $arQuestion["CODE"],
				"IS_REQUIRED" => $arQuestion["IS_REQUIRED"],
				"HINT" => $arQuestion["HINT"],
				"DEFAULT" => $arQuestion["DEFAULT_VALUE"],
				"ICON" => $arQuestion["XML_ID"],
				"FIELD_TYPE" => "hidden",
				"MULTIPLE" => $arQuestion["MULTIPLE"],
				"VALUE" => $arParams["PRODUCT_ID"],
				"HTML_CODE" => '<input type="hidden" id="'.$arQuestion["CODE"].'" name="'.$arQuestion["CODE"].'" value="'.$arParams["PRODUCT_ID"].'" />'
			);	
		}	
	}
}
if(!empty($arNewQuestion)) {
	$arResult["QUESTIONS"][$arNewQuestion["CODE"]] = $arNewQuestion;
}

?>


<?$this->__component->SetResultCacheKeys(array("CACHED_TPL"));?>