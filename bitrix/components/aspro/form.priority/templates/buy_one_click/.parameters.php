<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters = array(
	'PRODUCT_ID' => array(
		'NAME' => 'PRODUCT_ID',
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
	'COUNT' => array(
		'NAME' => 'COUNT',
		'TYPE' => 'STRING',
		'DEFAULT' => '',
	),
);
?>