<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?
$arResult["SQUARE_TO"] = 15; //default

if(strlen($arParams["SKLAD_CODE"]) > 0) {
	//ищем максимальную площадь бокса на данном складе
	CModule::IncludeModule("iblock");
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_SQUARE");
	$arFilter = Array("IBLOCK_ID"=>STORAGES_CATALOG_IBLOCK, "ACTIVE"=>"Y", "SECTION_CODE"=>$arParams["SKLAD_CODE"], "PROPERTY_STATUS"=>BOX_STATUS_OPENED_ID);
	$res = CIBlockElement::GetList(Array("property_SQUARE"=>"desc"), $arFilter, false, Array("nTopCount"=>50), $arSelect);
	if($ob = $res->GetNextElement()) {
		$arItem = $ob->GetFields();
		$arResult["SQUARE_TO"] = $arItem["PROPERTY_SQUARE_VALUE"];
	}

	if($arResult["SQUARE_TO"] <= 1) {
		$arResult["SQUARE_TO"] = 2;
	}
}
?>

<?$this->__component->SetResultCacheKeys(array("CACHED_TPL", "MAP_POINTS"));?>