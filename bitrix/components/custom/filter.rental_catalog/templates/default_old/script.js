$(document).ready(function() {
	// селектор выбора площади
	var $range_square = $('.box_filter .filter_block .range_square');
    $range_square.ionRangeSlider({
		skin: 'round',
        type: 'double',
        min: 1,
        max: BX.message('SQUARE_TO'),
        from: 1,
        to: BX.message('SQUARE_TO'),
        grid: true,
		grid_snap: true,
		postfix: " м<sup>2</sup>",
    });
	var timeoutID_sendAjaxData;
    $range_square.on("change", function () {
        var $inp = $(this);
        var square_from = parseInt($inp.data("from"));
		var square_to = parseInt($inp.data("to"));
		var sklad_code = $('.box_filter .sklad_code').val();
		var floor_code = $('.box_filter .floor_code').val();
		var boxes_list = $('.box_filter .boxes_list').val();

		clearInterval(timeoutID_sendAjaxData);
		timeoutID_sendAjaxData = setTimeout(sendAjaxData, 1000, square_from, square_to, sklad_code, floor_code, boxes_list, 1);	

		$('.rental_catalog .ajaxPreloader').show();
		$('.rental_catalog_slider .ajaxPreloader').show();
		
		//формируем ссылку с кнопки "Перейти к выбору бокса" при упрощенном отображении
		var def_url = $('.box_filter .buttons .simple_view_url').attr('data-href');
		$('.box_filter .buttons .simple_view_url').attr('href', def_url+'?SQUARE_FROM='+square_from+'&SQUARE_TO='+square_to);
    });		
	
	//Связь селектора выбора площади и GET параметра SQUARE_FROM
	var range_square_instance = $range_square.data("ionRangeSlider");
	var square_from = parseInt(getUrlParameter(window.location.search.substring(1), 'SQUARE_FROM'));
	var square_to = parseInt(getUrlParameter(window.location.search.substring(1), 'SQUARE_TO'));
	if(square_from > 0) {
		range_square_instance.update({
			from: square_from,
		});		
	}
	if(square_to > 0) {
		range_square_instance.update({
			to: square_to,
		});		
	}	
	
	
	
	// селектор выбора месяцев
	var $range_months = $('.box_filter .filter_block .range_months');
    $range_months.ionRangeSlider({
		skin: 'round',
        min: 0,
        max: 36,
		from_min: 1,
        from: 1,
        grid: true,
		grid_num: 6,
		postfix: " мес.",		
    });
	// Связь селектора выбора месяцев и кнопок со скидками
    $range_months.on("change", function () {
        var $inp = $(this);
        var monthsCnt = parseInt($inp.data("from"));
		
		$('.box_filter .sale_block .sale_btn .icon-text').addClass('grey');
		$('.box_filter .sale_block .sale_btn').each(function(index) {
			var saleBtnFrom = parseInt($(this).find('.icon-text').attr('data-from'));
			var saleBtnTo = parseInt($(this).find('.icon-text').attr('data-to'));
			
			if((monthsCnt >= saleBtnFrom) && (monthsCnt <= saleBtnTo)) {
				$(this).children('.icon-text').removeClass('grey');
			}
		});
		
		$('.rental_catalog_list .buy_block .counter .input input').val($range_months.data('from')).trigger('input');
    });
	var range_months_instance = $range_months.data("ionRangeSlider");
    $('html').on('click', '.box_filter .sale_block .sale_btn .icon-text', function () {
        var monthsCnt = $(this).attr('data-from');   
        range_months_instance.update({
            from: monthsCnt,
        });
    });


	$('body').on('click', '.rental_catalog_list .pagination li a', function(e) {
		e.preventDefault();
		var pagenUrl = $(this).attr('href');		
		pagenUrl = pagenUrl.substr(pagenUrl.indexOf('?')+1);
		var pagen = parseInt(getUrlParameter(pagenUrl, 'PAGEN_1'));
		
		var sklad_code = $('.box_filter .sklad_code').val();
		var floor_code = $('.box_filter .floor_code').val();		
		var boxes_list = $('.box_filter .boxes_list').val();		
		sendAjaxData($range_square.data('from'), $range_square.data('to'), sklad_code, floor_code, boxes_list, pagen)
	})
	
	
	
	
	
	
	//ajax обновление данных о боксах
	function sendAjaxData(square_from, square_to, sklad_code, floor_code, boxes_list, pagen) {
		$.post("/bitrix/components/custom/filter.rental_catalog/templates/.default/ajax.php", {ACTION:"FILTER", "SKLAD_CODE":sklad_code, "FLOOR_CODE":floor_code, "SQUARE_FROM":square_from, "SQUARE_TO":square_to, "FILTERED_BOXES_LIST":boxes_list, "PAGEN_1":pagen}, function(data){
			$('.ajax_rentalCatalogList').empty().append(data);
			$('.rental_catalog_list .buy_block .counter .input input').val($range_months.data('from')).trigger('input');
			setTimeout(function(){
				$('.rental_catalog .ajaxPreloader').hide();
			}, 500);			
		});
		$.post("/bitrix/components/custom/filter.rental_catalog/templates/.default/ajax.php", {ACTION:"UPDATE_MIN_PRICE", "SKLAD_CODE":sklad_code, "FLOOR_CODE":floor_code, "SQUARE_FROM":square_from, "SQUARE_TO":square_to, "FILTERED_BOXES_LIST":boxes_list}, function(data){
			$('.box_filter .start_price .price').empty().append(data);			
		});		
		$.post("/bitrix/components/custom/filter.rental_catalog/templates/.default/ajax.php", {ACTION:"UPDATE_SLIDER", "SKLAD_CODE":sklad_code, "FLOOR_CODE":floor_code, "SQUARE_FROM":square_from, "SQUARE_TO":square_to, "FILTERED_BOXES_LIST":boxes_list}, function(data){
			$('.ajax_rentalCatalogListSlider').empty().append(data);
			$('.box_filter_slider #carousel').flexslider({
				animation: 'slide',
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				itemWidth: 100,
				itemMargin: 12,
				directionNav: false,
				touch: true,
				minItems: 2,
				maxItems: 8,
				asNavFor: '.box_filter_slider #slider',
				start: function(){
					$('.box_filter_slider').height('auto');
					$('.box_filter_slider #carousel').css({'width': 'auto', 'opacity': 1});
				}
			});

			$('.box_filter_slider #slider').flexslider({
				animation: 'slide',
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				directionNav: true,
				sync: '.box_filter_slider #carousel',
			});			
			setTimeout(function(){
				$('.rental_catalog_slider .ajaxPreloader').hide();
			}, 500);			
		});			
	}	
});





var getUrlParameter = function getUrlParameter(sUrl, sParam) {
    var sPageURL = decodeURIComponent(sUrl),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};