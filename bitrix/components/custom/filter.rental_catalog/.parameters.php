<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
        "SKLAD_CODE" => array(
            "PARENT" => "BASE",
            "NAME" => "SKLAD_CODE",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "FLOOR_CODE" => array(
            "PARENT" => "BASE",
            "NAME" => "FLOOR_CODE",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "BOXES_LIST" => array(
            "PARENT" => "BASE",
            "NAME" => "BOXES_LIST",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "PROP_SIZE" => array(
            "PARENT" => "BASE",
            "NAME" => "PROP_SIZE",
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
	),
);
?>