<?require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";?>
<?
CModule::IncludeModule("iblock");

if($_REQUEST["ACTION"] == "SAVE") {
	
	global $USER;
	
	if($USER->IsAuthorized()) {
		$result = Array();
		
		//формируем массив с данными для записи в ИБ
		$PROP = array();
		$PROP["USER"] = $USER->GetID();
		$PROP["BOX"] = $_REQUEST["LOAD_DATA"]["BOX_ID"];
		if(count($_REQUEST["LOAD_DATA"]["FILES"]) > 0) {
			foreach($_REQUEST["LOAD_DATA"]["FILES"] as $key=>$file) {
				$PROP["FILES"]["n".$key] = CFile::MakeFileArray($file); 
			}
		}
		
		$arLoadData = Array(
			"MODIFIED_BY"       => $USER->GetID(), 
			"IBLOCK_SECTION_ID" => false,
			"IBLOCK_ID"         => INVENTORY_IBLOCK,
			"PROPERTY_VALUES"   => $PROP,
			"NAME"              => "Опись на бокс с ID=".$_REQUEST["LOAD_DATA"]["BOX_ID"],
			"ACTIVE"            => "Y",
			"DETAIL_TEXT"       => $_REQUEST["LOAD_DATA"]["DETAIL_TEXT"]
		);
		
		$el = new CIBlockElement;
	
		if(strlen($_REQUEST["LOAD_DATA"]["INVENTORY_ID"]) > 0) {
			//редактируем элемент
			if($el->Update($_REQUEST["LOAD_DATA"]["INVENTORY_ID"], $arLoadData)) {
				$result = Array("STATUS"=>"OK");
			}			
		} else {
			//создаем элемент
			if($el->Add($arLoadData)) {
				$result = Array("STATUS"=>"OK");
			}
		}		
		
		echo json_encode($result);
	}
	
}


if($_REQUEST["ACTION"] == "LOAD_FILE") {

    $error = false;
    $errorText = "";
    $uploaddir = $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/";
    $newFileName = date("dmY-His-").rand(100, 999);
    $newFile = $_FILES[0];
    
    global $USER;
    
    if($USER->IsAuthorized()) {
        if($newFile["name"] !== '' && $newFile["error"] == 0) {
            $arTmpExt = explode(".", $newFile["name"]);
            $ext = ".".$arTmpExt[count($arTmpExt)-1];
            $filetypes = array('.bmp','.jpg','.png','.jpeg','.BMP','.JPG','.PNG','.JPEG');
            
            if(in_array($ext, $filetypes)) {
                if($newFile["size"] <= 2097152) {
                    if(move_uploaded_file($newFile["tmp_name"], $uploaddir.$newFileName.$ext)) { 
                        $fileSRC = $uploaddir.$newFileName.$ext;
                    } else {
                        $error = true;
                        $errorText = "Не удалось загрузить файл";
                    }                
                } else {
                    $error = true;
                    $errorText = "Размер файла не должен превышать 2 mb";            
                }            
            } else {
                $error = true;
                $errorText = "Расширение файла ".$ext." не поддерживается";        
            }    
        } else {
            $error = true;
            $errorText = "Не удалось загрузить файл";    
        }
    }
    
    if($error) {
        $result = Array("ERROR"=>$error, "ERROR_TEXT"=>$errorText);
    } else {
        $result = Array("ERROR"=>$error, "FILE_SRC"=>$fileSRC, "FILE_ORIGINAL_NAME"=>$newFile["name"]);        
    }
    
    echo json_encode($result);
    
}

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>