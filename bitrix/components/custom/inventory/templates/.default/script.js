$( document ).ready(function() {

    //добавление еще одного поля под файл
    $('html').on('click', '.inventory .load-file-block .add-field-block a', function() {
        var field = $('.inventory .load-file-block .default_field .load-item');
		var maxFiles = parseInt($('.inventory').attr('data-max-files'))-1;
		var countFiles = parseInt($('.inventory').attr('data-count-files'));
		
		if(countFiles < maxFiles) {
			countFiles++;
			$('.inventory').attr('data-count-files', countFiles);
			$(this).before(field.clone());
		}
		if(countFiles >= maxFiles) {
			$(this).hide();
		}
    });
	
	//клик по кнопке Загрузить файл
    $('html').on('click', '.inventory .load-file-block .load-item .btn', function() {
        $(this).parents('.load-item').find('input[type="file"]').trigger('click');
    });	
	
	//загрузка файла
	var files;
    $('html').on('change', '.inventory .load-file-block .FILE', function() {
        files = $(this).prop("files");
        $(this).parents('form').submit();        
    });	
    $('html').on('submit', '.inventory .load-file-block .load-item form', function(e) {
        e.stopPropagation();
        e.preventDefault();
        var thisForm = $(this);           
        var formData = new FormData();
        $.each(files, function(key, value){
            formData.append(key, value);
        });
        formData.append('ACTION', 'LOAD_FILE');
        
        $.ajax({
            url: $(this).attr('action'),
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            type: 'POST',
            success: function(response){
                var myData = JSON.parse(response);
                if(myData['ERROR']) {
                    thisForm.find('.status').empty().append(myData['ERROR_TEXT']);
                    thisForm.find('.FILE').attr('data-file-src', '');
                } else {
                    thisForm.find('.status').empty().append(myData['FILE_ORIGINAL_NAME']);;
                    thisForm.find('.FILE').attr('data-file-src', myData['FILE_SRC']);
                }
                
            }
        });        
    });	
	

    //сохранение описи
    $('html').on('click', '.inventory .SAVE', function() {
        var btn = $(this);
        var LOAD_DATA = new Object();
		var obgFile = new Object();
		var fileCouter = 0;
		
		LOAD_DATA['DETAIL_TEXT'] = $('.inventory .DETAIL_TEXT').val();
		LOAD_DATA['INVENTORY_ID'] = $('.inventory').attr('data-inventory-id');
		LOAD_DATA['BOX_ID'] = $('.inventory').attr('data-box-id');
		
		$('.inventory .load-file-block .load-item .FILE').each(function(index) {
			if($(this).attr('data-file-src').length > 0) {
				obgFile[fileCouter] = $(this).attr('data-file-src');
				fileCouter++;
			}
        });
        LOAD_DATA['FILES'] = obgFile;
        
        if($(btn).attr("data-ctrl") != "Y") {
            $(btn).attr("data-ctrl", "Y");
            $.post($(this).attr('data-action'), {ACTION:"SAVE", "LOAD_DATA":LOAD_DATA}, function(data) {
                $(btn).attr("data-ctrl", "N");
                var myData = JSON.parse(data);
                if(myData['STATUS'] == 'OK') {
                    window.location.reload();
                }
            })
        }
    });

});
