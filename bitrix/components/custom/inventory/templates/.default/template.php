<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true);?>


<?if($arResult["DATA"]["IS_ACCESS"] == "Y") {?>
	<?
	$maxFiles = 10;
	?>
	<div class="inventory" data-inventory-id="<?=$arResult["DATA"]["INVENTORY_ID"]?>" data-box-id="<?=$arParams["BOX_ID"]?>" data-max-files="<?=$maxFiles?>" data-count-files="<?=count($arResult["DATA"]["FILES"])?>">
		<div class="text-block">
			<h3>Описание:</h3>
			<textarea class="DETAIL_TEXT" placeholder="Введите текст..."><?=$arResult["DATA"]["DETAIL_TEXT"]?></textarea>
		</div>
		<div class="pic-block">
			<h3>Фотографии:</h3>
			<?if(count($arResult["DATA"]["FILES"]) > 0) {?>
				<div class="row">
					<?foreach($arResult["DATA"]["FILES"] as $arFile) {?>
						<div class="col-md-3 col-xs-6">
							<div class="picture">
								<a class="link" href="<?=$arFile["SRC_FULL"]?>" target="_blank">
									<img src="<?=$arFile["SRC_SMALL"]?>" />
								</a>
							</div>
						</div>
					<?}?>
				</div>
			<?}?>
			<div class="load-file-block">
				<div class="default_field">
					<div class="load-item">
						<form action="<?=$templateFolder?>/ajax.php">
							<span class="btn btn-transparent btn-xs">Загрузить фото</span>
							<input type="file" class="FILE" data-file-src="" />
							<span class="status"></span>
						</form>
					</div>				
				</div>
				
				<?if(count($arResult["DATA"]["FILES"]) < $maxFiles) {?>
					<div class="load-item">
						<form action="<?=$templateFolder?>/ajax.php">
							<span class="btn btn-transparent btn-xs">Загрузить фото</span>
							<input type="file" class="FILE" data-file-src="" />
							<span class="status"></span>
						</form>
					</div>
				<?}?>
				<?if(count($arResult["DATA"]["FILES"]) < $maxFiles-1) {?>
					<div class="add-field-block">
						<a class="add-field">+ добавить еще один файл</a>
					</div>
				<?}?>
			</div>
		</div>
		<div class="save-block">
			<span class="btn btn-default SAVE" data-action="<?=$templateFolder?>/ajax.php">Сохранить изменения</span>
		</div>
	</div>
<?} else {?>
	<p>У вас нет прав на просмотр этой страницы.</p>
<?}?>