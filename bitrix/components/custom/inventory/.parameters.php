<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "IBLOCK_ID",
            "TYPE" => "STRING",
			"DEFAULT" => "",
        ),
        "USER_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "USER_ID",
            "TYPE" => "STRING",
			"DEFAULT" => "",
        ),
        "BOX_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "BOX_ID",
            "TYPE" => "STRING",
			"DEFAULT" => "",
        ),
        "CONTRACTS_IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "CONTRACTS_IBLOCK_ID",
            "TYPE" => "STRING",
			"DEFAULT" => "",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>3600)
	),
);
?>