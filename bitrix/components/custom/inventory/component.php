<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["USER_ID"] = intval($arParams["USER_ID"]);
$arParams["BOX_ID"] = intval($arParams["BOX_ID"]);
$arParams["CONTRACTS_IBLOCK_ID"] = intval($arParams["CONTRACTS_IBLOCK_ID"]);

$arResult = Array();

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, false, "/s1/custom/inventory/"))
{
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		return;
	}
	
	if(($arParams["USER_ID"] > 0) and ($arParams["BOX_ID"] > 0)) {
		//проверяем принадлежность бокса пользователю
		$isAccess = false;
		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_BOX");
		$arFilter = Array("IBLOCK_ID"=>$arParams["CONTRACTS_IBLOCK_ID"], "PROPERTY_USER"=>$arParams["USER_ID"], "PROPERTY_BOX"=>$arParams["BOX_ID"]);
		$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, Array("nTopCount"=>1), $arSelect);
		if($ob = $res->GetNextElement()) {
			$arBoxFields = $ob->GetFields();
			$isAccess = true;
		}		
	
		//поиск описи в ИБ
		if($isAccess) {
			$arResult["DATA"]["IS_ACCESS"] = "Y";			
			$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DETAIL_TEXT", "PROPERTY_*");
			$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "PROPERTY_USER"=>$arParams["USER_ID"], "PROPERTY_BOX"=>$arParams["BOX_ID"]);
			$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, Array("nTopCount"=>1), $arSelect);
			if($ob = $res->GetNextElement()) {
				$arFields = $ob->GetFields();  
				$arProps = $ob->GetProperties();
								
				$arResult["DATA"]["INVENTORY_ID"] = $arFields["ID"];
				$arResult["DATA"]["DETAIL_TEXT"] = $arFields["~DETAIL_TEXT"];
				foreach($arProps["FILES"]["VALUE"] as $fileID) {
					$arResult["DATA"]["FILES"][] = Array("ID"=>$fileID, "SRC_FULL"=>CFile::GetPath($fileID), "SRC_SMALL"=>CFile::ResizeImageGet($fileID, array("width"=>250, "height"=>250), BX_RESIZE_IMAGE_EXACT, false)["src"]);
				}
			}
		} else {
			$arResult["DATA"]["IS_ACCESS"] = "N";
		}
	}
	
	$this->SetResultCacheKeys(array(
		"DATA",
	));        

	$this->IncludeComponentTemplate();
}

?>