<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;

$IBLOCK_SKLAD_ID = 40;

if(!CModule::IncludeModule("iblock")){
    $this->AbortResultCache();
    return;
}

$arResult = Array();

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, false, "/s1/custom/payment.sberbank"))
{
	$arFilter = array("IBLOCK_ID"=>$IBLOCK_SKLAD_ID, "DEPTH_LEVEL"=>1, "ACTIVE"=>"Y");
	$rsSect = CIBlockSection::GetList(array("sort"=>"asc"), $arFilter);
	while($arSklad = $rsSect->GetNext()) {
		$arResult["DATA"]["SKLAD_LIST"][] = Array("ID"=>$arSklad["ID"], "NAME"=>$arSklad["NAME"]);
	}	
	
	$this->SetResultCacheKeys(array(
		"DATA",
	));        

	$this->IncludeComponentTemplate();
}

?>