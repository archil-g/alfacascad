<?require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";?>
<?
CModule::IncludeModule("iblock");

if($_REQUEST["ACTION"] == "PAYMENT") {

	$arResult = Array();
	
	if(strlen($_REQUEST["FULL_NAME"]) == 0) {
		$arResult["ERROR"]["FULL_NAME"] = 'Введите фамилию и имя!';
	}
	if(strlen($_REQUEST["SUMM"]) == 0) {
		$arResult["ERROR"]["SUMM"] = 'Введите сумму!';
	}
	if(strlen($_REQUEST["ACCOUNT_NUMBER"]) == 0) {
		$arResult["ERROR"]["ACCOUNT_NUMBER"] = 'Введите номер счета!';
	}	
	
	if(empty($arResult["ERROR"])) {
		$amount = str_replace(' ', '', $_POST['SUMM']);
		$floatAmount = floatval(str_replace(',', '.', $amount));
		$formatedAmount = number_format($floatAmount, 2, ',', '');
		$getAmount = str_replace(',', '', $formatedAmount);
		$getOrderNumber = $_POST['ACCOUNT_NUMBER'];
		$getDescription = $_POST['FULL_NAME'];
		
		if ($getAmount && $getDescription && $getOrderNumber) {
			$params = Array(
			'amount' => $getAmount, // Сумма пополнения
			'orderNumber' => $getOrderNumber, // Внутренний ID заказа
			'description' => $getDescription, // Описание
			'password' => '7PKBx%T2f', // Здесь пароль от вашего API юзера в Сбербанке
			'userName' => 'alfasklad-api', // А тут его логин
			'returnUrl' => 'https://alfasklad.ru/payment/success_payment/'     // URL куда вернуть пользователя после перечисления средств
			);
		
			$opts = Array(// А здесь параметры для POST запроса
			'http' => Array(
				'method' => 'POST', 
				'header' => "Content-type: application/x-www-form-urlencoded",
				'content' => http_build_query($params),
				'timeout' => 60
			)
			);
		
			// И отправляем эти данные на сервер сбербанка
			$context = stream_context_create($opts);
			// Здесь должен быть URL тестового и боевого сервера Сбербанка
			$url = 'https://securepayments.sberbank.ru/payment/rest/register.do';
			//$url2 = 'https://securepayments.sberbank.ru/payment/rest/getOrderStatus.do';
			$result = file_get_contents($url, false, $context);
		
			// Расшифруем полученные данные в массив $arSbrfResult
			$arSbrfResult = (array) json_decode($result);
			if(!$arSbrfResult['errorMessage']) {
				$arResult["OK"]["URL"] = $arSbrfResult['formUrl'];
			} else {
				$arResult["OK"]["TEXT"] = 'Заказ с таким номером уже обработан. В случае, если вы не успели произвести оплату, вам необходимо скорректировать номер счета, поставив после первой цифры в счете «/» (дробь) или «–» (тире). Например: 123456789 – 1/23 ';
			}
			
		}
	}
	
	echo json_encode($arResult);

}

?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>