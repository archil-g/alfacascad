<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>


<div class="form_payment_sberbank">
	<div class="form_block">
		<form id="PAYMENT_SBERBANK" method="post" action="<?=$templateFolder?>/ajax.php">
			<input type="hidden" name="ACTION" value="PAYMENT" />
			<div class="error_block"></div>
			<div class="form-body">
				<div class="row" data-sid="FULL_NAME">
					<div class="col-md-12">
						<div class="form-group animated-labels">
							<label for="FULL_NAME">Фамилия и имя<span class="required-star">*</span></label>
							<div class="input">
								<input type="text" id="FULL_NAME" name="FULL_NAME" class="form-control required" value="" aria-required="true">
							</div>
						</div>
					</div>
				</div>
				<div class="row" data-sid="SUMM">
					<div class="col-md-12">
						<div class="form-group animated-labels">
							<label for="SUMM">Сумма<span class="required-star">*</span></label>
							<div class="input">
								<input type="text" id="SUMM" name="SUMM" class="form-control required" value="" aria-required="true">
							</div>
						</div>
					</div>
				</div>				
				<div class="row" data-sid="ACCOUNT_NUMBER">
					<div class="col-md-12">
						<div class="form-group animated-labels">
							<label for="ACCOUNT_NUMBER">Номер счета<span class="required-star">*</span></label>
							<div class="input">
								<input type="text" id="ACCOUNT_NUMBER" name="ACCOUNT_NUMBER" class="form-control required" value="" aria-required="true">
							</div>
							<span class="hint">Вводить только цифры (без дополнительных знаков и без даты выставления счета).</span>
						</div>
					</div>
				</div>
				<?if(count($arResult["DATA"]["SKLAD_LIST"]) > 0) {?>
					<div class="row" data-sid="SKLAD">
						<div class="col-md-12">
							<div class="form-group animated-labels">
								<select class="form-control" id="SKLAD" name="SKLAD">
									<?foreach($arResult["DATA"]["SKLAD_LIST"] as $arSklad) {?>
										<option value="<?=$arSklad["ID"]?>"><?=$arSklad["NAME"]?></option>
									<?}?>
								</select>
							</div>
						</div>
					</div>
				<?}?>
				<div class="button">
					<a href="javascript:void(0);" class="btn-lg btn btn-default SEND">Оплатить</a>
				</div>				
			</div>
		</form>
	</div>

	<div class="text_block">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "inc",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/include/form-payment-sberbank-text.php"
			),
			$component,
			Array("HIDE_ICONS"=>"N")
		);?>		
	</div>

	<div class="pay_systems_icons">
		<div class="item">
			<img src="<?=$templateFolder?>/images/1.gif" />
		</div>
		<div class="item">
			<img src="<?=$templateFolder?>/images/2.gif" />
		</div>
		<div class="item">
			<img src="<?=$templateFolder?>/images/3.gif" />
		</div>
		<div class="item">
			<img src="<?=$templateFolder?>/images/visa.jpg" />
		</div>
		<div class="item">
			<img src="<?=$templateFolder?>/images/mir.jpg" />
		</div>
		<div class="item">
			<img src="<?=$templateFolder?>/images/mc.jpg" />
		</div>		
	</div>
</div>

