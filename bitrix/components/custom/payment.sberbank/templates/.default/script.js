$( document ).ready(function() {

    $('html').on('click', '#PAYMENT_SBERBANK .SEND', function() {        
        $('#PAYMENT_SBERBANK').submit();
    });

    $('#PAYMENT_SBERBANK').on('submit', function(e){    
        var f = $(this);
        $.ajax({
            type: f.attr('method'),
            url: f.attr('action'),
            data: f.serialize(),
            success: function(data)
            {
                var result = JSON.parse(data);

                if(result['ERROR']) {
					$('#PAYMENT_SBERBANK label.error').remove();
					$('#PAYMENT_SBERBANK .input.error').removeClass('error');
                    $.each(result['ERROR'], function(index, value) {
						$('#PAYMENT_SBERBANK *[data-sid="'+index+'"]').find('.input').addClass('error');
						$('#PAYMENT_SBERBANK *[data-sid="'+index+'"]').find('.input').after('<label class="error">'+value+'</label>');
					})
                } 
				
				if(result['OK']) {
					if(result['OK']['URL']) {
						location.href = result['OK']['URL'];
					} else {
						$('#PAYMENT_SBERBANK .error_block').empty().append(result['OK']['TEXT']);
					}
				}
            }
        });        
        
        e.preventDefault();
    });
	
});
