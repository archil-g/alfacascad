<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "IBLOCK_ID",
            "TYPE" => "STRING",
			"DEFAULT" => "",
        ),
        "QUESTION_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "ID вопроса",
            "TYPE" => "STRING",
			"DEFAULT" => "",
        ),
        "OPROSNIK_DATA" => array(
            "PARENT" => "BASE",
            "NAME" => "OPROSNIK_DATA",
            "TYPE" => "STRING",
			"DEFAULT" => "",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000)
	),
);
?>