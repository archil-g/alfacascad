<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CCacheManager $CACHE_MANAGER */
global $CACHE_MANAGER;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["QUESTION_ID"] = intval($arParams["QUESTION_ID"]);

$arResult = Array();

/*************************************************************************
			Work with cache
*************************************************************************/
if($this->StartResultCache(false, false, "/s1/custom/oprosnik/"))
{
	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		return;
	}

	$arFilter = array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ID"=>$arParams["QUESTION_ID"], "ACTIVE"=>"Y");
	$rsSect = CIBlockSection::GetList(array("sort"=>"asc"), $arFilter, false, Array("IBLOCK_ID", "ID", "NAME", "PICTURE", "UF_STEP"));
	if($arQuestion = $rsSect->GetNext()) {
		$arQ = Array();
		$arQ["ID"] = $arQuestion["ID"];
		$arQ["NAME"] = $arQuestion["NAME"];
		$arQ["STEP"] = $arQuestion["UF_STEP"];

		if(strlen($arQuestion["PICTURE"]) > 0) {
			$arQ["PICTURE"] = CFile::ResizeImageGet($arQuestion["PICTURE"], array("width"=>500, "height"=>300), BX_RESIZE_IMAGE_PROPORTIONAL, false);
		}

		$res = CIBlockElement::GetList(Array("sort"=>"asc"), Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "SECTION_ID"=>$arQuestion["ID"]), false, Array(), Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_*"));
		while($ob = $res->GetNextElement()) { 
			$arFields = $ob->GetFields();  
			$arProps = $ob->GetProperties();
			
			$arAnswer = Array();
			$arAnswer["ID"] = $arFields["ID"];
			$arAnswer["NAME"] = $arFields["NAME"];
			$arAnswer["NEXT_QUESTION"] = $arProps["NEXT_QUESTION"]["VALUE"];
			if(strlen($arFields["PREVIEW_PICTURE"]) > 0) {
				$arAnswer["ICON"] = CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array("width"=>45, "height"=>45), BX_RESIZE_IMAGE_PROPORTIONAL, false);
			}
			if(strlen($arProps["IS_NUMBER_ANSWER"]["VALUE"]) > 0) {
				$arAnswer["IS_NUMBER"] = "Y";
			}
			if(strlen($arProps["IS_TEXT_ANSWER"]["VALUE"]) > 0) {
				$arAnswer["IS_TEXT"] = "Y";
			}
			if(strlen($arProps["IS_CHECKBOX_MULTI_ANSWER"]["VALUE"]) > 0) {
				$arAnswer["IS_CHECKBOX_MULTI"] = "Y";
			}			
			
			$arQ["ANSWERS"][] = $arAnswer;
		}		
		
		$arResult["QUESTION"] = $arQ;
	}

	// получаем значения списка пользовательского свойства UF_STEPS
	$rsSteps = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID"=>74));
	while($arStep = $rsSteps->GetNext()) {
		if($arResult["QUESTION"]["STEP"] == $arStep["ID"]) {
			$arStep["CURRENT"] = "Y";
		}
		$arResult["STEPS"][] = Array("ID"=>$arStep["ID"], "VALUE"=>$arStep["VALUE"], "CURRENT"=>$arStep["CURRENT"]);
	}
	
	
	$this->SetResultCacheKeys(array(
		"QUESTION",
		"STEPS"
	));        

	$this->IncludeComponentTemplate();
}

?>