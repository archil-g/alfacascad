<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Стоимость аренды склада");
$APPLICATION->SetTitle("Цена");?><style>
.b-layout .bottom-red-border {
    padding-bottom: .625rem;
    border-bottom: 1px solid #e9565a;
}
.b-price-text {
    color: #000;
    font-size: 19px;
    font-weight: 400;
    line-height: 30px;
    letter-spacing: .57px;
}
.b-price-stuff-item {
    color: #e9565a;
    font-size: 19px;
    font-weight: 500;
    line-height: 30px;
    letter-spacing: .57px;
    margin-right: 50px;
}
.b-layout .cell {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 auto;
    flex: 0 0 auto;
    min-height: 0;
    min-width: 0;
    width: 100%;
	margin: 20px 0;
}
.bottom--fat-red-border {
    border-bottom: 5px solid #e9565a;
}
.b-price-table-title {
	/*height: 100px;*/
    color: #000;
    font-size: 18px;
    font-weight: 300;
    line-height: 30px;
    letter-spacing: .72px;
	padding-bottom: 20px;
}
table.b-price {
    border-collapse: collapse;
    width: 100%;
}
table.b-price tr {
    border-bottom: 1px solid #fff;
}
table.b-price tr th {
    font-size: 16px;
    font-weight: 500;
    line-height: 24px;
    letter-spacing: .57px;
    padding-bottom: 10px;
}
table.b-price tr th:first-of-type {
    text-align: left;
}
table.b-price tr th.action {
    color: #e9565a;
}
table.b-price tr td {
    padding-top: 28px;
    padding-bottom: 28px;
    padding-left: 17px;
    background: #f2f2f2;
    color: #000;
    font-size: 19px;
    font-weight: 300;
    line-height: 30px;
    letter-spacing: .57px;
}
table.b-price tr td:first-of-type {
    background: #ececec;
}
/*table.b-price tr td:last-of-type {
    background: #fad6d7;
}*/
table.b-price.sale-col tr td:last-of-type {
    background: #fad6d7;
}
.b-price-disc span {
    color: #e9565a;
}
.b-price-phone {
    color: #000;
    font-size: 20px;
    font-weight: 300;
    line-height: 30px;
    letter-spacing: .6px;
    margin-top: 45px;
}
.b-price-phone--center {
    text-align: center;
}
</style>
<div class="b-layout">
	<div class="cell large-12 bottom-red-border">
		<p class="b-price-text">
			 По нашему опыту, многие клиенты переоценивают площадь требуемого им Бокса. <br>
			 Вы можете позвонить или отправить заявку нашим менеджерам, чтобы более точно определить&nbsp;необходимый размер помещения.
		</p>
		<p>
			 Что входит в стоимость:
		</p>
		<p class="cell large-12">
 <span class="b-price-stuff-item">Страховка</span> <span class="b-price-stuff-item">Доступ 24/7</span> <span class="b-price-stuff-item">Охрана</span> <span class="b-price-stuff-item">Электричество</span> <span class="b-price-stuff-item">Быстрый Интернет</span> <span class="b-price-stuff-item">Тележки&nbsp; &nbsp;Освещение</span>
		</p>
	</div>
	<div class="row">
		<div class="col-md-6 col-xs-12">
			<p class="b-price-table-title bottom--fat-red-border">
 <b>ЗАО</b><br>
				 Цена Складских помещений за месяц аренды, вкл.&nbsp;НДС<br>
 <b>Склад на Молодогвардейской</b>
			</p>
			<table class="b-price sale-col">
			<tbody>
			<tr>
				<th>
					 Камера хранения,<br>
					 ячейка 1 м<sup>3</sup>
				</th>
				<th>
					 Цена<br>
					 руб. в месяц&nbsp; &nbsp; &nbsp;
				</th>
				<th class="action">
					 Акция! Скидка 50%*<br>
					 до 29.02.2020 г.&nbsp;&nbsp;
				</th>
			</tr>
			<tr>
				<td style="text-align: center;">
					 Ячейка размером 1 м<sup>3</sup>
				</td>
				<td style="text-align: center;">
					 1 610
				</td>
				<td style="text-align: center;">
					 860
				</td>
			</tr>
			</tbody>
			</table>
			<p class="b-price-disc b-price-disc--all-rent">
 <span style="color: #ee1d24;">* </span><span style="color: #ee1d24;">Скидка действительна в течение всего периода аренды</span>
			</p>
			<table class="b-price b-price--hight-box sale-col">
			<tbody>
			<tr>
				<th>
					 Высокий бокс, высота 3 м
				</th>
				<th>
					 Цена<br>
					 руб. в месяц&nbsp; &nbsp; &nbsp;
				</th>
				<th class="action">
					 Акция! Скидка 20%<br>
					 на первый платёж&nbsp; &nbsp; &nbsp;
				</th>
			</tr>
			<tr>
				<td style="text-align: center;">
					 1 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 1 930
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 1,5 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 2 850
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 2 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 3 650
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 3 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 5 370
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 4 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 7 140
				</td>
				<td align="center" style="text-align: center;">
					 5 734
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 5 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 8 640
				</td>
				<td align="center" style="text-align: center;">
					 6 934
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 6 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 10 290
				</td>
				<td align="center" style="text-align: center;">
					 8 254
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 7 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 11 930
				</td>
				<td style="text-align: center;">
					 9 566
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 8 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 13 680
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 9 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 15 310
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 10 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 17 040
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 12 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 20 030
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 15 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 24 920
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			</tbody>
			</table>
		</div>
		<div class="col-md-6 col-xs-12">
			<p class="b-price-table-title bottom--fat-red-border">
 <b>ВАО<br>
 </b>Цена Складских помещений за месяц аренды, вкл.&nbsp;НДС<br>
 <b>Склад шоссе Энтузиастов/МКАД</b>
			</p>
			<table class="b-price sale-col">
			<tbody>
			<tr>
				<th>
					 Камера хранения,<br>
					 ячейка 1 м<sup>3</sup>
				</th>
				<th>
					 Цена<br>
					 руб. в месяц&nbsp; &nbsp; &nbsp;
				</th>
				<th class="action">
					 Акция! Скидка 50%*<br>
					 до 29.02.2020 г.&nbsp; &nbsp;
				</th>
			</tr>
			<tr>
				<td style="text-align: center;">
					 Ячейка размером 1 м<sup>3</sup>
				</td>
				<td style="text-align: center;">
					 1 490
				</td>
				<td style="text-align: center;">
					 800
				</td>
			</tr>
			</tbody>
			</table>
			<p class="b-price-disc b-price-disc--all-rent">
 <span style="color: #ee1d24;">* Скидка действительна в течение всего периода аренды</span>
			</p>
			<table class="b-price sale-col">
			<tbody>
			<tr>
				<th>
					 Высокий бокс, высота 3 м
				</th>
				<th>
					 Цена<br>
					 руб. в месяц&nbsp; &nbsp; &nbsp;
				</th>
				<th class="action">
					 Акция! Скидка 20%<br>
					 на первый платёж&nbsp; &nbsp; &nbsp;
				</th>
			</tr>
			<tr>
				<td style="text-align: center;">
					 1 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 1 780
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 1,5 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 2 710
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 2 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 3 560
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 3 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 5 200
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 4 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 6 680
				</td>
				<td align="center" style="text-align: center;">
					 5 366
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 5 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 8 170
				</td>
				<td align="center" style="text-align: center;">
					 6&nbsp;558
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 6 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 9 470
				</td>
				<td align="center" style="text-align: center;">
					 7 598
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 7 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 10 720<br>
				</td>
				<td style="text-align: center;">
					 8 598
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 8 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 12 230
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 9 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 13 680
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 10 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 14 600
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 12 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 17 540
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			<tr>
				<td style="text-align: center;">
					 15 (м<sup>2</sup>)
				</td>
				<td style="text-align: center;">
					 21 900
				</td>
				<td style="text-align: center;">
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
<div class="cell large-12 b-price-phone b-price-phone--center">
	 Для брони свяжитесь с нашим менеджером: <a href="tel:+74955802047">8 (495) 580-20-47</a>
</div>
<div class="cell large-12 b-price-phone b-price-phone--center b-price-phone--margin-30">
	 Или оставьте заявку в форме ниже:
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>