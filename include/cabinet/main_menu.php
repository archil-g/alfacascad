<br>
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="photogallery_list_sections cabinet_menu">						
			<div class="item-views within services-items type_5">
				<div class="items flexbox">

					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-16.png" title="Мои заказы и боксы" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/orders/">Мои заказы и боксы</a></div>										
							</div>
							<a href="/cabinet/orders/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-17.png" title="Договоры и счета" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/docs/">Договоры и счета</a></div>										
							</div>
							<a href="/cabinet/docs/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-18.png" title="Мои обращения" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/forms/">Мои обращения</a></div>										
							</div>
							<a href="/cabinet/forms/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-19.png" title="Баланс" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/balance/">Баланс</a></div>										
							</div>
							<a href="/cabinet/balance/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-20.png" title="Настройки и личные данные" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/user/">Настройки и личные данные</a></div>										
							</div>
							<a href="/cabinet/user/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-21.png" title="Информационный раздел" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/info/">Информационный раздел</a></div>										
							</div>
							<a href="/cabinet/info/"></a>
						</div>
					</div>
					<?/*
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/cab_pic_04.png" title="Профили для заказа" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/profiles/">Профили для заказа</a></div>										
							</div>
							<a href="/cabinet/profiles/"></a>
						</div>
					</div>
					*/?>

				</div>        
			</div>							
		</div>
	</div>
</div>

<br>
<h3>Общая информация</h3>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="photogallery_list_sections cabinet_menu">						
			<div class="item-views within services-items type_5">
				<div class="items flexbox">

					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-22.png" title="Подбор нового бокса" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/rental_catalog/">Подбор нового бокса</a></div>										
							</div>
							<a href="/rental_catalog/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-23.png" title="Адреса складов" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/find_storage/">Адреса складов</a></div>										
							</div>
							<a href="/find_storage/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-24.png" title="Услуги" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/services/">Услуги</a></div>										
							</div>
							<a href="/services/"></a>
						</div>
					</div>				
					
				</div>        
			</div>							
		</div>
	</div>
</div>