<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="photogallery_list_sections cabinet_menu">						
			<div class="item-views within services-items type_5">
				<div class="items flexbox">

					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-13.png" title="Договоры" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/docs/contracts/">Договоры</a></div>										
							</div>
							<a href="/cabinet/docs/contracts/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-14.png" title="Все счета" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/docs/invoices/">Все счета</a></div>										
							</div>
							<a href="/cabinet/docs/invoices/"></a>
						</div>
					</div>

				</div>        
			</div>							
		</div>
	</div>
</div>