<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="photogallery_list_sections cabinet_menu">						
			<div class="item-views within services-items type_5">
				<div class="items flexbox">

					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-11.png" title="Заказы" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/orders/new/">Заказы</a></div>										
							</div>
							<a href="/cabinet/orders/new/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-12.png" title="Заказы в архиве" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/orders/archive/">Заказы в архиве</a></div>										
							</div>
							<a href="/cabinet/orders/archive/"></a>
						</div>
					</div>						

				</div>        
			</div>							
		</div>
	</div>
</div>