<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="photogallery_list_sections cabinet_menu">						
			<div class="item-views within services-items type_5">
				<div class="items flexbox">

					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-08.png" title="Мои текущие боксы" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/orders/myboxes/current/">Мои текущие боксы</a></div>										
							</div>
							<a href="/cabinet/orders/myboxes/current/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-07.png" title="Счета оплаченные" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/orders/myboxes/archive/">Ранее арендованные боксы</a></div>										
							</div>
							<a href="/cabinet/orders/myboxes/archive/"></a>
						</div>
					</div>

				</div>        
			</div>							
		</div>
	</div>
</div>