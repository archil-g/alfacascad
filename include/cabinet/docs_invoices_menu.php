<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="photogallery_list_sections cabinet_menu">						
			<div class="item-views within services-items type_5">
				<div class="items flexbox">

					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-14.png" title="Счета оплаченные" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/docs/invoices_paid/">Счета оплаченные</a></div>										
							</div>
							<a href="/cabinet/docs/invoices_paid/"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-15.png" title="Счета неоплаченные" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/docs/invoices_not_paid/">Счета неоплаченные</a></div>										
							</div>
							<a href="/cabinet/docs/invoices_not_paid/"></a>
						</div>
					</div>

				</div>        
			</div>							
		</div>
	</div>
</div>