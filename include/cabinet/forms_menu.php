<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="photogallery_list_sections cabinet_menu">						
			<div class="item-views within services-items type_5">
				<div class="items flexbox">

					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-01.png" title="Заявка на обратный звонок" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="#" data-event="jqm" data-param-id="21" data-name="callback">Заявка на обратный звонок</a></div>										
							</div>
							<a href="#" data-event="jqm" data-param-id="21" data-name="callback"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-02.png" title="Задать вопрос" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="#" data-event="jqm" data-param-id="20" data-name="question">Задать вопрос</a></div>										
							</div>
							<a href="#" data-event="jqm" data-param-id="20" data-name="question"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-03.png" title="Оставить отзыв" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="#" data-event="jqm" data-param-id="30" data-name="add_review">Оставить отзыв</a></div>										
							</div>
							<a href="#" data-event="jqm" data-param-id="30" data-name="add_review"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-04.png" title="Заявка на установку видеонаблюдения" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="#" data-event="jqm" data-param-webform-id="20" data-param-type="webform" data-name="webform">Заявка на установку видеонаблюдения</a></div>										
							</div>
							<a href="#" data-event="jqm" data-param-webform-id="20" data-param-type="webform" data-name="webform"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-05.png" title="Заявка на ремонт" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="#" data-event="jqm" data-param-webform-id="19" data-param-type="webform" data-name="webform">Заявка на ремонт</a></div>										
							</div>
							<a href="#" data-event="jqm" data-param-webform-id="19" data-param-type="webform" data-name="webform"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-25.png" title="Заявка на расторжение договора" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="#" data-event="jqm" data-param-webform-id="18" data-param-type="webform" data-name="webform">Заявка на расторжение договора</a></div>										
							</div>
							<a href="#" data-event="jqm" data-param-webform-id="18" data-param-type="webform" data-name="webform"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-03.png" title="Доска объявлений" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="#" data-event="jqm" data-param-webform-id="21" data-param-type="webform" data-name="webform">Доска объявлений</a></div>										
							</div>
							<a href="#" data-event="jqm" data-param-webform-id="21" data-param-type="webform" data-name="webform"></a>
						</div>
					</div>
					<div class="item shadow border">
						<div class="wrap">									
							<div class="image">
								<div class="wrap"><img src="<?=SITE_TEMPLATE_PATH?>/images/custom/lk-pic-red-06.png" title="Лист ожидания" class="img-responsive" /></div>
							</div>									
							
							<div class="body-info">										
								<div class="title"><a class="dark-color" href="/cabinet/forms/waiting_list/">Лист ожидания</a></div>										
							</div>
							<a href="/cabinet/forms/waiting_list/"></a>
						</div>
					</div>

				</div>        
			</div>							
		</div>
	</div>
</div>