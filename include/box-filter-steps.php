<div class="select_steps horizontal">
	<div class="row">
		<div class="col-md-4 col-xs-12">
			<div class="step">
				<div class="head">
					<div class="num">1</div>
					<div class="name">Выбор склада</div>
				</div>
				<div class="text">Каждый склад имеет удобное расположение, проезд и пункт разгрузки</div>
			</div>
		</div>
		<div class="col-md-4 col-xs-12">
			<div class="step active">
				<div class="head">
					<div class="num">2</div>
					<div class="name">Выбор бокса</div>
				</div>
				<div class="text">На выбор боксы от 1 до 50 куб. м, сроком от 1 дня до 11 месяцев</div>
			</div>	
		</div>
		<div class="col-md-4 col-xs-12">
			<div class="step">
				<div class="head">
					<div class="num">3</div>
					<div class="name">Аренда</div>
				</div>
				<div class="text">Для завершения аренды с Вами свяжется наш менеджер, спасибо!</div>
			</div>
		</div>
	</div>					
</div>