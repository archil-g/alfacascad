<div class="footer-partner-logo hidden-xs">
	<a href="/upload/docs/aflaskad_certificate.pdf" target="_blank">
		<img style="width: 200px" class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/images/custom/fedessa-logo.png" alt="Fedessa" />
	</a>
</div>
<div class="footer-partner-logo visible-xs">
	<a href="/about/docs/">
		<img style="width: 200px" class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/images/custom/fedessa-logo.png" alt="Fedessa" />
	</a>
</div>