<p>Если стоимость бокса указана без скидки, данный бокс можно забронировать со скидкой при единоразовой оплате:</p>
<ul class="list">
	<li>3-х месяцев - 5%</li>
</ul>
<p>Если вы не нашли Бокс нужного размера, позвоните менеджеру по телефону:
<a href="tel:+74955802047">+7 (495) 580-20-47</a></p>

<div class="info-blocks">
	<div class="row">
		<div class="col-md-6">
			<div class="item">
				<div class="pic">
					<img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/images/custom/icon-map-block-01.png" alt="Доступ 24/7" >
				</div>
				<div class="name">Доступ 24/7</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="item">
				<div class="pic">
					<img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/images/custom/icon-map-block-02.png" alt="Защищенная аренда" >
				</div>
				<div class="name">Защищенная аренда</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="item">
				<div class="pic">
					<img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/images/custom/icon-map-block-03.png" alt="Видеонаблюдение" >
				</div>
				<div class="name">Видеонаблюдение</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="item">
				<div class="pic">
					<img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/images/custom/icon-map-block-04.png" alt="Сигнализация" >
				</div>
				<div class="name">Сигнализация</div>
			</div>
		</div>
	</div>
</div>