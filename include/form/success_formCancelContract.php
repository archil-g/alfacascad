<p>Ваше уведомление принято!</p>
<p>Для улучшения качества обслуживания просим вас указать причину расторжения договора:</p>
<p><a class="btn btn-default btn-lg" href="/cabinet/forms/poll_cancel_contract.php">Ответить</a></p>
<br>
<span class="btn btn-transparent btn-xs animate-load" title="Оставить отзыв" data-event="jqm" data-param-id="30" data-name="add_review">Оставить отзыв</span> 
<span class="btn btn-transparent btn-xs animate-load" title="Заказать доставку" data-event="jqm" data-param-webform-id="23" data-param-type="webform" data-name="webform">Заказать доставку</span>