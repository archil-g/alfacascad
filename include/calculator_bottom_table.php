<br><br>
<div class="row">
	<div class="col-md-8 col-xs-12">
		<div class="calculator_bottom_table">
			<h3>Соотношение размера перевозимых вещей с&nbsp;боксом</h3>
			<div class="table-responsive">
				<table class="table">
				<tbody>
				<tr>
					<th>
						 Офис/квартира
					</th>
					<th>
						 Кол-во
					</th>
					<th>
						 Вид транспорта
					</th>
					<th>
						 Площадь отсека
					</th>
				</tr>
				<tr>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-01.png">
						</div>
						 до 10 кв. м.
					</td>
					<td>
						 1
					</td>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-02.png">
						</div>
						 Легковое авто 3 куб. м.
					</td>
					<td>
						 1 - 3 кв. м.
					</td>
				</tr>
				<tr>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-01.png">
						</div>
						 от 10 до 15 кв. м.
					</td>
					<td>
						 2
					</td>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-02.png">
						</div>
						 Партер 5 куб. м.
					</td>
					<td>
						 1 - 3 кв. м.
					</td>
				</tr>
				<tr>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-01.png">
						</div>
						 от 15 до 30 кв. м.
					</td>
					<td>
						 3
					</td>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-02.png">
						</div>
						 Газель 10 куб. м.
					</td>
					<td>
						 1 - 3 кв. м.
					</td>
				</tr>
				<tr>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-01.png">
						</div>
						 от 30 до 60 кв. м.
					</td>
					<td>
						 4
					</td>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-02.png">
						</div>
						 Удлиненная газель 20 куб. м.
					</td>
					<td>
						 1 - 3 кв. м.
					</td>
				</tr>
				<tr>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-01.png">
						</div>
						 от 60 кв. м.
					</td>
					<td>
						 -
					</td>
					<td>
						<div class="icon">
 <img src="/bitrix/templates/aspro-priority/images/custom/icon-calc-table-02.png">
						</div>
						 Фура более 50 куб. м.
					</td>
					<td>
						 1 - 3 кв. м.
					</td>
				</tr>
				</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-xs-12">
		<div class="thumbnail">
 <img src="/bitrix/templates/aspro-priority/images/custom/calc-test-banner.png">
		</div>
	</div>
</div>
 <br>