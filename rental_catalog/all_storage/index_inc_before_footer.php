<div id="formManagerOrder_block"></div>
<hr class="tall" style="margin-top:0;">
<div class="maxwidth-theme">
	<?$APPLICATION->IncludeComponent(
	"bitrix:form", 
	"formManagerOrder", 
	array(
		"COMPONENT_TEMPLATE" => "formManagerOrder",
		"START_PAGE" => "new",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_VIEW_PAGE" => "N",
		"SUCCESS_URL" => "",
		"WEB_FORM_ID" => "11",
		"RESULT_ID" => $_REQUEST["RESULT_ID"],
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_STATUS" => "N",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "N",
		"NOT_SHOW_FILTER" => array(
			0 => "",
			1 => "",
		),
		"NOT_SHOW_TABLE" => array(
			0 => "",
			1 => "",
		),
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"PHONE" => "+7 (495) 580-20-47",
		"EMAIL" => "info@alfasklad.ru",
		"SHOW_LICENCE" => "Y",
		"VARIABLE_ALIASES" => array(
			"action" => "action",
		)
	),
	false
);?>
	<br>
</div>