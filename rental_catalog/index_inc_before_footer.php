<div class="greyline">
	<div class="maxwidth-theme">
		<div class="padding-top-bottom-50">
			<div class="row">
				<div class="col-md-12">

					<?$APPLICATION->IncludeComponent(
						"bitrix:news.list", 
						"news_list_4_v4", 
						array(
							"COMPONENT_TEMPLATE" => "news_list_4_v4",
							"IBLOCK_TYPE" => "aspro_priority_content",
							"IBLOCK_ID" => "44",
							"NEWS_COUNT" => "4",
							"SORT_BY1" => "ACTIVE_FROM",
							"SORT_ORDER1" => "DESC",
							"SORT_BY2" => "ID",
							"SORT_ORDER2" => "DESC",
							"FILTER_NAME" => "",
							"FIELD_CODE" => array(
								0 => "NAME",
								1 => "PREVIEW_PICTURE",
								2 => "DATE_ACTIVE_FROM",
								3 => "",
							),
							"PROPERTY_CODE" => array(
								0 => "",
								1 => "",
							),
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "60",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "N",
							"PREVIEW_TRUNCATE_LEN" => "120",
							"ACTIVE_DATE_FORMAT" => "j F Y",
							"SET_TITLE" => "N",
							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_LAST_MODIFIED" => "N",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "N",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"PARENT_SECTION" => "",
							"PARENT_SECTION_CODE" => "b-mainpage-news",
							"INCLUDE_SUBSECTIONS" => "N",
							"STRICT_SECTION_CHECK" => "N",
							"DISPLAY_DATE" => "N",
							"DISPLAY_NAME" => "N",
							"DISPLAY_PICTURE" => "N",
							"DISPLAY_PREVIEW_TEXT" => "N",
							"PAGER_TEMPLATE" => ".default",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "Новости",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"SET_STATUS_404" => "N",
							"SHOW_404" => "N",
							"MESSAGE_404" => "",
							"IMAGE_POSITION" => "left",
							"SHOW_DETAIL_LINK" => "Y",
							"USE_SHARE" => "N",
							"S_ASK_QUESTION" => "",
							"S_ORDER_SERVICE" => "",
							"T_GALLERY" => "",
							"T_DOCS" => "",
							"T_GOODS" => "",
							"T_SERVICES" => "",
							"T_PROJECTS" => "",
							"T_REVIEWS" => "",
							"T_STAFF" => "",
							"SHOW_SECTIONS" => "Y",
							"SHOW_GOODS" => "Y",
							"TITLE" => "Действующие акции",
							"MAX_COUNT_ELEMENTS_ON_PAGE" => "9",
							"SHOW_ALL_TITLE" => "Посмотреть все акции",
							"S_ORDER_PRODUCT" => ""
						),
						false,
						array(
							"HIDE_ICONS" => "N"
						)
					);?>

				</div>
			</div>

		</div>
	</div>
</div>


<div class="maxwidth-theme">
	<div class="padding-top-bottom-50">
		<h2 class="style-h3">Наши дополнительные возможности</h2>
		<p>
			Вы можете заказать дополнительные услуги по доставке и упаковке вещей, а также по аренде стеллажей. 
		</p>
	
		<?$APPLICATION->IncludeComponent(
			"bitrix:news.list", 
			"news_list_4_v2", 
			array(
				"COMPONENT_TEMPLATE" => "news_list_4_v2",
				"IBLOCK_TYPE" => "aspro_priority_content",
				"IBLOCK_ID" => "32",
				"NEWS_COUNT" => "3",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "ID",
				"SORT_ORDER2" => "DESC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array(
					0 => "NAME",
					1 => "PREVIEW_TEXT",
					2 => "PREVIEW_PICTURE",
					3 => "",
				),
				"PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "60",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"PREVIEW_TRUNCATE_LEN" => "120",
				"ACTIVE_DATE_FORMAT" => "j F Y",
				"SET_TITLE" => "N",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"SET_LAST_MODIFIED" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "b-mainpage-news",
				"INCLUDE_SUBSECTIONS" => "N",
				"STRICT_SECTION_CHECK" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "N",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "N",
				"PAGER_TEMPLATE" => ".default",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Новости",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"SET_STATUS_404" => "N",
				"SHOW_404" => "N",
				"MESSAGE_404" => "",
				"IMAGE_POSITION" => "left",
				"SHOW_DETAIL_LINK" => "Y",
				"USE_SHARE" => "N",
				"S_ASK_QUESTION" => "",
				"S_ORDER_SERVICE" => "",
				"T_GALLERY" => "",
				"T_DOCS" => "",
				"T_GOODS" => "",
				"T_SERVICES" => "",
				"T_PROJECTS" => "",
				"T_REVIEWS" => "",
				"T_STAFF" => "",
				"SHOW_SECTIONS" => "Y",
				"SHOW_GOODS" => "Y",
				"TITLE" => "",
				"MAX_COUNT_ELEMENTS_ON_PAGE" => "9",
				"SHOW_ALL_TITLE" => "",
				"S_ORDER_PRODUCT" => ""
			),
			false,
			array(
				"HIDE_ICONS" => "N"
			)
		);?>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => "/include/tabs_formManagerOrder.php"
    ),
    false,
    Array("HIDE_ICONS" => "Y")
);?>
