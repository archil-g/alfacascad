<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Подобрать Бокс");?>

<?/*<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
	Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
	Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>*/?>


<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/include/rental_catalog-nav-block.php"
	),
	false,
	Array("HIDE_ICONS"=>"Y")
);?>


<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "select_storage_map", Array(
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"FILTER_NAME" => "",	// Имя массива со значениями фильтра разделов
		"IBLOCK_ID" => "40",	// Инфоблок
		"IBLOCK_TYPE" => "aspro_priority_catalog",	// Тип инфоблока
		"SECTION_CODE" => $_REQUEST["SECTION_CODE"],	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "NAME",
			1 => "DESCRIPTION",
			2 => "",
		),
		"SECTION_ID" => "",	// ID раздела
		"SECTION_URL" => "#CODE#/",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "UF_PHOTOGALLERY",
			1 => "UF_ADDRESS",
			2 => "UF_RECEPTION",
			3 => "UF_DOSTUP_TIME",
			4 => "UF_PHONE",
			5 => "UF_MAP",
			6 => "UF_PRICE_ON_MAP",
			7 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "find_storage_list"
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"storage_select_withoutmap",
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "40",
		"IBLOCK_TYPE" => "aspro_priority_catalog",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "NAME",
			1 => "DESCRIPTION",
			2 => "",
		),
		"SECTION_ID" => "",
		"SECTION_URL" => "#CODE#/",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_ADDRESS",
			1 => "UF_RECEPTION",
			2 => "UF_DOSTUP_TIME",
			3 => "UF_PHONE",
			4 => "UF_MAP",
			5 => "UF_PHOTOGALLERY",
			6 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "storage_select_withoutmap"
	),
	false
);?>


<?$APPLICATION->IncludeComponent(
	"bitrix:form",
	"formManagerOrder_4",
	array(
		"COMPONENT_TEMPLATE" => "formManagerOrder_4",
		"START_PAGE" => "new",
		"SHOW_LIST_PAGE" => "N",
		"SHOW_EDIT_PAGE" => "N",
		"SHOW_VIEW_PAGE" => "N",
		"SUCCESS_URL" => "",
		"WEB_FORM_ID" => "14",
		"RESULT_ID" => $_REQUEST["RESULT_ID"],
		"SHOW_ANSWER_VALUE" => "N",
		"SHOW_ADDITIONAL" => "N",
		"SHOW_STATUS" => "N",
		"EDIT_ADDITIONAL" => "N",
		"EDIT_STATUS" => "N",
		"NOT_SHOW_FILTER" => array(
			0 => "",
			1 => "",
		),
		"NOT_SHOW_TABLE" => array(
			0 => "",
			1 => "",
		),
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"USE_EXTENDED_ERRORS" => "N",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "form_14",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CHAIN_ITEM_TEXT" => "",
		"CHAIN_ITEM_LINK" => "",
		"SHOW_LICENCE" => "Y",
		"VARIABLE_ALIASES" => array(
			"action" => "action",
		)
	),
	false,
	Array("HIDE_ICONS"=>"Y")
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
