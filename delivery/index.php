<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "При аренде бокса в АльфаСклад мы предлагаем недорого перевезти вещи из дома в бокс.");
$APPLICATION->SetPageProperty("keywords", "доставка, переезд, автомобиль для перевозки вещей, перевозка вещей");
$APPLICATION->SetPageProperty("title", "Доставка и перевозка вещей");
$APPLICATION->SetTitle("Доставка");?><style>
.b-layout .bottom-red-border {
    padding-bottom: .625rem;
    border-bottom: 1px solid #e9565a;
}
.b-price-text {
    color: #000;
    font-size: 19px;
    font-weight: 400;
    line-height: 30px;
    letter-spacing: .57px;
}
.b-price-stuff-item {
    color: #e9565a;
    font-size: 19px;
    font-weight: 500;
    line-height: 30px;
    letter-spacing: .57px;
    margin-right: 50px;
}
.b-layout .cell {
    -webkit-box-flex: 0;
    -ms-flex: 0 0 auto;
    flex: 0 0 auto;
    min-height: 0;
    min-width: 0;
    width: 100%;
    margin: 20px 0;
}
.bottom--fat-red-border {
    border-bottom: 5px solid #e9565a;
}
.b-price-table-title {
    /*height: 100px;*/
    color: #000;
    font-size: 18px;
    font-weight: 300;
    line-height: 30px;
    letter-spacing: .72px;
    padding-bottom: 20px;
}
table.b-price {
    border-collapse: collapse;
    width: 100%;
}
table.b-price tr {
    border-bottom: 1px solid #fff;
}
table.b-price tr th {
    font-size: 16px;
    font-weight: 600;
    line-height: 24px;
    letter-spacing: .57px;
    padding-bottom: 10px;
}
table.b-price tr th:first-of-type {
    text-align: left;
}
table.b-price tr th.action {
    color: #e9565a;
}
table.b-price tr td {
    padding-top: 28px;
    padding-bottom: 28px;
    padding-left: 17px;
    background: #f2f2f2;
    color: #000;
    font-size: 19px;
    font-weight: 300;
    line-height: 30px;
    letter-spacing: .57px;
}
table.b-price tr td:first-of-type {
    background: #ececec;
}
table.b-price tr td:last-of-type {
    background: #fad6d7;
	font-weight: 500;
}
.b-price-disc span {
    color: #e9565a;
}
.b-price-phone {
    color: #000;
    font-size: 20px;
    font-weight: 300;
    line-height: 30px;
    letter-spacing: .6px;
    margin-top: 45px;
}
.b-price-phone--center {
    text-align: center;
}
.b-layout .c-table__caption {
    color: #e9565a;
    font-size: 24px;
    font-weight: 400;
    line-height: 30px;
    letter-spacing: .57px;
    padding-bottom: 20px;
    text-align: left;
}
.b-price-table-title--auto {
    height: auto;
	padding-bottom: 0;
}
.b-list__item {
    padding: 0 0 0 1.25rem;
    list-style-type: none;
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAICAMAAAAY5vW6AAAAP1BMV…/v/me1AZAAyMtKAbVt7H1gHidEH1z3BNHjaRUQ75cBlAR+MsQBQMkxoMsAAAAASUVORK5CYII=);
    background-position: .0625rem .5rem;
    color: #e9565a;
    line-height: 1.375rem;
    font-size: .875rem;
    font-weight: 300;
}
.b-list__item--black {
    color: #0a0a0a;
}
.b-list__item {
    padding: 0 0 0 1.75rem;
    background-position: .0625rem .8125rem;
    line-height: 2.1875rem;
    font-size: 16px;
}
.c-delivery__list {
	margin-bottom: 30px;
}
.b-layout .c-red {
    color: #e9565a;
}
.b-layout .c-table--short td {
    padding-top: 5px;
    padding-bottom: 5px;
}
.b-layout table.b-price tr td.neutral {
    background: #fefefe!important;
    padding-top: 5px;
    padding-bottom: 5px;
	font-weight: 500;
}
table.small-font-size tr td {
	font-size: 18px;
}
.b-layout .c-anchor {
    color: #e9565a;
    text-decoration: none;
    display: inline-block;
    margin: 0 3px;
}
.b-layout ul li::before {
    content: "\2714";
    color: red;
}
</style>
<div class="b-layout">
	<div class="cell large-12 c-margin-top-42">
		<div class="grid-x grid-margin-x b-price row">
			<div class="col-md-6 col-xs-12 c-padding-top-70 c-hide-on-mobile">
				<h2>Почасовой тариф на доставку</h2>
				<table class="b-price c-table--short small-font-size">
				<tbody>
				<tr>
					<th>
						 Тариф
					</th>
					<th>
						 Стоимость доставки, руб.
					</th>
				</tr>
				<tr>
					<td colspan="2" class="neutral" align="center">
						 2 часа
					</td>
				</tr>
				<tr>
					<td>
						 Автомобиль + 1 грузчик&nbsp;
					</td>
					<td>
						3 400 + 1 300 за доп. час
					</td>
				</tr>
				<tr>
					<td colspan="2" class="neutral" align="center">
						 3 часа
					</td>
				</tr>
				<tr>
					<td>
						 Автомобиль + 1 грузчик
					</td>
					<td>
						4 500 + 1 300 за доп. час
					</td>
				</tr>
				<tr>
					<td>
						 Автомобиль + 2 грузчика
					</td>
					<td>
						5 900 + 2 000 за доп. час
					</td>
				</tr>
				<tr>
					<td colspan="2" class="neutral" align="center">
						 4 часа
					</td>
				</tr>
				<tr>
					<td>
						 Автомобиль + 2 грузчика
					</td>
					<td>
						7 900 + 2 000 за доп. час
					</td>
				</tr>
				</tbody>
				</table>
 <br>

			</div>
			<div class="col-md-6 col-xs-12">
				<div class="c-delivery__list">
					<p class="b-price-table-title b-price-table-title--auto">
 <b>Что входит в стоимость:</b>
					</p>
					<ul>
						<li class="b-list__item b-list__item--black">Автомобиль для перевозки</li>
						<li class="b-list__item b-list__item--black">Разборка мебели стандартной конструкции</li>
						<li class="b-list__item b-list__item--black">Упаковка (материал за дополнительную плату)</li>
						<li class="b-list__item b-list__item--black">Погрузка вещей в автомобиль</li>
						<li class="b-list__item b-list__item--black">Доставка до склада</li>
						<li class="b-list__item b-list__item--black">Загрузка вещей в бокс<br>
 <br>
 <br>
 </li>
					</ul>
				</div>
				<div class="c-delivery__list">
					<p class="b-price-table-title b-price-table-title--auto">
 <b>Дополнительные услуги:</b>
					</p>
					<ul>
						<li class="b-list__item b-list__item--black">Продажа упаковочных материалов</li>
					</ul>
				</div>
			</div>
		</div>
	</div>


	<!--
	<div style="text-align: center;">
	 <a class="btn btn-default btn-lg" href="/promotions/besplatnaya-dostavka/">Бесплатная доставка</a>
	</div>
	-->

	<div style="text-align: center;">
		<span class="btn btn-default animate-load" title="Заказать доставку" data-event="jqm" data-param-webform-id="23" data-param-type="webform" data-name="webform">Заказать доставку</span>
	</div>

	<br>



	<div class="cell large-12 c-margin-top-42">
		<div class="grid-x grid-margin-x">
			<div class="cell large-12 medium-12">
				<h2>Исключенные категории грузов</h2>
			</div>
			<div class="row">
				<div class="col-md-6 col-xs-12">
					<p class="c-page-description">
					</p>
					<ul>
						<li class="b-list__item b-list__item--black">военные грузы и грузы двойного назначения;</li>
						<li class="b-list__item b-list__item--black">огнестрельное оружие;</li>
						<li class="b-list__item b-list__item--black">опасные грузы (вещества и материалы с физико-химическими свойствами высокой степени опасности по ГОСТ 19433-88);</li>
						<li class="b-list__item b-list__item--black">предметы (вещи, вещества, товары, материалы и т.п.), перевозка которых автомобильным и городским транспортом запрещена действующим законодательством РФ;</li>
						<li class="b-list__item b-list__item--black">биоматериалы;</li>
						<li class="b-list__item b-list__item--black">денежные знаки, монеты, дорожные чеки, ценные бумаги, кредитные карты, акцизные марки;</li>
						<li class="b-list__item b-list__item--black">драгоценные металлы и драгоценные камни, а также изделия из них, ювелирные изделия;</li>
						<li class="b-list__item b-list__item--black">документы, паспорта, сертификаты, бланки строгой отчетности и т.п.;</li>
						<li class="b-list__item b-list__item--black">филателистические материалы;</li>
					</ul>
					<p>
					</p>
				</div>
				<div class="col-md-6 col-xs-12">
					<p class="c-page-description">
					</p>
					<ul>
						<li class="b-list__item b-list__item--black">предметы, которые могут представлять опасность для сохранности других грузов (их упаковки), транспорта и оборудования, либо для жизни и здоровья окружающих, в частности: пачкающих или пылящих грузов, грузов, выделяющих влагу, выделяющих ядовитые газы или запахи и т.д., если упаковка (тара) таких грузов не предотвращает вышеуказанные процессы, происходящие с грузом;</li>
						<li class="b-list__item b-list__item--black">корреспонденция и сопроводительные документы, следующие с грузом;</li>
						<li class="b-list__item b-list__item--black">нумизматические материалы, ценные бумаги, банковские карты;</li>
						<li class="b-list__item b-list__item--black">произведения искусства<a class="c-anchor" href="#note">*</a>, культурные ценности</li>
						<li class="b-list__item b-list__item--black">животные;</li>
						<li class="b-list__item b-list__item--black">пищевые продукты, алкоголь, табак;</li>
						<li class="b-list__item b-list__item--black">медикаменты.</li>
					</ul>
					<p>
					</p>
				</div>
			</div>
			<div class="cell large-12 medium-12">
				<div class="c-page-description">
					<p id="note">
 <span class="c-anchor">*</span>Под произведением искусства, культурной ценностью в рамках настоящего Договора понимается продукт художественной деятельности, обладающий хотя бы одним из нижеперечисленных признаков:
					</p>
					<ul>
						<li class="b-list__item b-list__item--black">является объектом авторских прав, имеющим культурную и художественную ценность;</li>
						<li class="b-list__item b-list__item--black">является антиквариатом;</li>
						<li class="b-list__item b-list__item--black">предметы коллекции, постоянно хранящиеся в государственных и муниципальных музеях, архивах, библиотеках, других государственных хранилищах культурных ценностей РФ и/или учет которых ведется государственными органами, в том числе предметы и коллекции, включенные в состав Музейного фонда РФ;</li>
						<li class="b-list__item b-list__item--black">предмет, выпущенный в единичном экземпляре, независимо от наличия исторической, художественной, научной и литературной ценности и не являющийся предметом серийного, массового производства или сувенирной продукцией;</li>
						<li class="b-list__item b-list__item--black">редкие предметы, представляющие особый интерес (исторический, художественный, научный и литературный), отдельно или в коллекциях.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>