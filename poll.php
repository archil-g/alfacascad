<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оцените качество обслуживания");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:voting.form", 
	"custom", 
	array(
		"COMPONENT_TEMPLATE" => "custom",
		"VOTE_ID" => "1",
		"VOTE_RESULT_TEMPLATE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>