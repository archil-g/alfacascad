<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Нужна аренда склада от собственника в Москве? АльфаСклад предлагает снять склады от собственника на выгодных условиях ✔ Без риэлторов и переплат ✔ Онлайн подбор боксов по районам Москвы ✔ Онлайн консультации и заказ склада 8 (495) 580-20-47");
$APPLICATION->SetPageProperty("keywords", "склад от собственника, аренда склада от собственника, склады от собственника, аренда склада в Москве от собственника");
$APPLICATION->SetPageProperty("title", "Склад от собственника. Аренда склада в Москве от собственника | АльфаСклад");
$APPLICATION->SetTitle("Склад от собственника АльфаСклад");?><div class="row">
	<div class="col-md-8 col-xs-12">
		<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",	// Суффикс имени файла включаемой области
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "/include/sklad-ot-sobstvennika-text.php"
	)
);?>
	</div>
	<div class="col-md-4 col-xs-12">
		<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"rating_sklad_small",
	array(
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
		"FILTER_NAME" => "",	// Имя массива со значениями фильтра разделов
		"IBLOCK_ID" => "40",	// Инфоблок
		"IBLOCK_TYPE" => "aspro_priority_catalog",	// Тип инфоблока
		"SECTION_CODE" => "",	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "NAME",
			1 => "",
			2 => "",
		),
		"SECTION_ID" => "",	// ID раздела
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "UF_YMAP_REVIEWS",
			1 => "UF_YMAP_VOTES",
			2 => "UF_YMAP_RATING",
			3 => "",
			4 => "",
			5 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "rating_sklad_small"
	),
	false,
	array("HIDE_ICONS" => "Y")
);?>
	</div>
</div>



<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	array(
	"AREA_FILE_SHOW" => "file",	// Показывать включаемую область
		"AREA_FILE_SUFFIX" => "inc",	// Суффикс имени файла включаемой области
		"EDIT_TEMPLATE" => "",	// Шаблон области по умолчанию
		"PATH" => "/include/sklad-ot-sobstvennika-our-team.php"
	),
	false,
	array("HIDE_ICONS" => "Y")
);?>

<?
global $arrArticles;
$arrArticles["!PROPERTY_SKLAD_OT_SOBST"] = false;
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"news_list_4_v3",
	array(
	"COMPONENT_TEMPLATE" => "news_list_4_v3",
		"IBLOCK_TYPE" => "aspro_priority_content",	// Тип информационного блока (используется только для проверки)
		"IBLOCK_ID" => "33",	// Код информационного блока
		"NEWS_COUNT" => "3",	// Количество новостей на странице
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
		"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
		"FILTER_NAME" => "arrArticles",	// Фильтр
		"FIELD_CODE" => array(	// Поля
			0 => "NAME",
			1 => "PREVIEW_PICTURE",
			2 => "DATE_ACTIVE_FROM",
			3 => "",
		),
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "60",	// Время кеширования (сек.)
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"ACTIVE_DATE_FORMAT" => "j F Y",	// Формат показа даты
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "b-mainpage-news",	// Код раздела
		"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SHOW_404" => "N",	// Показ специальной страницы
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"SHOW_DETAIL_LINK" => "Y",
		"TITLE" => "",
		"MAX_COUNT_ELEMENTS_ON_PAGE" => "9",
		"SHOW_ALL_TITLE" => ""
	),
	false,
	array("HIDE_ICONS" => "Y")
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>