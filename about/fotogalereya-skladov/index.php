<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Фотогалерея складов");?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"fotogalereya_skladov", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "40",
		"IBLOCK_TYPE" => "aspro_priority_catalog",
		"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
		"SECTION_FIELDS" => array(
			0 => "NAME",
			1 => "PICTURE",
			2 => "",
		),
		"SECTION_ID" => "",
		"SECTION_URL" => "#CODE#/",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_PHOTOGALLERY",
			1 => "UF_ADDRESS",
			2 => "UF_DOSTUP_TIME",
			3 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "fotogalereya_skladov"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>