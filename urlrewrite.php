<?php
$arUrlRewrite=array (
  44 => 
  array (
    'CONDITION' => '#^/rental_catalog/([a-zA-Z0-9\\-_]+)/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'SKLAD_CODE=$1&FLOOR_CODE=$2',
    'ID' => '',
    'PATH' => '/rental_catalog/detail.php',
    'SORT' => 100,
  ),
  52 => 
  array (
    'CONDITION' => '#^/cabinet/orders/myboxes/current/inventory-([a-zA-Z0-9\\-_]+)/#',
    'RULE' => 'BOX_ID=$1',
    'ID' => '',
    'PATH' => '/cabinet/orders/myboxes/current/inventory.php',
    'SORT' => 100,
  ),
  25 => 
  array (
    'CONDITION' => '#^/about/fotogalereya-skladov/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'SECTION_CODE=$1',
    'ID' => '',
    'PATH' => '/about/fotogalereya-skladov/detail.php',
    'SORT' => 100,
  ),
  53 => 
  array (
    'CONDITION' => '#^/cabinet/docs/invoices/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'INVOICE_ID=$1',
    'ID' => '',
    'PATH' => '/cabinet/docs/invoices/detail.php',
    'SORT' => 100,
  ),
  24 => 
  array (
    'CONDITION' => '#^/features_for_personal/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'ELEMENT_CODE=$1',
    'ID' => '',
    'PATH' => '/features_for_personal/detail.php',
    'SORT' => 100,
  ),
  23 => 
  array (
    'CONDITION' => '#^/features_for_business/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'ELEMENT_CODE=$1',
    'ID' => '',
    'PATH' => '/features_for_business/detail.php',
    'SORT' => 100,
  ),
  43 => 
  array (
    'CONDITION' => '#^/rental_catalog/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'SKLAD_CODE=$1',
    'ID' => '',
    'PATH' => '/rental_catalog/detail.php',
    'SORT' => 100,
  ),
  32 => 
  array (
    'CONDITION' => '#^/about/reviews/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'SKLAD_CODE=$1',
    'ID' => '',
    'PATH' => '/about/reviews/index.php',
    'SORT' => 100,
  ),
  26 => 
  array (
    'CONDITION' => '#^/find_storage/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'SECTION_CODE=$1',
    'ID' => '',
    'PATH' => '/find_storage/detail.php',
    'SORT' => 100,
  ),
  28 => 
  array (
    'CONDITION' => '#^/for_business/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'SECTION_CODE=$1',
    'ID' => '',
    'PATH' => '/for_business/detail.php',
    'SORT' => 100,
  ),
  29 => 
  array (
    'CONDITION' => '#^/storage/([a-zA-Z0-9\\-_]+)/(/?)([^/]*)#',
    'RULE' => 'SECTION_CODE=$1',
    'ID' => '',
    'PATH' => '/storage/detail.php',
    'SORT' => 100,
  ),
  1 => 
  array (
    'CONDITION' => '#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#',
    'RULE' => 'alias=$1',
    'ID' => NULL,
    'PATH' => '/desktop_app/router.php',
    'SORT' => 100,
  ),
  47 => 
  array (
    'CONDITION' => '#^/cabinet/info/notifications/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/cabinet/info/notifications/index.php',
    'SORT' => 100,
  ),
  3 => 
  array (
    'CONDITION' => '#^\\/?\\/mobileapp/jn\\/(.*)\\/.*#',
    'RULE' => 'componentName=$1',
    'ID' => NULL,
    'PATH' => '/bitrix/services/mobileapp/jn.php',
    'SORT' => 100,
  ),
  48 => 
  array (
    'CONDITION' => '#^/cabinet/info/promotions/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/cabinet/info/promotions/index.php',
    'SORT' => 100,
  ),
  41 => 
  array (
    'CONDITION' => '#^/about/useful_for_rental/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/about/useful_for_rental/index.php',
    'SORT' => 100,
  ),
  5 => 
  array (
    'CONDITION' => '#^/bitrix/services/ymarket/#',
    'RULE' => '',
    'ID' => '',
    'PATH' => '/bitrix/services/ymarket/index.php',
    'SORT' => 100,
  ),
  45 => 
  array (
    'CONDITION' => '#^/cabinet/orders/archive/#',
    'RULE' => 'filter_history=Y',
    'ID' => '',
    'PATH' => '/cabinet/orders/new/index.php',
    'SORT' => 100,
  ),
  21 => 
  array (
    'CONDITION' => '#^/about/voprosi_i_otveti/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/about/voprosi_i_otveti/index.php',
    'SORT' => 100,
  ),
  19 => 
  array (
    'CONDITION' => '#^/company/manufacturers/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/company/manufacturers/index.php',
    'SORT' => 100,
  ),
  49 => 
  array (
    'CONDITION' => '#^/cabinet/info/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/cabinet/info/news/index.php',
    'SORT' => 100,
  ),
  2 => 
  array (
    'CONDITION' => '#^/online/(/?)([^/]*)#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/desktop_app/router.php',
    'SORT' => 100,
  ),
  20 => 
  array (
    'CONDITION' => '#^/company/licenses/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/company/licenses/index.php',
    'SORT' => 100,
  ),
  0 => 
  array (
    'CONDITION' => '#^/stssync/calendar/#',
    'RULE' => '',
    'ID' => 'bitrix:stssync.server',
    'PATH' => '/bitrix/services/stssync/calendar/index.php',
    'SORT' => 100,
  ),
  6 => 
  array (
    'CONDITION' => '#^/company/partners/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/company/partners/index.php',
    'SORT' => 100,
  ),
  8 => 
  array (
    'CONDITION' => '#^/company/vacancy/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/company/vacancy/index.php',
    'SORT' => 100,
  ),
  7 => 
  array (
    'CONDITION' => '#^/company/reviews/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/company/reviews/index.php',
    'SORT' => 100,
  ),
  9 => 
  array (
    'CONDITION' => '#^/company/staff/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/company/staff/index.php',
    'SORT' => 100,
  ),
  10 => 
  array (
    'CONDITION' => '#^/company/docs/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/company/docs/index.php',
    'SORT' => 100,
  ),
  16 => 
  array (
    'CONDITION' => '#^/cabinet/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/cabinet/news/index.php',
    'SORT' => 100,
  ),
  51 => 
  array (
    'CONDITION' => '#^/cabinet/info/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/cabinet/info/index.php',
    'SORT' => 100,
  ),
  42 => 
  array (
    'CONDITION' => '#^/promotions/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/promotions/index.php',
    'SORT' => 100,
  ),
  12 => 
  array (
    'CONDITION' => '#^/projects/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/projects/index.php',
    'SORT' => 100,
  ),
  18 => 
  array (
    'CONDITION' => '#^/landings/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/landings/index.php',
    'SORT' => 100,
  ),
  11 => 
  array (
    'CONDITION' => '#^/articles/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/articles/index.php',
    'SORT' => 100,
  ),
  40 => 
  array (
    'CONDITION' => '#^/services/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/services/index.php',
    'SORT' => 100,
  ),
  54 => 
  array (
    'CONDITION' => '#^/product/#',
    'RULE' => '',
    'ID' => 'bitrix:catalog',
    'PATH' => '/product/index.php',
    'SORT' => 100,
  ),
  4 => 
  array (
    'CONDITION' => '#^/rest/#',
    'RULE' => '',
    'ID' => NULL,
    'PATH' => '/bitrix/services/rest/index.php',
    'SORT' => 100,
  ),
  50 => 
  array (
    'CONDITION' => '#^/news/#',
    'RULE' => '',
    'ID' => 'bitrix:news',
    'PATH' => '/news/index.php',
    'SORT' => 100,
  ),
);
