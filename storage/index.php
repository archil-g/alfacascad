<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "АльфаСклад предлагает в аренду удобные теплые склады для хранения личных вещей. Мы обеспечиваем складское хранение в отапливаемых помещениях: теплые боксы и складские ячейки. Наши боксы теплые, чистые и надежные. Снять склад для хранения личных вещей очень просто в АльфаСклад.");
$APPLICATION->SetPageProperty("keywords", "складское хранение, складские ячейки, теплый склад, теплый склад в аренду москва, аренда теплого склада, аренда теплого бокса, теплый бокс в аренду, аренда отапливаемого склада, аренда теплого склада в москве, складские услуги, складские услуги в москве, услуги склада, услуги складского хранения, услуги хранения, склад для хранения личных вещей, аренда склада для хранения личных вещей, аренда склада для личных вещей, хранение личных вещей, хранение личных вещей в москве, склад для хранения личных вещей москва, снять склад для хранения личных вещей");
$APPLICATION->SetPageProperty("title", "Хранение личных вещей");
$APPLICATION->SetTitle("АльфаСклад для дома");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"topblock_for_business", 
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "41",
		"IBLOCK_TYPE" => "aspro_priority_content",
		"SECTION_CODE" => "storage",
		"SECTION_FIELDS" => array(
			0 => "NAME",
			1 => "DESCRIPTION",
			2 => "PICTURE",
			3 => "",
		),
		"SECTION_ID" => "",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_USE_TYPE_B",
			1 => "UF_USE_TYPE_P",
			2 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "1",
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "topblock_for_business"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"fixed_custom_menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "contentMenu",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "contentMenu",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "fixed_custom_menu"
	),
	false
);?>

<div class="bg-gray">

	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list", 
		"tisersblock_for_business", 
		array(
			"ADD_SECTIONS_CHAIN" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COUNT_ELEMENTS" => "N",
			"FILTER_NAME" => "",
			"IBLOCK_ID" => "41",
			"IBLOCK_TYPE" => "aspro_priority_content",
			"SECTION_CODE" => "storage",
			"SECTION_FIELDS" => array(
				0 => "NAME",
				1 => "DESCRIPTION",
				2 => "PICTURE",
				3 => "",
			),
			"SECTION_ID" => "",
			"SECTION_URL" => "",
			"SECTION_USER_FIELDS" => array(
				0 => "UF_TISERS_TITLE",
				1 => "UF_TISERS_TEXT",
				2 => "UF_TISERS_GALLERY",
				3 => "UF_TISERS_ITEMS",
			),
			"SHOW_PARENT_NAME" => "Y",
			"TOP_DEPTH" => "1",
			"VIEW_MODE" => "LINE",
			"COMPONENT_TEMPLATE" => "tisersblock_for_business"
		),
		false
	);?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "inc",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/include/tabs_formManagerOrder.php",
			"SHADOW_BOX" => "Y"
		),
		false,
		Array("HIDE_ICONS" => "Y")
	);?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list", 
		"useblock_for_business", 
		array(
			"ADD_SECTIONS_CHAIN" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COUNT_ELEMENTS" => "N",
			"FILTER_NAME" => "",
			"IBLOCK_ID" => "41",
			"IBLOCK_TYPE" => "aspro_priority_content",
			"SECTION_CODE" => "storage",
			"SECTION_FIELDS" => array(
				0 => "NAME",
				1 => "DESCRIPTION",
				2 => "PICTURE",
				3 => "",
			),
			"SECTION_ID" => "",
			"SECTION_URL" => "#CODE#/",
			"SECTION_USER_FIELDS" => array(
				0 => "UF_USE_TEXT",
				1 => "",
				2 => "",
			),
			"SHOW_PARENT_NAME" => "Y",
			"TOP_DEPTH" => "2",
			"VIEW_MODE" => "LINE",
			"COMPONENT_TEMPLATE" => "useblock_for_business"
		),
		false
	);?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"AREA_FILE_SUFFIX" => "inc",
			"EDIT_TEMPLATE" => "",
			"PATH" => "/include/featuresblock_for_business.php",
			"SECTION_CODE" => "storage"
		),
		false,
		Array("HIDE_ICONS" => "N")
	);?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list", 
		"storage_list_withmap", 
		array(
			"ADD_SECTIONS_CHAIN" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COUNT_ELEMENTS" => "N",
			"FILTER_NAME" => "",
			"IBLOCK_ID" => "40",
			"IBLOCK_TYPE" => "aspro_priority_catalog",
			"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
			"SECTION_FIELDS" => array(
				0 => "NAME",
				1 => "DESCRIPTION",
				2 => "",
			),
			"SECTION_ID" => "",
			"SECTION_URL" => "#CODE#/",
			"SECTION_USER_FIELDS" => array(
				0 => "UF_PHOTOGALLERY",
				1 => "UF_ADDRESS",
				2 => "UF_RECEPTION",
				3 => "UF_DOSTUP_TIME",
				4 => "UF_PHONE",
				5 => "UF_MAP",
				6 => "",
			),
			"SHOW_PARENT_NAME" => "Y",
			"TOP_DEPTH" => "1",
			"VIEW_MODE" => "LINE",
			"COMPONENT_TEMPLATE" => "storage_list_withmap"
		),
		false
	);?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:news.list", 
		"partners_front_1_v2", 
		array(
			"COMPONENT_TEMPLATE" => "partners_front_1_v2",
			"IBLOCK_TYPE" => "aspro_priority_content",
			"IBLOCK_ID" => "14",
			"NEWS_COUNT" => "20",
			"SORT_BY1" => "SORT",
			"SORT_ORDER1" => "ASC",
			"SORT_BY2" => "ID",
			"SORT_ORDER2" => "ASC",
			"FILTER_NAME" => "",
			"FIELD_CODE" => array(
				0 => "NAME",
				1 => "PREVIEW_PICTURE",
				2 => "",
			),
			"PROPERTY_CODE" => array(
				0 => "",
				1 => "",
			),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"SET_TITLE" => "N",
			"SET_BROWSER_TITLE" => "N",
			"SET_META_KEYWORDS" => "N",
			"SET_META_DESCRIPTION" => "N",
			"SET_LAST_MODIFIED" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "N",
			"STRICT_SECTION_CHECK" => "N",
			"SHOW_DETAIL_LINK" => "N",
			"TITLE" => "Нам доверяют",
			"SHOW_ALL_TITLE" => "",
			"PAGER_TEMPLATE" => ".default",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "Новости",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"SET_STATUS_404" => "N",
			"SHOW_404" => "N",
			"MESSAGE_404" => ""
		),
		false
	);?>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>